<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    
    protected $fillable = ['fullname','email','user_id','admin_status','password','phone','image','type','CUID','fsocialid','gsocialid','pavr_status','chat_status','report_status'];
}
