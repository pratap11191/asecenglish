<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    //

    protected $fillable = [
        'video_id','user_id','rating_num','feedback'];
}
