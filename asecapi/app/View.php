<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class View extends Model
{
    
    protected $fillable = ['user_id', 'instance_id','course_id'];
}
