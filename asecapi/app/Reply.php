<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reply extends Model
{
    
    protected $fillable = ['thread_id', 'msg','user_id'];
}
