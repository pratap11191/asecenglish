<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classroom extends Model
{
    
    protected $fillable = ['name','number','number_of_children','number_of_nannies','grade_id','user_id'];
}
