<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fametalerating extends Model
{
    //
    protected $fillable = [
        'user_id','rating','feedback'];
}
