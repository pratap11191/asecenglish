<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Forumtopiccomment extends Model
{
    
    protected $fillable = ['topic_id', 'msg','user_id'];
}
