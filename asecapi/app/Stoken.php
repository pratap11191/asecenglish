<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stoken extends Model
{
    
    protected $fillable = ['user_id', 'token', 'deviceType','deviceToken'];
}
