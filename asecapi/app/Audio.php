<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Audio extends Model
{
    //

     protected $fillable = [
        'user_id','topic_id','description','audio_url','duration','tag_list'];
}
