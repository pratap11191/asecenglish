<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mycourse extends Model
{
    
    protected $fillable = ['course_id','user_id','type'];
}
