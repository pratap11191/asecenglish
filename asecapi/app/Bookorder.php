<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bookorder extends Model
{
    //

     protected $fillable = [
        'user_id','book_id','address1','address2','city','pincode'];
}
