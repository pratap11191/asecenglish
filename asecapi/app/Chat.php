<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    //
    protected $fillable = ['nursery_id', 'parent_id','msg','sender_type'];
}
