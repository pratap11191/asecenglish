<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meal extends Model
{
    
     protected $fillable = ['type1','item_lists1','type2','item_lists2','type3','item_lists3','day_of_week','user_id'];
}
