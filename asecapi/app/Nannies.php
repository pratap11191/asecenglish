<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nannies extends Model
{
    
     protected $fillable = ['name','surename','email','password','user_id','classroom_id'];
}
