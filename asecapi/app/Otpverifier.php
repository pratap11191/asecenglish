<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Otpverifier extends Model
{
    
    protected $fillable = ['email_id', 'otp'];
}
