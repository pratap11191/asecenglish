<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Advertisement extends Model
{
    //
    
     protected $fillable = ['ad_image', 'status','type'];
}
