<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sessionjoin extends Model
{
    
    protected $fillable = ['user_id', 'filelink', 'event_id'];
}
