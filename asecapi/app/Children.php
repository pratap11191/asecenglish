<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Children extends Model
{
    
    protected $fillable = ['name','surname','CUID','age','image','father','mother','classroom_id','email','phone','user_id','SCUID'];
}
