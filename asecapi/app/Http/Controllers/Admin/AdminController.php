<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Pagination\LengthAwarePaginator;
use App\AdvertisementCount;
use App\Http\Controllers\Controller;
use App\RestaurantList;
use App\User;
use App\Video;
use Auth;
use DB;
use Hash;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image as Image;
use Mail;
use Redirect;
use Session;
use View;
use Crypt;
use Validator;
use App\Advertisement;
use App\Topic;
use App\Report;
use App\Member;
use App\Classroom;
use App\Grade;
use App\Nannies;
use App\Children;
use App\Query;
use App\Menuitem;
use App\Meal;


class AdminController extends Controller
{
	public function gradedetails(Request $request,$id)
	{
		$classroomsdetails = Classroom::find($id);
		if ((Auth::user()) && Auth::user()->is_admin == '0') {
            
            $obj   = new CommonAdminController;
            $usersgradedetails = Children::orderBy('id', 'DESC')->where(['user_id'=>Auth::user()->id,'classroom_id'=>$id])->get();
            foreach ($usersgradedetails as $user) {
                $user->created_on = date("dFY", strtotime($user->created_at));
            }

              $users1gradedetails = Nannies::orderBy('id', 'DESC')->where(['user_id'=>Auth::user()->id,'classroom_id'=>$id])->get();
            foreach ($users1gradedetails as $user) {
                $user->created_on = date("dFY", strtotime($user->created_at));
            }

            $classroomsdetails_name = $classroomsdetails->name;

            $classroomsdetails_id = $id;

            return view('admins.gradedetails',compact('usersgradedetails','users1gradedetails','classroomsdetails_name','classroomsdetails_id'));
        } else {
           return redirect()->intended(route('signin'));
        }	
	}

    public function index(Request $request)
    {

    }
 

 public function uploadcsv(Request $request,$id)

    {

	  is_uploaded_file($_FILES['filename']['tmp_name']);


	$handle = fopen($_FILES['filename']['tmp_name'], "r");

	$dataarray =array();


	while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {

		 $dataarray['name']            = $data[0];
		 $dataarray['age']             = $data[1];
		 $dataarray['email']           = $data[2];
		 $dataarray['phone']           = $data[3];
		 $dataarray['surname']         = $data[4];

		 $dataarray['CUID']           = $this->uniquecuidorscuidgenration('CUID',$dataarray['name'],$dataarray['surname']);
		 $dataarray['SCUID']          = $this->uniquecuidorscuidgenration('SCUID',$dataarray['name'],$dataarray['surname']);
		 $dataarray['father']          = $data[5];
		 $dataarray['mother']          = $data[6];
		 $dataarray['classroom_id']    = $id;

		 $dataarray['user_id']    = Auth::user()->id;

         

         Children::create($dataarray);

		 //here in table column is classroom_id and we add classromm name

		 //$dataarray['classroom_name']   = $data[9];

		}
		
	
				
	fclose($handle);

    Session::flash('message', 'Children csv file uploded successfully'); 

	return back();



    }

    

    public function user_list(Request $request)
    {
        if ((Auth::user()) && Auth::user()->is_admin == '1') {
           
              $obj   = new CommonAdminController;
            $users = User::where('is_admin', '0')->orderBy('id', 'DESC')->get();
            foreach ($users as $user) {
                if(empty($user->image)){
                $user->image = url('/public/img/user-default.png');
                }
                $user->created_on = date("dFY", strtotime($user->created_at));
            }
            return view('admins.user_list', compact('users'));
        } else {
           return redirect()->intended(route('signin'));
        }
    }

    public function children_list(Request $request)
    {
        if ((Auth::user()) && Auth::user()->is_admin == '0') {
            $obj   = new CommonAdminController;
            $users = Children::orderBy('id', 'DESC')->where('user_id',Auth::user()->id)->get();
            foreach ($users as $user) {
                $user->created_on = date("dFY", strtotime($user->created_at));
            }
            $users1 = Classroom::orderBy('id', 'DESC')->where('user_id',Auth::user()->id)->get()->toArray();
            return view('admins.children_list', compact('users','users1'));
            
        } else {
            return redirect()->intended(route('signin'));
        }
    }

    public function classroom_list(Request $request)
    {
         if ((Auth::user()) && Auth::user()->is_admin == '0') {
           
            $obj   = new CommonAdminController;
            if(isset($_GET['grade_id']) && !empty($_GET['grade_id'])){
            $users = Classroom::orderBy('id', 'DESC')->where(['user_id'=>Auth::user()->id,'grade_id'=>$_GET['grade_id']])->get();
            } else {
            $users = Classroom::orderBy('id', 'DESC')->where('user_id',Auth::user()->id)->get();    
            }
            foreach ($users as $user) {
                $user->created_on = date("dFY", strtotime($user->created_at));
            }
            $users1 = Grade::orderBy('id', 'DESC')->where('user_id',Auth::user()->id)->get()->toArray();
           
            return view('admins.classroom_list', compact('users','users1'));
        } else {
             return redirect()->intended(route('signin'));
        }
    }

    public function nannies_list(Request $request)
    {
        if ((Auth::user()) && Auth::user()->is_admin == '0') {
            
             $obj   = new CommonAdminController;
            $users = Nannies::orderBy('id', 'DESC')->where('user_id',Auth::user()->id)->get();
            foreach ($users as $user) {
                $user->created_on = date("dFY", strtotime($user->created_at));
            }
             $users1 = Classroom::orderBy('id', 'DESC')->where('user_id',Auth::user()->id)->get()->toArray();
            return view('admins.nannies_list', compact('users','users1'));
        } else {
           return redirect()->intended(route('signin'));
        }
    }

    public function parent_list(Request $request)
    {
        if ((Auth::user()) && Auth::user()->is_admin == '1') {
            
             $obj   = new CommonAdminController;
            $users = Member::join('users','members.user_id','=','users.id')->select('members.id','members.email','members.fullname','members.phone','members.image','users.fullname as nfullname','members.created_at','members.admin_status')->orderBy('members.id', 'DESC')->get();
            foreach ($users as $user) {
                if(empty($user->image)){
                $user->image = url('/public/img/user-default.png');
                }
                $user->created_on = date("dFY", strtotime($user->created_at));
            }
            return view('admins.parent_list', compact('users'));
        } else {
           return redirect()->intended(route('signin'));
        }
    }


    public function grades_list(Request $request)
    {
       //return view('admins.faqnursery');
       
        if ((Auth::user()) && Auth::user()->is_admin == '0') {
            $users = Grade::where('user_id',Auth::user()->id)->orderBy('id', 'DESC')->get()->toArray();
            if ($request->isMethod('post')) {
                $input = $request->all();
                $input['user_id'] = Auth::user()->id;

                $obj = new CommonAdminController;
                if (!empty($request->file('image'))) {
                     $filename = substr( md5( time() ), 0, 15) . '.' . $request->file('image')->getClientOriginalExtension();

                     $path = public_path('users-photos/' . $filename);

                     Image::make($request->image)->orientate()->fit(500)->save($path);
             
                      $image = '/users-photos/'.$filename;

                      $input['image'] = $image;
                }

                Grade::create($input);

                return redirect()->back()->with('message', 'Your Grade Successfully Created'); 
            } else {
                return view('admins.grades_list', compact('users'));
            } 
            
        } else {
       
           return redirect()->intended(route('signin'));       
        }
    
    }

    public function menu_list(Request $request)
    {
       //return view('admins.faqnursery');
       
        if ((Auth::user()) && Auth::user()->is_admin == '0') {
            $Breakfast2 = Meal::where(['type1'=>'Breakfast','user_id'=>Auth::user()->id])->orderBy('id', 'DESC')->get()->toArray();
            $Lunch2 = Meal::where(['type2'=>'Lunch','user_id'=>Auth::user()->id])->orderBy('id', 'DESC')->get()->toArray();
            $Snack2 = Meal::where(['type3'=>'Snack','user_id'=>Auth::user()->id])->orderBy('id', 'DESC')->get()->toArray();

            $Breakfast = array();
            $Lunch = array();
            $Snack = array();

              foreach ($Breakfast2 as $value) {
                 $value['item_lists1'] = $this->idconverttostr($value['item_lists1'],'menuitems');
                 //dd($value['item_lists1']);
                 $Breakfast[] = $value;
            }

             //dd($Breakfast);

             foreach ($Lunch2 as $value) {
               $value['item_lists2'] = $this->idconverttostr($value['item_lists2'],'menuitems');
               $Lunch[] = $value;
            }

             foreach ($Snack2 as $value) {
                $value['item_lists3'] = $this->idconverttostr($value['item_lists3'],'menuitems');
                $Snack[] = $value;
            }

           
            if ($request->isMethod('post')) {
                $input = $request->all();
                $input['user_id'] = Auth::user()->id;

                $menuobj = Menuitem::create($input);

                $input['type1'] = 'Breakfast';
                $input['type2'] = 'Lunch';
                $input['type3'] = 'Snack';
               
                if($input['type'] == 'Breakfast'){
                      $Breakfast1 = Meal::where(['type1'=>'Breakfast','user_id'=>Auth::user()->id,'day_of_week'=>$input['day_of_week']])->orderBy('id', 'DESC')->first();
                    if(!empty($Breakfast1)){
                         if(!empty($Breakfast1->item_lists1)){
                         $Breakfast1newarray = explode(",", $Breakfast1->item_lists1);
                         }
                        $Breakfast1newarray[] = $menuobj->id;
                        $Breakfast1->item_lists1 = implode(",", $Breakfast1newarray);
                        $Breakfast1->save();
                    } else {
                        $input['item_lists1'] = $menuobj->id;
                        $input['item_lists2'] = '';
                        $input['item_lists3'] = '';
                        Meal::create($input);
                    }
                }

                if($input['type'] == 'Lunch'){
                      $Lunch1 = Meal::where(['type2'=>'Lunch','user_id'=>Auth::user()->id,'day_of_week'=>$input['day_of_week']])->orderBy('id', 'DESC')->first();
                    if(!empty($Lunch1)){
                         if(!empty($Lunch1->item_lists2)){
                         $Breakfast1newarray = explode(",", $Lunch1->item_lists2);
                         }
                        $Breakfast1newarray[] = $menuobj->id;
                        $Lunch1->item_lists2 = implode(",", $Breakfast1newarray);
                        $Lunch1->save();
                    } else {
                         $input['item_lists2'] = $menuobj->id;
                        $input['item_lists1'] = '';
                        $input['item_lists3'] = '';
                         Meal::create($input);
                    }
                }

                if($input['type'] == 'Snack'){
                    $Snack1 = Meal::where(['type3'=>'Snack','user_id'=>Auth::user()->id,'day_of_week'=>$input['day_of_week']])->orderBy('id', 'DESC')->first();
                    if(!empty($Snack1)){
                         if(!empty($Snack1->item_lists3)){
                         $Breakfast1newarray = explode(",", $Snack1->item_lists3);
                         }
                        $Breakfast1newarray[] = $menuobj->id;
                        $Snack1->item_lists3 = implode(",", $Breakfast1newarray);
                        $Snack1->save();
                    } else {
                        $input['item_lists3'] = $menuobj->id;
                        $input['item_lists1'] = '';
                        $input['item_lists2'] = '';
                         Meal::create($input);
                    }
                }

                return redirect()->back()->with('message', 'Your Menu Successfully Submited'); 
            } else {

               
                return view('admins.menu_list',compact('Snack','Breakfast','Lunch'));
            } 
            
        } else {
       
           return redirect()->intended(route('signin'));       
        }
    
    }
    
    public function faqnursery(Request $request)
    {
       //return view('admins.faqnursery');
       
        if ((Auth::user()) && Auth::user()->is_admin == '0') {
            if ($request->isMethod('post')) {
                $input = $request->all();
                $input['user_id'] = Auth::user()->id;

                Query::create($input);

                return redirect()->back()->with('message', 'Your Query Successfully Submited'); 
            } else {
                return view('admins.faqnursery');
            } 
            
        } else {
       
           return redirect()->intended(route('signin'));       
        }
    
    } 
    
    public function adupload()
    {
    
        return view('admins.ad_genre');
    
    }

    public function deletechild(Request $request,$id)
    {
        $findreport = Classroom::where('id',$id)->delete();

        return back();
    }

    public function addchildes1(Request $request)
    {

        $input = $request->all();

        //dd($input);

        // $val_arr = [
        //     'name'=>'required',
        //     'surename'=>'required',
        //     'age' => 'required',
        //     'father'=>'required',
        //     'email'=>'required',
        //     'mother'=>'required',
        //     'phone'=>'required',
        //     'classroom_id'=>'required',
        //      'image'    => 'image|mimes:jpeg,png,jpg,gif,svg|max:10240',
        // ];

        //  $messages = [

        // 'name.required' => 'Please enter Child Name',
        // 'surename.required'=>'Please enter Child surename',
        // 'age.required'=>'Please enter Age',
        // 'father.required'=>'Please enter Father Name',
        // 'mother.required'=>'Please enter Mother Name',
        //  'email.required'=>'Please enter Email',
        //   'phone.required'=>'Please enter Phone Number',

        //  ];


        // $validator = Validator::make($input, $val_arr,$messages);


        // if($validator->fails()){

        //     echo json_encode(array("0",$validator->errors()->first()));

        //     die();
              
        // }

        $obj = new CommonAdminController;
        if (!empty($request->file('image'))) {
             $filename = substr( md5( time() ), 0, 15) . '.' . $request->file('image')->getClientOriginalExtension();

             $path = public_path('users-photos/' . $filename);

             Image::make($request->image)->orientate()->fit(500)->save($path);
     
              $image = '/users-photos/'.$filename;

              $input['image'] = $image;
        }

            $input['user_id'] = Auth::user()->id;

             $input['CUID'] = $this->uniquecuidorscuidgenration('CUID',$input['name'],$input['surname']);

             

             $input['SCUID'] = $this->uniquecuidorscuidgenration('SCUID',$input['name'],$input['surname']);

            Children::create($input);
            
                 return redirect()->back()->with('message', 'Children Details Added Successfully'); 
        


    }

    public function uniquecuidorscuidgenration($type,$name,$surename)
    {
    	if($type == 'CUID'){
    		$key_flag =  'P'.strtoupper(substr($name,0,2).substr($surename,0,1)).rand(10,100);

    		$checkdata = Children::where($type,$key_flag)->first();
            if(!empty($checkdata)){
            	return $this->uniquecuidorscuidgenration($type,$name,$surename);
            }	
    	} else if($type == 'SCUID'){
    		$key_flag = 'S'.strtoupper(rand(10000,99999));

    		$checkdata = Children::where($type,$key_flag)->first();
            if(!empty($checkdata)){
            	return $this->uniquecuidorscuidgenration($type,$name,$surename);
            }
    	}

    	return $key_flag;
    }

    public function editchildes1(Request $request)
    {

       $input = $request->all(); 

        $details = Children::find($input['id']);  

       $obj = new CommonAdminController;
        if (!empty($request->file('image'))) {
             $filename = substr( md5( time() ), 0, 15) . '.' . $request->file('image')->getClientOriginalExtension();

             $path = public_path('users-photos/' . $filename);

             Image::make($request->image)->orientate()->fit(500)->save($path);
     
              $image = '/users-photos/'.$filename;

              $details->image = $image;
        }

           

         

        $details->name = $input['name'];
        $details->surname = $input['surname'];
        $details->email = $input['email'];
        $details->phone = $input['phone'];
        $details->father = $input['father'];
        $details->mother = $input['mother'];
        $details->age = $input['age'];
        $details->classroom_id = $input['classroom_id'];

        $details->save();

            
                 return redirect()->back()->with('message', 'Children Details Updated Successfully'); 
        


    }

    public function addchildes(Request $request)
    {

        $input = $request->all();

        $val_arr = [
            'name'=>'required',
            'number'=>'required',
            'number_of_nannies' => 'required',
            'number_of_children'=>'required',
             'grade_id'=>'required',
        ];

         $messages = [

        'name.required' => 'Please enter Classroom Name',
        'number.required'=>'Please enter Classroom Number',
        'number_of_nannies.required'=>'Please enter Number of Nannies',
        'number_of_children.required'=>'Please enter Number of Children',
        'grade_id.required'=>'Please enter Grade',

         ];


        $validator = Validator::make($input, $val_arr,$messages);


        if($validator->fails()){

            echo json_encode(array("0",$validator->errors()->first()));

            die();
              
        }

        $input['user_id'] = Auth::user()->id;

            Classroom::create($input);
            
                   echo json_encode(array("1","Classroom created successfully!!"));
                die();
        


    }

    public function editchildes(Request $request)
    {

        $input = $request->all();

        $val_arr = [
            'name'=>'required',
            'number'=>'required',
            'number_of_nannies' => 'required',
            'number_of_children'=>'required',
             'grade_id'=>'required',
        ];

         $messages = [

        'name.required' => 'Please enter Classroom Name',
        'number.required'=>'Please enter Classroom Number',
        'number_of_nannies.required'=>'Please enter Number of Nannies',
        'number_of_children.required'=>'Please enter Number of Children',
        'grade_id.required'=>'Please enter Grade',

         ];


        $validator = Validator::make($input, $val_arr,$messages);


        if($validator->fails()){

            echo json_encode(array("0",$validator->errors()->first(),$input['id']));

            die();
              
        }

        $posting = Classroom::find($input['id']);

        $posting->name = $input['name'];
        $posting->number = $input['number'];
        $posting->number_of_nannies = $input['number_of_nannies'];
        $posting->number_of_children = $input['number_of_children'];
        $posting->grade_id = $input['grade_id'];
        $posting->save();
            
                   echo json_encode(array("1","Classroom created successfully!!",$input['id']));
                die();
        


    }

    public function addnannies(Request $request)
    {

        $input = $request->all();

        $val_arr = [
            'name'=>'required',
            'surename'=>'required',
            'email' => 'required',
            'password'=>'required|min:6',
            'classroom_id'=>'required',
        ];

         $messages = [

        'name.required' => 'Please enter Name',
        'surename.required'=>'Please enter surename',
        'email.required'=>'Please enter email',
        'password.required'=>'Please enter password',
        'classroom_id.required'=>'Please select a Classroom',
         ];


        $validator = Validator::make($input, $val_arr,$messages);


        if($validator->fails()){

            echo json_encode(array("0",$validator->errors()->first()));

            die();
              
        }

        $input['user_id'] = Auth::user()->id;

            Nannies::create($input);

             Session::flash('message', 'Nannies Added Successfully');
            
                   echo json_encode(array("1","Nanny created successfully!!"));
                die();
        


    }

    public function editnannies(Request $request)
    {

        $input = $request->all();

         $val_arr = [
            'name'=>'required',
            'surename'=>'required',
            'email' => 'required',
            'password'=>'required|min:6',
            'classroom_id'=>'required',
        ];

         $messages = [

        'name.required' => 'Please enter Name',
        'surename.required'=>'Please enter surename',
        'email.required'=>'Please enter email',
        'password.required'=>'Please enter password',
        'classroom_id.required'=>'Please select a Classroom',
         ];


        $validator = Validator::make($input, $val_arr,$messages);


        if($validator->fails()){

            echo json_encode(array("0",$validator->errors()->first(),$input['id']));

            die();
              
        }

        

        $posting = Nannies::find($input['id']);

        $posting->name = $input['name'];
        $posting->surename = $input['surename'];
        $posting->email = $input['email'];
        $posting->password = $input['password'];
        $posting->classroom_id = $input['classroom_id'];
        $posting->save();

         Session::flash('message', 'Nannies Updated Successfully');
            
                   echo json_encode(array("1","Nanny Updated successfully!!",$input['id']));
                die();
        


    }


    
    public function addnursury(Request $request)
    {
         if ((Auth::user())) {
            if ($request->isMethod('post')) {
                $input = $request->all();
                $val_arr = [
                    'email'=>'required',
                    'fullname'=>'required|min:2',
                    'phone'=>'required|min:10',
                    'password'=>'required|min:6',
                ];

                $message = [
                    'email.required'=>'Please enter Email ID',
                    'fullname.required'=>'Please enter Fullname',
                    'phone.required'=>'Please enter Phone Number',
                    'password.required'=>'Please enter a secure password',
                ];

                $validator = Validator::make($input, $val_arr,$message);


                if($validator->fails()){

                       $messages =$validator->errors()->toArray();

                       //dd($messages);

                       return view('admins.addnursury',compact('messages'));
                      
                }

                User::create($input);

                return redirect()->intended(route('admins.user_list')); 
            } else {
                return view('admins.addnursury');
            } 
            
        } else {
       
           return redirect()->intended(route('signin'));       
        }
    
    }

     public function addparents(Request $request)
    {
         if ((Auth::user())) {
            $nursurylist = User::where('is_admin', '0')->orderBy('id', 'DESC')->get();
            if ($request->isMethod('post')) {
                $input = $request->all();
                $val_arr = [
                    'email'=>'required',
                    'fullname'=>'required|min:2',
                    'phone'=>'required|min:10',
                    'password'=>'required|min:6',
                    'user_id'=>'required',
                ];

                $message = [
                    'email.required'=>'Please enter Email ID',
                    'fullname.required'=>'Please enter Fullname',
                    'phone.required'=>'Please enter Phone Number',
                    'password.required'=>'Please enter a secure password',
                ];

                $validator = Validator::make($input, $val_arr,$message);


                if($validator->fails()){

                       $messages =$validator->errors()->toArray();

                       //dd($messages);

                       return view('admins.addparents',compact('messages','nursurylist'));
                      
                }

                Member::create($input);

                return redirect()->intended(route('admins.parent_list')); 
            } else {
                return view('admins.addparents',compact('nursurylist'));
            } 
            
        } else {
       
           return redirect()->intended(route('signin'));       
        }
    
    }

    public function editnursury(Request $request,$id)
    {
         if ((Auth::user())) {
              $userdeatils = User::find($id);
            if ($request->isMethod('post')) {
                $input = $request->all();
                $val_arr = [
                    'email'=>'required',
                    'fullname'=>'required|min:2',
                    'phone'=>'required|min:10',
                    'password'=>'required|min:6',
                ];

                $message = [
                    'email.required'=>'Please enter Email ID',
                    'fullname.required'=>'Please enter Fullname',
                    'phone.required'=>'Please enter Phone Number',
                    'password.required'=>'Please enter a secure password',
                ];

                $validator = Validator::make($input, $val_arr,$message);


                if($validator->fails()){

                       $messages =$validator->errors()->toArray();

                       //dd($messages);

                       return view('admins.editnursury',compact('messages','userdeatils','id'));
                      
                }

                $userdeatils->email = $input['email'];
                $userdeatils->fullname = $input['fullname'];
                $userdeatils->phone = $input['phone'];
                $userdeatils->password = $input['password'];
                $userdeatils->save();

                //User::create($input);

                return redirect()->intended(route('admins.user_list')); 
            } else {
                return view('admins.editnursury',compact('userdeatils','id'));
            } 
            
        } else {
       
           return redirect()->intended(route('signin'));       
        }
    
    }

    public function editparents(Request $request,$id)
    {
         if ((Auth::user())) {
              $nursurylist = User::where('is_admin', '0')->orderBy('id', 'DESC')->get();
              $userdeatils = Member::find($id);
            if ($request->isMethod('post')) {
                $input = $request->all();
                $val_arr = [
                    'email'=>'required',
                    'fullname'=>'required|min:2',
                    'phone'=>'required|min:10',
                    'password'=>'required|min:6',
                ];

                $message = [
                    'email.required'=>'Please enter Email ID',
                    'fullname.required'=>'Please enter Fullname',
                    'phone.required'=>'Please enter Phone Number',
                    'password.required'=>'Please enter a secure password',
                ];

                $validator = Validator::make($input, $val_arr,$message);


                if($validator->fails()){

                       $messages =$validator->errors()->toArray();

                       //dd($messages);

                       return view('admins.editparents',compact('messages','userdeatils','id','nursurylist'));
                      
                }

                $userdeatils->email = $input['email'];
                $userdeatils->fullname = $input['fullname'];
                $userdeatils->phone = $input['phone'];
                $userdeatils->password = $input['password'];
                $userdeatils->user_id = $input['user_id'];
                $userdeatils->save();

                //User::create($input);

                return redirect()->intended(route('admins.parent_list')); 
            } else {
                return view('admins.editparents',compact('userdeatils','id','nursurylist'));
            } 
            
        } else {
       
           return redirect()->intended(route('signin'));       
        }
    
    }
    
    

    public function ignor(Request $request,$id)
    {
        $findreport = Report::where('reported_user_id',$id)->delete();

        return back();
    }

    public function block(Request $request,$id)
    {
        $user = User::find($id);

       
        $user->admin_status = "0";

        $user->save();

        return back();
    
    }
    
     public function uploadTopic(Request $request)
    {
        $input = $request->all();

        $val_arr = [
            'name'=>'required'
        ];


        

        $validator = Validator::make($input, $val_arr);


        if($validator->fails()){
          
            $messages =$validator->errors()->toArray();

          

            return view('admins.ad_genre',compact('messages'));    
        }

        
        
        Topic::create($input);
        
        return redirect('admin/genre_list');
    }

    public function login(Request $request)
    {
        if ((Auth::user())) {
            if(Auth::user()->is_admin == '1'){
                return redirect()->intended(route('user_list'));
            } else {
                return redirect()->intended(route('grades_list'));
            }
        } else {
            if ($request->isMethod('post')) {
                $rule = $this->validate($request, [
                    'email'    => 'required|email|max:190',
                    'password' => 'required|max:190',
                ]);

                if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'is_admin' => '1'], $request->remember)) {
                    return redirect('user_list');
                } else {
                    //dd($request->all());
                   if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'is_admin' => '0'], $request->remember)) {
                        
                        return redirect('grades_list');
                    } else {
                        $request->session()->flash('alert-danger', 'Wrong Credentials ');
                        return redirect('signin');
                    }
                }
            }
            return view('admins.login');
        }

    }

    public function changeStatus($id)
    {

        $user = User::find($id);

        if($user->admin_status == "0"){
            $user->admin_status = "1";
        } else {
            $user->admin_status = "0";
        }

        $user->save();

        return back();
    }

    public function change_statusChildren($id)
    {

        $user = Children::find($id);

        //dd($user);

        if($user->admin_status == "0"){
            $user->admin_status = "1";
        } else {
            $user->admin_status = "0";
        }

        $user->save();

        return back();
    }

     public function changeStatus1p($id)
    {

        $user = Member::find($id);

        if($user->admin_status == "0"){
            $user->admin_status = "1";
        } else {
            $user->admin_status = "0";
        }

        $user->save();

        return back();
    }

    public function changeStatusad($id)
    {

        $ad = Advertisement::find($id);

        if($ad->status == "0"){
            $ad->status = "1";
        } else {
            $ad->status = "0";
        }

        $ad->save();

        return back();
    }

    public function logout(Request $request)
    {
        Auth::logout();
        return redirect('signin');
    }

    
   public function report_user_list(Request $request)
    {
        if ((Auth::user()) && Auth::user()->is_admin == '0') {

            if(isset($_GET['grade_id']) && !empty($_GET['grade_id'])){
            $reportlistdetails = DB::table('reports')->join('nannies','reports.nanny_id','=','nannies.id')->join('classrooms','reports.classroom_id','=','classrooms.id')->join('grades','reports.grade_id','=','grades.id')->orderBy('reports.id', 'DESC')->select('reports.id as reports_id','nannies.username','classrooms.name as classroom_name','grades.name as grade_name','reports.meal_id','reports.activity_id','reports.toilet_id','reports.nap_id','reports.children_list','nannies.user_id as fatheridm')->where(['reports.grade_id'=>$_GET['grade_id'],'nannies.user_id'=>Auth::user()->id])->get();

            } else {
                  $reportlistdetails = DB::table('reports')->join('nannies','reports.nanny_id','=','nannies.id')->join('classrooms','reports.classroom_id','=','classrooms.id')->join('grades','reports.grade_id','=','grades.id')->orderBy('reports.id', 'DESC')->select('reports.id as reports_id','nannies.username','classrooms.name as classroom_name','grades.name as grade_name','reports.meal_id','reports.activity_id','reports.toilet_id','reports.nap_id','reports.children_list','nannies.user_id as fatheridm')->where('nannies.user_id',Auth::user()->id)->get();
            }

            foreach ($reportlistdetails as $value) {
                 $meal_list_array = DB::table('meals')->where('id',$value->meal_id)->first();

                 if(!empty($meal_list_array)){
                       $meal_list_array->item_lists1 =  $this->idconverttostr($meal_list_array->item_lists1,'menuitems');

                       $meal_list_array->item_lists2 =  $this->idconverttostr($meal_list_array->item_lists2,'menuitems');

                       $meal_list_array->item_lists3 =  $this->idconverttostr($meal_list_array->item_lists3,'menuitems');
                 }

                 $value->toilet_list_array = DB::table('toilets')->where('id',$value->toilet_id)->first();

                 $value->nap_list_array = DB::table('naps')->where('id',$value->nap_id)->first();

                 $value->meal_list_array = $meal_list_array;

                 $value->childrens_list_by_name = $this->idconverttostr($value->children_list,'childrens');
                 
            }

             $currentPage = LengthAwarePaginator::resolveCurrentPage();
 
            // Create a new Laravel collection from the array data
            $itemCollection = collect($reportlistdetails);
     
            // Define how many items we want to be visible in each page
            $perPage = 1;
     
            // Slice the collection to get the items to display in current page
            $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
     
            // Create our paginator and pass it to the view
            $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
     
            // set url path for generted links
            $paginatedItems->setPath($request->url());
 
        

            return view('admins.reportlist',['items' => $paginatedItems]);
                
        } else {
           
           // dd($users);
           return redirect()->intended(route('signin'));
        }
    }

    public function idconverttostr($str,$table)
    {
        $newarray = explode(",", $str);

        $return_array = array();

        foreach ($newarray as $value) {

             if($table == 'menuitems'){
                $getname = DB::table($table)->where('id',$value)->first();
                
                if(!empty($getname)){

                $return_array[] = $getname->item_msg;
                }
             } else {
                $getname = DB::table($table)->where('id',$value)->first();

                $return_array[] = $getname->name;
             }
            
        }

        return implode(",", $return_array);
    }

    
    public function genre_list(Request $request)
    {
        if (is_null(Auth::user())) {
            return redirect()->intended(route('admins.login'));
        } else {
            $obj   = new CommonAdminController;
            $users = Topic::orderBy('id', 'DESC')->get();

           // dd($users);
            foreach ($users as $user) {
               
                $user->created_on = date("dFY", strtotime($user->created_at));
            }
            return view('admins.genre_list', compact('users'));
        }
    }

  

    public function video_list(Request $request)
    {
        if (is_null(Auth::user())) {
            return redirect()->intended(route('admins.login'));
        } else {
           $recorde_tranding = DB::table('videos')->join('categories', 'videos.categories_id', '=', 'categories.id')->join('hashtags', 'videos.hashtag_id', '=', 'hashtags.id')->join('users','videos.user_id','=','users.id')->select('videos.user_id as userId','videos.id as id','users.image','users.username','users.name','videos.video_name','videos.description','videos.view_count','hashtags.tag_name','categories.category_name','videos.created_at')->orderBy('videos.id', 'DESC')->get();


        $users = array();

        foreach ($recorde_tranding->toArray() as  $value) {
         

          $avgrate = DB::table('ratings')->where('video_id',$value->id)->select(DB::raw("SUM(rating_num)/count(*) as avgratem"))->first();


          if(empty($avgrate->avgratem))
          {
               $value->avg_rating = "0";
          } else {
             $value->avg_rating = $avgrate->avgratem;
          }


         

          
           $users[] = $value;
          
        }
        
        


        
        
        //dd($return_array);
            return view('admins.video_list', compact('users'));
        }
    }


    public function edit_user_details(Request $request,$id)
    {
        if (is_null(Auth::user())) {
            return redirect()->intended(route('signin'));
        } else {
            $obj   = new CommonAdminController;
            $person = User::where(['is_admin'=>'0','id'=>$id])->first();
            
                if(empty($person['image'])){
                $person['image'] = url('/public/img/user-default.png');
                }
                //$person['created_on'] = date("dFY", strtotime($person['created_at']));
            

            ///dd($person);
            return view('admins.edit_user_details', compact('person'));
        }
    }
    
    public function common_delete($id,$table)
    {
       
            DB::table($table)->where('id', $id)->delete();

            return back();
           
    }

    public function edit_user_details_update(Request $request,$id)
    {
        if (is_null(Auth::user())) {
            return redirect()->intended(route('admins.login'));
        } else {
           
            $person = User::where(['is_admin'=>'0','id'=>$id])->first();
            if ($request->isMethod('post')) {
                $rule = $this->validate($request, [
                    'username'    => 'required|max:20',
                    'phone' => 'required',
                    'name'     => 'required|max:190',
                    'image'    => 'image|mimes:jpeg,png,jpg,gif,svg|max:10240',
                ]);
                $obj = new CommonAdminController;
                if (!empty($request->file('image'))) {
                     $filename = substr( md5( $id . '-' . time() ), 0, 15) . '.' . $request->file('image')->getClientOriginalExtension();

                     $path = public_path('users-photos/' . $filename);

                     Image::make($request->image)->orientate()->fit(500)->save($path);
             
                      $image = url('/').'/public/users-photos/'.$filename;
                } else {
                    $image = $person->image;
                }

                $update = array(
                    'username'           => $request->username,
                    'phone'        => $request->phone,
                    'name'        => $request->name,
                    'image'        => $image,
                );

                $user_array = User::find($id);


                if (is_null($user_array)) {
                    $request->session()->flash('alert-danger', 'User not found.');
                } else{

                if (User::where('id', $id)->update($update)) {
                    $request->session()->flash('alert-success', 'Updated successfully.');
                    return back();
                } else {
                    $request->session()->flash('alert-danger', 'Internal error .');
                }
            }
            } else {
                return view('admins.edit_user_details', compact('person'));
            }
        }
    }

   

    
    

    
    public function profile(Request $request)
    {
        if (is_null(Auth::user())) {
            return redirect()->intended(route('signin'));
        } else {
            $admin = User::where("is_admin", "1")->first();
            if ($request->isMethod('post')) {
                $rule = $this->validate($request, [
                    'email'    => 'required|email|max:190',
                    'password' => 'required|max:190',
                    'fullname'     => 'required|max:190',
                    'image'    => 'image|mimes:jpeg,png,jpg,gif,svg|max:10240',
                ]);
                $obj = new CommonAdminController;
                if (!empty($request->file('image'))) {
                    if (file_exists(public_path('profile' . '/' . $admin->image))) {
                        @unlink(public_path('profile' . '/' . $admin->image));
                    }
                    if (file_exists(public_path('profile_resize' . '/' . $admin->image))) {
                        @unlink(public_path('profile_resize' . '/' . $admin->image));
                    }
                    $image = $obj->upload_image($request);
                } else {
                    $image = $admin->pic_file;
                }

                $update = array(
                    'email'           => $request->email,
                    'password'        => Hash::make($request->password),
                    'confirmpassword' => $request->password,
                    'fullname'        => $request->name,
                    'image'        => $image,
                );
                if (User::where('id', $admin->id)->update($update)) {
                    $request->session()->flash('alert-success', 'Updated successfully.');
                    return redirect('/admin/profile');
                } else {
                    $request->session()->flash('alert-danger', 'Internal error .');
                }
            } else {
                return view('admins.profile', compact('admin'));
            }
        }
    }

    public function nurserysignup(Request $request)
    {
        if ($request->isMethod('post')) {
                $input = $request->all();
                $val_arr = [
                    'email'=>'required',
                    'fullname'=>'required|max:16',
                    'phone'=>'required|min:10|max:15',
                    'password'=>'required|confirmed|min:6',
                    'password_confirmation'=>'required|min:6',
                    'address'=>'required',
                    'image'    => 'image|mimes:jpeg,png,jpg,gif,svg|max:10240',
                ];

                $message = [
                    'email.required'=>'Please enter Email ID',
                    'fullname.required'=>'Please enter Fullname',
                    'phone.required'=>'Please enter Phone Number',
                    'password.required'=>'Please enter a secure password',  
                    'password_confirmation.required'=>'Please renter your secure password',     
                    'address.required'=>'Please enter your address',
                ];

                $validator = Validator::make($input, $val_arr,$message);


                if($validator->fails()){

                       $messages =$validator->errors()->toArray();

                       //dd($messages);

                       return view('index',compact('messages'));
                      
                }

               $obj = new CommonAdminController;
                if (!empty($request->file('image'))) {
                     $filename = substr( md5( time() ), 0, 15) . '.' . $request->file('image')->getClientOriginalExtension();

                     $path = public_path('users-photos/' . $filename);

                     Image::make($request->image)->orientate()->fit(500)->save($path);
             
                      $image = '/users-photos/'.$filename;

                      $input['image'] = $image;
                }

                $input['confirmpassword'] = $input['password'];

                $input['password'] = Hash::make($input['password']);

                //$input['image'] = $image;

                User::create($input);

                return redirect()->intended(route('classroom_list')); 
            } else {
                return redirect()->intended(route('/')); 
            } 
    }



    
    public function forgot_password(Request $request)
    {
        try {
            if (Auth::user()) {
                return redirect()->intended(route('user_list'));
            } elseif ($request->isMethod('post')) {
                $this->validate($request, [
                    'email' => 'required|email',
                ]);
                $create = User::where('is_admin', '1')->where('email', $request->email)->first();
                if (!empty($create)) {
                    $password                = $create->confirmpassword; //mt_rand(10000, 99999);
                    $create->password        = Hash::make($password);
                    $create->confirmpassword = $password;
                    if ($create->save()) {
                        $email   = $create->email;
                        $subject = "New Password";
                        if (Mail::send('emails.forgot_password', ['newPassword' => $password, 'email' => $create->email], function ($message) use ($email, $subject) {
                            $message->to($email)->subject($subject);
                        })) {
                            $request->session()->flash('alert-success', 'Password send to your mail');
                            return redirect('/');
                        } else {
                            $request->session()->flash('alert-danger', 'Fail to send.');
                            return redirect('forgot_password');
                        }

                    } else {
                        $request->session()->flash('alert-success', 'Internal Server Error');
                    }
                } else {
                    $request->session()->flash('alert-danger', 'Not a valid user.');
                }
                return Redirect::back();
            } else {
                return view('admins.forgot_password');
            }

        } catch (Exception $e) {

        }
    }

    public function change_user_status(Request $request){
        $id = Crypt::decrypt($request->id);
    }

}
