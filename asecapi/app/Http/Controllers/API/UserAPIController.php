<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use App\User;
use App\Token;
use Validator;
use Mail;
use Image;
use Intervention\Image\ImageServiceProvider;
use DB;
use App\Member;
use App\Ptoken;
use App\Children;
use App\Follower;
use App\Nannies;
use App\Classroom;
use App\Stoken;
use App\Grade;
use App\Post;
use App\Comment;
use Carbon\Carbon;
use App\Chat;
use App\Mycourse;
use App\Bookorder;
use App\Attempt;
use App\Manageattempt;
use App\Forumtopiccomment;
use App\Reply;
use App\Help;
use PDF;
use App\Sessionjoin;
use App\Notification;
use App\View;



class UserAPIController extends APIBaseController
{

    public function testtandc(Request $request)
    {
       return view('testtandc');
    }

    public function tandc(Request $request)
    {
       return view('tandc');
    }

    public function viewbook(Request $request,$id)
    {
       $details = DB::table('books')->where('id',$id)->first();

       return view('viewbook',compact('details'));
    }

    public function sendpostnotify(Request $request,$msg)
    {
       $tokendetails = Token::get();
     
       $msgg = $msg;
       
        $return_arraynew = array("course_id"=>"","viewlink"=>"","msg"=>$msgg,"title"=>$msgg,"image"=>"","time"=>date("Y-m-d h:i:s"));

        $create_time = date("Y-m-d h:i:s");

        foreach ($tokendetails as $value) {

               $insertrecord = array("receiver_id"=>$value->user_id,"msg"=>$msgg,"create_time"=>$create_time);

               Notification::create($insertrecord);

               $userdetailsnotifystatus = User::find($value->user_id);

               if(!empty($userdetailsnotifystatus)){
                    if($value->deviceType == 'android' && $userdetailsnotifystatus->notify_status == "1")
              {
                    
                   
                $this->android_push($value->deviceToken,$msgg,"asec",1,$return_arraynew);
                   
              }

               }

        }

       return $this->sendResponse(['status'=>'success'], 'Notification Sent successfully',$request->path());      
    }

    public function discussioncommentlist1(Request $request,$id)
    {
        $finddetails = DB::table('forumtopiccomments')->join('users','forumtopiccomments.user_id','=','users.id')->where('forumtopiccomments.topic_id',$id)->orderby('forumtopiccomments.id','desc')->select('forumtopiccomments.id as thread_id','forumtopiccomments.msg','users.fullname','users.id as user_id','users.image','forumtopiccomments.created_at')->get();


        foreach ($finddetails as $value) {
              if(!empty($value->image)){
                 $value->image = url('/').'/public/'.$value->image;   
              } else {
                 $value->image = url('/public/')."/img/user_signup.png";
              }
              $value->fullname = "ASEC English:".$value->user_id;
              $value->thread_id = (string)$value->thread_id;

              $replylist = Reply::where('thread_id',$value->thread_id)->get();

               $reply_array = array();
              foreach($replylist as $valuenew) {
                  $otheruserdetails = User::find($valuenew->user_id);
                  if(!empty($otheruserdetails->image)){
                         $otheruserdetails->image = url('/').'/public/'.$otheruserdetails->image;   
                      } else {
                         $otheruserdetails->image = url('/public/')."/img/user_signup.png";
                      }
                   $valuenew->user_id = (string)$valuenew->user_id;
                   $otheruserdetails->fullname = "ASEC English:".$otheruserdetails->id;
                   $varnewcoll = $valuenew->created_at->format('Y-m-d H:i:s');

                   $valuenewhasfdfashd = Carbon::parse($varnewcoll)->diffForHumans();   
                  $reply_array[] = array("user_id"=>$valuenew->user_id,"image"=>$otheruserdetails->image,"msg"=>$valuenew->msg,"fullname"=>$otheruserdetails->fullname,"created_at"=>$valuenewhasfdfashd);
              }

              $value->replylist = $reply_array;
              $value->user_id = (string)$value->user_id;

              $value->created_at = Carbon::parse($value->created_at)->diffForHumans();
        }

         return $this->sendResponse(['topic_id'=>(string)$id,'join_flag'=>"0",'comments'=>$finddetails], 'Comment list retrieved successfully' ,$request->path()); 
    }

    public function exptoken(Request $request,$id)
    {
       $findtokencolm = Token::where('user_id',$id)->first();

        $msgg = "logout";

           $return_arraynew = array("course_id"=>"","viewlink"=>"","msg"=>$msgg,"title"=>"You are login on other device","image"=>"","time"=>date("Y-m-d h:i:s"));

             
                    if($findtokencolm->deviceType == 'android')
              {
                    
                   
                $this->android_push($findtokencolm->deviceToken,$msgg,"tokenexpire",1,$return_arraynew);
                   
              }

        return $this->sendResponse(['status'=>'success'], 'View count successfully',$request->path());      
    }
    public function downloadPDF(Request $request,$id)
    {

      $user = Bookorder::find($id);

      $userdetails = User::find($user->user_id);

      if(!empty($userdetails)){
        $fullname = $userdetails->fullname;
      } else {
        $fullname = "";
      }

      $pdf = PDF::loadView('shipment', compact('user','fullname'));
      return $pdf->download('shimpmentdetails.pdf');

    }

    public function downloadPDFall(Request $request)
    {

      $user = Bookorder::join('books','bookorders.book_id','=','books.id')->join('users','bookorders.user_id','=','users.id')->whereIn('bookorders.status',array('1','0'))->select('users.fullname','bookorders.address1','bookorders.address2','bookorders.city','bookorders.pincode','books.title')->get();

     

      $pdf = PDF::loadView('shipment1', compact('user'));
      return $pdf->download('all_shimpmentdetails.pdf');

    }

    public function viewcount(Request $request,$id1,$id2)
    {
        $details = View::where(['user_id'=>$id1,"instance_id"=>$id2])->count();
        
        if($details > 2){
            // dd($details);
            return $this->sendError($request->path(),'Your view limit has been over');
        } else {
        
            return $this->sendResponse(['status'=>'success'], 'View count successfully',$request->path());
        }
    }

     public function viewcount1(Request $request,$id1,$id2)
    {
        $details = View::where(['user_id'=>$id1,"instance_id"=>$id2])->count();
       // $courseiduuu = "";
        //dd(strpos($id2,"live"));
        if(strpos($id2,"live") > -1){
                $makeflag = "live";
                $latestinstanceid = str_replace("live","",$id2);
                $findcourseidunique = DB::table('coursemodules')->where(['id'=>$latestinstanceid])->first(); 
                $courseiduuu = explode(",", $findcourseidunique->mapid)[0];

                $coursedetailsbyc1 = DB::table('paidcourses')->where(['id'=>$courseiduuu])->first();

                  if(!empty($coursedetailsbyc1)){
                     $viewcounttotal = $coursedetailsbyc1->view_count - 2;
                  } else {
                     $viewcounttotal = 1;
                  }

            } else {
                 $makeflag = "";
                $latestinstanceid = str_replace("stream","",$id2); 
                $othdetails_vcount = DB::table('uplv')->where('id',$latestinstanceid)->first();

                if(!empty($othdetails_vcount)){
                  $courseiduuu = $othdetails_vcount->course_id;  
                  $allcoursedetails = DB::table('paidcourses')->where('id',$othdetails_vcount->course_id)->first();

                  if(!empty($allcoursedetails)){
                     $viewcounttotal = $allcoursedetails->view_count - 2;
                  } else {
                     $viewcounttotal = 1;
                  }
                } else {
                    $courseiduuu = "";
                   $viewcounttotal = 1;
                } 
            }
        
        if($details > $viewcounttotal){
          //  dd($details);
            $insertarray = array("user_id"=>$id1,"instance_id"=>$id2,"course_id"=>$courseiduuu);
            View::create($insertarray);
            if(!empty($makeflag)){
                $totalcountofunique = View::where(['user_id'=>$id1,"course_id"=>$courseiduuu])->count();
                
                $numberofmodule = sizeof(explode(",",$coursedetailsbyc1->modules_id))*$coursedetailsbyc1->view_count - sizeof(explode(",",$coursedetailsbyc1->modules_id));
               // dd($numberofmodule);
                if($totalcountofunique >= $numberofmodule){
                     $detailsdetails = View::where(['user_id'=>$id1,"course_id"=>$courseiduuu])->get();
                     foreach ($detailsdetails as $value2) {
                          $tempdetails = View::find($value2->id);

                          $tempdetails->delete();
                     }
                    
                     $findpaidcoursedlete = Mycourse::where(['type'=>"paid",'user_id'=>$id1,"course_id"=>$courseiduuu])->first();
                     
                     $findpaidcoursedlete->delete();
                }
            } 
            return $this->sendError($request->path(),'Your view limit has been over');
        } else {
            $insertarray = array("user_id"=>$id1,"instance_id"=>$id2,"course_id"=>$courseiduuu);
            View::create($insertarray);

            return $this->sendResponse(['status'=>'success'], 'View count successfully',$request->path());
        }
    }

    public function paymentnotification(Request $request,$id,$type,$other)
    {
        $details = Token::where('user_id',$id)->first();

        $msgg = $type;

          $userdetailsnotifystatus = User::find($id);

           $return_arraynew = array("course_id"=>"","viewlink"=>"","msg"=>$msgg,"title"=>$other,"image"=>"","time"=>date("Y-m-d h:i:s"));

               if(!empty($userdetailsnotifystatus)){
                    if($details->deviceType == 'android' && $userdetailsnotifystatus->notify_status == "1")
              {
                    
                   
                $this->android_push($details->deviceToken,$msgg,"payment",1,$return_arraynew);
                   
              }

          }

            return $this->sendResponse([], 'Invoice retrieved successfully',$request->path()); 
    }

    public function invoicesender(Request $request,$id)
    {
      $billdetails = DB::table('payments')->where('id',$id)->first();

      if(!empty($billdetails)){

           

           if($billdetails->type == "book"){
           // dd($billdetails->type);
              $findbookid = DB::table('bookorders')->where('id',$billdetails->agains_id)->first();
              $coursedetails = DB::table('books')->where('id',$findbookid->book_id)->first();
           } else {
              $coursedetails = DB::table('paidcourses')->where('id',$billdetails->agains_id)->first();
           }

           $userdeatils = User::find($billdetails->user_id);

           $data = array("flag"=>"","billdata"=>$billdetails,"coursedata"=>$coursedetails,"userdata"=>$userdeatils);

        //dd($data);

        $pdf = PDF::loadView('invoice', $data);

        $randfilename = time()."invoicebyasec.pdf";

        file_put_contents("public/admitcard/".$randfilename, $pdf->output());


        $otp = url("public/admitcard/")."/".$randfilename;

          $email = $userdeatils->email;
           // $email = "pratap11191@gmail.com";
                   $subject = "Asec Invoice";
                 
                $postData ="";
                 try{
                     Mail::send('emails.otpverify4', ['otp' =>$otp], function($message) use ($postData,$email)
                                {
                                  $message->from('asecclass@asecenglish.awsapps.com', 'TMASEC');
                                  $message->to($email, 'TMASEC')->subject('Asec Invoice');
                                });

                    

                 }
                  catch(Exception $e){
                                   
                                } 

              return $this->sendResponse([], 'Invoice retrieved successfully',$request->path());                     
      }
    }

   

    public function notifylist(Request $request,$id)
    {
        
        $input['user_id'] = $id;
       
        $notifydatalist = Notification::where(['receiver_id'=>$input['user_id']])->orderBy('id', 'DESC')->take(20)->get();

        $data_array = array();

        foreach($notifydatalist->toArray() as $value){
            
             $value['time'] =  Carbon::parse($value['created_at'])->diffForHumans();

             $data_array[] = $value;

        }

       

        Notification::where(['receiver_id'=>$input['user_id']])->update(['status' => "1"]);


         return $this->sendResponse($data_array, 'Notification list retrieved successfully',$request->path());



    }

    public function newbooknotify(Request $request,$id)
    {
        $testdetails = DB::table('books')->where('id',$id)->first();

        $alltokens = Token::get();

        $msgg = $testdetails->description;
       
        $return_arraynew = array("course_id"=>"","viewlink"=>"","msg"=>$msgg,"title"=>$testdetails->title,"image"=>"","time"=>date("Y-m-d h:i:s"));

        $create_time = date("Y-m-d h:i:s");



        foreach ($alltokens as $value) {

               $insertrecord = array("receiver_id"=>$value->user_id,"msg"=>$msgg,"create_time"=>$create_time);

               Notification::create($insertrecord);

               $userdetailsnotifystatus = User::find($value->user_id);

               if(!empty($userdetailsnotifystatus)){
                    if($value->deviceType == 'android' && $userdetailsnotifystatus->notify_status == "1")
              {
                    
                   
                $this->android_push($value->deviceToken,$msgg,"asec",1,$return_arraynew);
                   
              }

               }

              
        }

        return $this->sendResponse(['status'=>'success'], 'Book create notification sent successfully',$request->path()); 
    }

    public function newpaidnotify(Request $request,$id)
    {
        $testdetails = DB::table('paidcourses')->where('id',$id)->first();

        $alltokens = Token::get();

        $msgg = $testdetails->description;
       
        $return_arraynew = array("course_id"=>"","viewlink"=>"","msg"=>$msgg,"title"=>$testdetails->title,"image"=>"","time"=>date("Y-m-d h:i:s"));

        $create_time = date("Y-m-d h:i:s");

        foreach ($alltokens as $value) {

               $insertrecord = array("receiver_id"=>$value->user_id,"msg"=>$msgg,"create_time"=>$create_time);

               Notification::create($insertrecord);

               $userdetailsnotifystatus = User::find($value->user_id);

               if(!empty($userdetailsnotifystatus)){
                    if($value->deviceType == 'android' && $userdetailsnotifystatus->notify_status == "1")
              {
                    
                   
                $this->android_push($value->deviceToken,$msgg,"asec",1,$return_arraynew);
                   
              }

               }

        }

        return $this->sendResponse(['status'=>'success'], 'Paid course create notification sent successfully',$request->path()); 
    }

    public function newfreenotify(Request $request,$id)
    {
        $testdetails = DB::table('freecourses')->where('id',$id)->first();

        $alltokens = Token::get();

         $msgg = $testdetails->description;
       
        $return_arraynew = array("course_id"=>"","viewlink"=>"","msg"=>$msgg,"title"=>$testdetails->title,"image"=>"","time"=>date("Y-m-d h:i:s"));

        $create_time = date("Y-m-d h:i:s");

        foreach ($alltokens as $value) {

               $insertrecord = array("receiver_id"=>$value->user_id,"msg"=>$msgg,"create_time"=>$create_time);

               Notification::create($insertrecord);

                $userdetailsnotifystatus = User::find($value->user_id);

              if(!empty($userdetailsnotifystatus)){
                    if($value->deviceType == 'android' && $userdetailsnotifystatus->notify_status == "1")
              {
                    
                   
                $this->android_push($value->deviceToken,$msgg,"asec",1,$return_arraynew);
                   
              }

               }

        }

        return $this->sendResponse(['status'=>'success'], 'Free course created notification sent successfully',$request->path()); 
    }

    public function testcreatenotify(Request $request,$id)
    {
        $testdetails = DB::table('tests')->where('id',$id)->first();

        $alltokens = Token::get();

        $msgg = "New Test awaited for you";
       
        $return_arraynew = array("course_id"=>"","viewlink"=>"","msg"=>$msgg,"title"=>$testdetails->test_name,"image"=>"","time"=>date("Y-m-d h:i:s"));

        $create_time = date("Y-m-d h:i:s");

        foreach ($alltokens as $value) {
              $insertrecord = array("receiver_id"=>$value->user_id,"msg"=>$msgg,"create_time"=>$create_time);

               Notification::create($insertrecord);
              $userdetailsnotifystatus = User::find($value->user_id);
                if(!empty($userdetailsnotifystatus)){
              if($value->deviceType == 'android' && $userdetailsnotifystatus->notify_status == "1")
              {
                    
                   
                $this->android_push($value->deviceToken,$msgg,"testcreate",1,$return_arraynew);
                   
              }
            }
        }

        return $this->sendResponse(['status'=>'success'], 'Test create notification sent successfully',$request->path()); 
    }


   public function livestartlec(Request $request,$id)
   {
       $coursedetails = DB::table('livestreaming')->where('id',$id)->first();

       $alldetails = DB::table('mycourses')->where(['course_id'=>$coursedetails->course_id,'type'=>'paid'])->pluck('user_id');

     
       
         $msgg = "Live streaming now";

       $return_arraynew = array("course_id"=>$coursedetails->course_id,"viewlink"=>$coursedetails->viewlink,"msg"=>$msgg,"title"=>$coursedetails->title,"image"=>"","time"=>date("Y-m-d h:i:s"));

       $alltokens = Token::whereIn('user_id',$alldetails)->get();

       $create_time = date("Y-m-d h:i:s");
       
     

        foreach ($alltokens as $value) {
              $insertrecord = array("receiver_id"=>$value->user_id,"msg"=>$msgg,"create_time"=>$create_time);

               Notification::create($insertrecord);
               $userdetailsnotifystatus = User::find($value->user_id);
                 if(!empty($userdetailsnotifystatus)){
              if($value->deviceType == 'android' && $userdetailsnotifystatus->notify_status == "1")
              {
                    
                   
                $this->android_push($value->deviceToken,$msgg,"livestart",1,$return_arraynew);
                   
              }
            }
        }

        return $this->sendResponse(['status'=>'success'], 'Live start streaming notification sent successfully',$request->path()); 
   }

   public function livestartleccreate(Request $request,$id)
   {
      $coursedetails = DB::table('livestreaming')->where('id',$id)->first();

       $alldetails = DB::table('mycourses')->where(['course_id'=>$coursedetails->course_id,'type'=>'paid'])->pluck('user_id');
       
         $msgg = "A live lecture has been schedule on ".date("Y-m-d h:i:s",strtotime($coursedetails->startdatetime));

       $return_arraynew = array("course_id"=>$coursedetails->course_id,"viewlink"=>$coursedetails->viewlink,"msg"=>$msgg,"title"=>$coursedetails->title,"image"=>"","time"=>date("Y-m-d h:i:s"));

      // dd($return_arraynew);

       $alltokens = Token::whereIn('user_id',$alldetails)->get();
       
       $create_time = date("Y-m-d h:i:s"); 

        foreach ($alltokens as $value) {
               $insertrecord = array("receiver_id"=>$value->user_id,"msg"=>$msgg,"create_time"=>$create_time);

               Notification::create($insertrecord);
               $userdetailsnotifystatus = User::find($value->user_id);
                 if(!empty($userdetailsnotifystatus)){
              if($value->deviceType == 'android' && $userdetailsnotifystatus->notify_status == "1")
              {
                    
                   
                $this->android_push($value->deviceToken,$msgg,"livecreate",1,$return_arraynew);
                   
              }
            }
        }

        return $this->sendResponse(['status'=>'success'], 'Live streaming create notification sent successfully',$request->path()); 
   }
   
    public function livestreambycourse(Request $request,$evid,$newtoken,$otherid)
    {
        $check_token = Token::where(['user_id'=>$otherid,'token'=>$newtoken])->first();

        if (empty($check_token)) {
            return $this->sendError($request->path(),'Login Token Expire');
        }

         $demostatus = array("0","1");
         $alldetails = DB::table("livestreaming")->where(['course_id'=>$evid])->whereIn('streaming_status',$demostatus)->get();

          return $this->sendResponse($alldetails, 'Live stream retrieved successfully',$request->path());    
    }

    public function myPDF(Request $request,$id,$eventid)
    {

        $data1 = User::find($id);
        
        //dd($data);

        $newdata = DB::table('doubts')->where('id',$eventid)->first();

        if(!empty($data1->image)){
           $datanameimage = url('/public/').$data1->image;
        } else {
        
          $datanameimage = url('/public/')."/img/user_signup.png";
        }


        $data = array("data"=>$data1,"newdata"=>$newdata,"datanameimage"=>$datanameimage);

        //dd($data);

        $pdf = PDF::loadView('admitcard', $data);

        $randfilename = time()."admit_card.pdf";

        file_put_contents("public/admitcard/".$randfilename, $pdf->output());

        $newarraydetails = array("user_id"=>$id,"event_id"=>$eventid,"filelink"=>url("public/admitcard/")."/".$randfilename);

        Sessionjoin::create($newarraydetails);

        $otp = url("public/admitcard/")."/".$randfilename;

          $email = $data1->email;

                   $subject = "Asec Admit Card";
                 
                $postData ="";
                 try{
                     Mail::send('emails.otpverify1', ['otp' =>$otp], function($message) use ($postData,$email)
                                {
                                  $message->from('asecclass@asecenglish.awsapps.com', 'TMASEC');
                                  $message->to($email, 'TMASEC')->subject('TMASEC Admit Card');
                                });

                    

                 }
                  catch(Exception $e){
                                   
                                } 

        return $this->sendResponse(["admitcard"=>url("public/admitcard/")."/".$randfilename], 'Doubtsesion retrieved successfully',$request->path());  

    }
    
    public function faqandhelp(Request $request)
    {
        $allfaq = DB::table('faqs')->select("query","ans")->get();

        return $this->sendResponse($allfaq, 'Help and Faq retrieved successfully',$request->path()); 


    }

    public function testpdf(Request $request)
    {
        $input = $request->all();

        $tem_array1w = explode(",",$input['ans_record']);
        $dbac = Attempt::join('questions','attempts.question_id','=','questions.id')->whereIn('attempts.id',$tem_array1w)->select("questions.title","questions.option_a","questions.option_b","questions.option_c","questions.option_d","questions.option_e","attempts.user_ans","questions.explanation","questions.ans")->get();

        $testtitle11 = DB::table('tests')->where('id',$input['test_id'])->first();

        $testtitle = $testtitle11->test_name;

        $userdetails = User::find($input['user_id']);

        $data = array("dbac"=>$dbac,"testtitle"=>$testtitle);
        //dd($data);
        $pdf = PDF::loadView('testpdf', $data);

        $randfilename = time()."test_sheet.pdf";

        file_put_contents("public/testresult/".$randfilename, $pdf->output());

        if(!empty($userdetails)){
            $email = $userdetails->email;

            $otp = url("public/testresult/")."/".$randfilename;

            $subject = "Asec Test Result";
                 
                $postData ="";
                 try{
                     Mail::send('emails.otpverify1', ['otp' =>$otp], function($message) use ($postData,$email)
                                {
                                  $message->from('asecclass@asecenglish.awsapps.com', 'TMASEC');
                                  $message->to($email, 'TMASEC')->subject('TMASEC Test Result');
                                });

                    

                 }
                  catch(Exception $e){
                                   
                                }
        }

        

        

        return $this->sendResponse(['url'=>url("public/testresult/")."/".$randfilename,"testtitle"=>$testtitle], 'Test result Pdf successfully created',$request->path());
    }

    public function notifystatus(Request $request)
    {
         $input = $request->all();

         $validator = Validator::make($input,[
            'token'=>'required',
            'user_id'=>'required',
            ]);


        if($validator->fails()){
            return $this->sendError($request->path(),$validator->errors()->first());       
        }

        $check_token = Token::where(['user_id'=>$input['user_id'],'token'=>$input['token']])->first();

        if (empty($check_token)) {
            return $this->sendError($request->path(),'Login Token Expire');
        }

        $userdetails = User::find($input['user_id']);

        if($userdetails->notify_status == "0"){
            $userdetails->notify_status = "1";
            $userdetails->save();    
            return $this->sendResponse(['status'=>"1"], 'Notification status change successfully',$request->path());

        } else {
            $userdetails->notify_status = "0";
             $userdetails->save();  
               return $this->sendResponse(['status'=>"0"], 'Notification status change successfully',$request->path());
        }


    }

    public function doubtsesion(Request $request)
    {
        $detailsother = DB::table('doubts')->orderby('id','desc')->first();

        $detailsother->id = (string)$detailsother->id;

        return $this->sendResponse($detailsother, 'Doubtsesion retrieved successfully',$request->path());   
    }

    public function doubtsesionadmitcard(Request $request)
    {
        $detailsother = DB::table('doubts')->orderby('id','desc')->first();

        $detailsother->id = (string)$detailsother->id;

        return $this->sendResponse($detailsother, 'Doubtsesion retrieved successfully',$request->path());   
    }

    public function myorder(Request $request,$id)
    {
        $details = Bookorder::join('books','bookorders.book_id','=','books.id')->where(['bookorders.user_id'=>$id])->select("bookorders.id as order_id","books.title","books.image","books.price","bookorders.updated_at as order_date","bookorders.status","bookorders.address1","bookorders.address2","bookorders.city","bookorders.pincode","bookorders.created_at as shipment_date")->get();

        $detailspast = array();

        $detailsongoing = array();
       // dd($details);
        foreach ($details as $value) {
            if($value->status != '5'){

            $value->ordernumber = "ASECORDER".$value->order_id;
            $value->order_date = date("d M Y",strtotime($value->order_date));
            $value->shipment_date = date("d M Y",strtotime($value->shipment_date));
            $value->address1 = $value->address1." ".$value->address2." ".$value->city." ".$value->pincode;
            $value->qty = "1";
            //dd($value);
            if($value->status == '3'){
                $value->status_name = "Shipped";
                $detailspast[] = $value;
            } else if($value->status == '2'){
                $value->status_name = "Packaged";
                $detailsongoing[] = $value;
            } else if($value->status == '1'){
                $value->status_name = "Payment";
                $detailsongoing[] = $value;
            } else {
                $value->status_name = "Order";
                $detailsongoing[] = $value;
            }
            }
        }

        return $this->sendResponse(['past_order'=>$detailspast,'ongoing_order'=>$detailsongoing], 'My Order list retrieved successfully',$request->path());
    }
    
    public function helpus(Request $request)
    {
        $input = $request->all();

         $validator = Validator::make($input,[
            'token'=>'required',
            'user_id'=>'required',
            'msg'=>'required',
            ]);


        if($validator->fails()){
            return $this->sendError($request->path(),$validator->errors()->first());       
        }

        $check_token = Token::where(['user_id'=>$input['user_id'],'token'=>$input['token']])->first();

        if (empty($check_token)) {
            return $this->sendError($request->path(),'Login Token Expire');
        }

        Help::create($input);



       return $this->sendResponse(['status'=>'success'], 'Help Message sent successfully',$request->path());

    }

    public function reply(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input,[
            'token'=>'required',
            'user_id'=>'required',
            'msg'=>'required',
            'thread_id'=>'required'
            ]);


        if($validator->fails()){
            return $this->sendError($request->path(),$validator->errors()->first());       
        }

        $check_token = Token::where(['user_id'=>$input['user_id'],'token'=>$input['token']])->first();

        if (empty($check_token)) {
            return $this->sendError($request->path(),'Login Token Expire');
        }

        Reply::create($input);

        return $this->sendResponse(['status'=>'success'], 'Message sent successfully',$request->path());

    }
    
    public function checkcupon(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'token'=>'required',
            'user_id'=>'required',
            'cupon_code' => 'required',
            'course_id'=>'required',
        ]);


        if($validator->fails()){
            return $this->sendError($request->path(),$validator->errors()->first());       
        }

        $check_token = Token::where(['user_id'=>$input['user_id'],'token'=>$input['token']])->first();

        if (empty($check_token)) {
            return $this->sendError($request->path(),'Login Token Expire');
        }


        $checkcupons = DB::table('cupons')->where(['code'=>$input['cupon_code'],'usedby'=>"0"])->first();

        if(!empty($checkcupons)){
           
           $arraytoinsert_record = array("course_id"=>$input['course_id'],"type"=>"paid","user_id"=>$input["user_id"]);

           Mycourse::create($arraytoinsert_record);

           DB::table('cupons')->where('id',$checkcupons->id)->update(['usedby'=>$input["user_id"]]);


           $coursedetails = DB::table('paidcourses')->where('id',$input['course_id'])->first();
           

           $userdeatils = User::find($input["user_id"]);  

           $billdetails = array("paiddate"=>date("Y-m-d h:i:s"),"id"=>"BYCUPON".$input['cupon_code'],"trnx_id"=>"","type"=>"course");
           
           $data = array("flag"=>"cupon","billdata"=>$billdetails,"coursedata"=>$coursedetails,"userdata"=>$userdeatils);

          // dd($data);           

            $pdf = PDF::loadView('invoice', $data);

            $randfilename = time()."invoicebyasec.pdf";

            file_put_contents("public/admitcard/".$randfilename, $pdf->output());


            $otp = url("public/admitcard/")."/".$randfilename;

              $email = $userdeatils->email;
               // $email = "pratap11191@gmail.com";
                       $subject = "Asec Invoice";
                     
                    $postData ="";
                     try{
                         Mail::send('emails.otpverify4', ['otp' =>$otp], function($message) use ($postData,$email)
                                    {
                                      $message->from('asecclass@asecenglish.awsapps.com', 'TMASEC');
                                      $message->to($email, 'TMASEC')->subject('TMASEC Invoice');
                                    });

                        

                     }
                      catch(Exception $e){
                                       
                                    } 
           return $this->sendResponse(['status'=>'success'], 'Course added successfully',$request->path());
        } else {
             return $this->sendError($request->path(),'Cupon Expire');
        }

    }

    public function viewresult(Request $request)
    {
        $input = $request->all();

        $return_array = array();

        $correct = 0;

        $incorrect = 0;

        $total = 0;

        $bar_records = array();

        $bar_records1 = array();

        $validator = Validator::make($input, [
            'token'=>'required',
            'user_id'=>'required',
            'testresult_id' => 'required',
            'test_id'=>'required',
        ]);


        if($validator->fails()){
            return $this->sendError($request->path(),$validator->errors()->first());       
        }

        $check_token = Token::where(['user_id'=>$input['user_id'],'token'=>$input['token']])->first();

        if (empty($check_token)) {
            return $this->sendError($request->path(),'Login Token Expire');
        }

        $questiondetails = DB::table('questions')->where('test_id',$input['test_id'])->get();

        $allarrayofresult = DB::table('manageattempts')->where('id',$input['testresult_id'])->first();

        foreach ($questiondetails as $value) {
               $total++; 
               $value->remark = ucfirst($value->remark);
              $allarrayofresultallans = DB::table('attempts')->whereIn('id',explode(",",$allarrayofresult->questionids_list))->where('question_id',$value->id)->first();
             
                if(in_array($value->remark, $bar_records)){
                    $bar_records1[array_search($value->remark, $bar_records)]['section_name'] = $value->remark;

                    $bar_records1[array_search($value->remark, $bar_records)]['total_question'] = $bar_records1[array_search($value->remark, $bar_records)]['total_question'] + 1;
                } else {
                    $bar_records[] = $value->remark;

                    $bar_records1[array_search($value->remark, $bar_records)]['section_name'] = $value->remark;

                    $bar_records1[array_search($value->remark, $bar_records)]['total_question'] = 1;

                    $bar_records1[array_search($value->remark, $bar_records)]['correct_question'] = 0;
                }
                if(!empty($allarrayofresultallans)){
                     

                     if($allarrayofresultallans->user_ans != $value->ans){
                        $incorrect++;
                       
                     } else {
                        $bar_records1[array_search($value->remark, $bar_records)]['correct_question'] = $bar_records1[array_search($value->remark, $bar_records)]['correct_question'] + 1;
                        $correct++;
                     }
                }
        }

        $totalingatlast = $correct - 0.25*$incorrect;
        $return_array = array("number_of_correct"=>$correct,"number_of_incorrect"=>$incorrect,"NA"=>$total - $correct - $incorrect,"otherlisting"=>array_reverse($bar_records1),"total"=>$total,"mainresult"=>$totalingatlast);

         return $this->sendResponse($return_array, 'Test Series result retrieved successfully',$request->path());


    }

    public function viewanswers(Request $request)
    {
        $input = $request->all();

        $return_array = array();


        $validator = Validator::make($input, [
            'token'=>'required',
            'user_id'=>'required',
            'testresult_id' => 'required',
            'test_id'=>'required',
            'priorites'=>'required',
            'question_id'=>'required',
        ]);


        if($validator->fails()){
            return $this->sendError($request->path(),$validator->errors()->first());       
        }

        $check_token = Token::where(['user_id'=>$input['user_id'],'token'=>$input['token']])->first();

        if (empty($check_token)) {
            return $this->sendError($request->path(),'Login Token Expire');
        }

        $testsdetails = DB::table('tests')->where(['id'=>$input['test_id']])->first();

        $allarrayofresult = DB::table('manageattempts')->where('id',$input['testresult_id'])->first();

       

         if(empty($input['type'])){


                $finish_flag = "0";
                $start_flag = "1";
        
            $questiondetails = DB::table('questions')->where(['test_id'=>$input['test_id'],'priorites'=>'1'])->first();

            $allarrayofresultallans = DB::table('attempts')->whereIn('id',explode(",",$allarrayofresult->questionids_list))->where('question_id',$questiondetails->id)->first();

            if(!empty($allarrayofresultallans)){
                $your_answer = $this->returncorretoptionname($allarrayofresultallans->user_ans);
            } else {
                $your_answer = "";
            }

            $return_array = array("Test_description"=>$questiondetails->explanation,"name"=>$testsdetails->test_name,"question"=>$questiondetails->title,"option"=>[["option"=>$questiondetails->option_a,"option_key"=>"option_a"],["option"=>$questiondetails->option_b,"option_key"=>"option_b"],["option"=>$questiondetails->option_c,"option_key"=>"option_c"],["option"=>$questiondetails->option_d,"option_key"=>"option_d"]],"priorites"=>($input['priorites']+1),"finish_flag"=>$finish_flag,"start_flag"=>$start_flag,"question_id"=>(string)$questiondetails->id,"correct_answer"=>$this->returncorretoptionname($questiondetails->ans),"your_answer"=>$your_answer,"explanation"=>$questiondetails->explanation);
        } else if($input['type'] == 'Next'){


              if($input['priorites'] == ($testsdetails->num_of_ques - 1)){
                    $finish_flag = "1";
                    $start_flag = "0";
                    
                } else if($input['priorites'] == 0){
                    $finish_flag = "0";
                    $start_flag = "1";
                } else {
                    $finish_flag = "0";
                    $start_flag = "0";
                  
                }

            $questiondetails = DB::table('questions')->where(['test_id'=>$input['test_id'],'priorites'=>$input['priorites']+1])->first();

            $allarrayofresultallans = DB::table('attempts')->whereIn('id',explode(",",$allarrayofresult->questionids_list))->where('question_id',$questiondetails->id)->first();

            if(!empty($allarrayofresultallans)){
                $your_answer = $this->returncorretoptionname($allarrayofresultallans->user_ans);
            } else {
                $your_answer = "";
            }

            $return_array = array("Test_description"=>$questiondetails->explanation,"name"=>$testsdetails->test_name,"question"=>$questiondetails->title,"option"=>[["option"=>$questiondetails->option_a,"option_key"=>"option_a"],["option"=>$questiondetails->option_b,"option_key"=>"option_b"],["option"=>$questiondetails->option_c,"option_key"=>"option_c"],["option"=>$questiondetails->option_d,"option_key"=>"option_d"]],"priorites"=>($input['priorites']+1),"finish_flag"=>$finish_flag,"start_flag"=>$start_flag,"question_id"=>(string)$questiondetails->id,"correct_answer"=>$this->returncorretoptionname($questiondetails->ans),"your_answer"=>$your_answer,"explanation"=>$questiondetails->explanation);
        } else if($input['type'] == 'Prev'){
            
               if(($input['priorites'] - 1) == 1){
                    $finish_flag = "0";
                    $start_flag = "1";
                } else {
                    $finish_flag = "0";
                    $start_flag = "0";
                  
                }

            $questiondetails = DB::table('questions')->where(['test_id'=>$input['test_id'],'priorites'=>$input['priorites']-1])->first();

            $allarrayofresultallans = DB::table('attempts')->whereIn('id',explode(",",$allarrayofresult->questionids_list))->where('question_id',$questiondetails->id)->first();

            if(!empty($allarrayofresultallans)){
                $your_answer = $this->returncorretoptionname($allarrayofresultallans->user_ans);
            } else {
                $your_answer = "";
            }

            $return_array = array("Test_description"=>$questiondetails->explanation,"name"=>$testsdetails->test_name,"question"=>$questiondetails->title,"option"=>[["option"=>$questiondetails->option_a,"option_key"=>"option_a"],["option"=>$questiondetails->option_b,"option_key"=>"option_b"],["option"=>$questiondetails->option_c,"option_key"=>"option_c"],["option"=>$questiondetails->option_d,"option_key"=>"option_d"]],"priorites"=>($input['priorites']-1),"finish_flag"=>$finish_flag,"start_flag"=>$start_flag,"question_id"=>(string)$questiondetails->id,"correct_answer"=>$this->returncorretoptionname($questiondetails->ans),"your_answer"=>$your_answer,"explanation"=>$questiondetails->explanation);
        } 


        return $this->sendResponse($return_array, 'Test Series result retrieved successfully',$request->path());
    }

    public function returncorretoptionname($id)
    {
        if($id == 'option_a'){
            return "A";
        }

        if($id == 'option_b'){
            return "B";
        }

        if($id == 'option_c'){
            return "C";
        }

        if($id == 'option_d'){
            return "D";
        }
    }

    public function teststart(Request $request)
    {
        $input = $request->all();

        $return_array = array();


        $validator = Validator::make($input, [
            'token'=>'required',
            'user_id'=>'required',
            'test_id' => 'required',
            'priorites'=>'required',
            'question_id'=>'required',
        ]);


        if($validator->fails()){
            return $this->sendError($request->path(),$validator->errors()->first());       
        }

        $check_token = Token::where(['user_id'=>$input['user_id'],'token'=>$input['token']])->first();

        if (empty($check_token)) {
            return $this->sendError($request->path(),'Login Token Expire');
        }

        $testsdetails = DB::table('tests')->where(['id'=>$input['test_id']])->first();

       

        if(empty($input['type'])){

             if($input['priorites'] == ($testsdetails->num_of_ques - 1)){
                $finish_flag = "1";
                $start_flag = "0";
                
            } else if($input['priorites'] == 0){
                $finish_flag = "0";
                $start_flag = "1";
            } else {
                $finish_flag = "0";
                $start_flag = "0";
              
            }

            $questiondetails = DB::table('questions')->where(['test_id'=>$input['test_id'],'priorites'=>'1'])->first();
            if(!empty($questiondetails->option_e)){
                        $return_array = array("Test_description"=>$questiondetails->explanation,"name"=>$testsdetails->test_name,"question"=>$questiondetails->title,"option"=>[["option"=>$questiondetails->option_a,"option_key"=>"option_a"],["option"=>$questiondetails->option_b,"option_key"=>"option_b"],["option"=>$questiondetails->option_c,"option_key"=>"option_c"],["option"=>$questiondetails->option_d,"option_key"=>"option_d"],["option"=>$questiondetails->option_e,"option_key"=>"option_e"]],"priorites"=>($input['priorites']+1),"finish_flag"=>$finish_flag,"start_flag"=>$start_flag,"question_id"=>(string)$questiondetails->id,"ans_record"=>"","answer"=>"");
            } else {
                    $return_array = array("Test_description"=>$questiondetails->explanation,"name"=>$testsdetails->test_name,"question"=>$questiondetails->title,"option"=>[["option"=>$questiondetails->option_a,"option_key"=>"option_a"],["option"=>$questiondetails->option_b,"option_key"=>"option_b"],["option"=>$questiondetails->option_c,"option_key"=>"option_c"],["option"=>$questiondetails->option_d,"option_key"=>"option_d"]],"priorites"=>($input['priorites']+1),"finish_flag"=>$finish_flag,"start_flag"=>$start_flag,"question_id"=>(string)$questiondetails->id,"ans_record"=>"","answer"=>"");
            }
        
        } else if($input['type'] == 'Next'){
             $questiondetails = DB::table('questions')->where(['test_id'=>$input['test_id'],'priorites'=>$input['priorites']+1])->first();

              if($input['priorites'] == ($testsdetails->num_of_ques - 1)){
                    $finish_flag = "1";
                    $start_flag = "0";
                    
                } else if($input['priorites'] == 0){
                    $finish_flag = "0";
                    $start_flag = "1";
                } else {
                    $finish_flag = "0";
                    $start_flag = "0";
                  
                }

            if(empty($input['answer'])){
                 if(empty($input['ans_record'])){
                      if(!empty($questiondetails->option_e)){
                          $return_array = array("Test_description"=>$questiondetails->explanation,"name"=>$testsdetails->test_name,"question"=>$questiondetails->title,"option"=>[["option"=>$questiondetails->option_a,"option_key"=>"option_a"],["option"=>$questiondetails->option_b,"option_key"=>"option_b"],["option"=>$questiondetails->option_c,"option_key"=>"option_c"],["option"=>$questiondetails->option_d,"option_key"=>"option_d"],["option"=>$questiondetails->option_e,"option_key"=>"option_e"]],"priorites"=>($input['priorites']+1),"finish_flag"=>$finish_flag,"start_flag"=>$start_flag,"question_id"=>(string)$questiondetails->id,"ans_record"=>"","answer"=>"");
                      } else {
                        $return_array = array("Test_description"=>$questiondetails->explanation,"name"=>$testsdetails->test_name,"question"=>$questiondetails->title,"option"=>[["option"=>$questiondetails->option_a,"option_key"=>"option_a"],["option"=>$questiondetails->option_b,"option_key"=>"option_b"],["option"=>$questiondetails->option_c,"option_key"=>"option_c"],["option"=>$questiondetails->option_d,"option_key"=>"option_d"]],"priorites"=>($input['priorites']+1),"finish_flag"=>$finish_flag,"start_flag"=>$start_flag,"question_id"=>(string)$questiondetails->id,"ans_record"=>"","answer"=>"");
                      }
                    
                } else {
                    $tem_array = explode(",", $input['ans_record']);
                    $findthisquestionrecord = Attempt::whereIn('id',$tem_array)->where('question_id',$input['question_id'])->first();

                    $findanswerofques = Attempt::whereIn('id',$tem_array)->where('question_id',$questiondetails->id)->first();

                    if(!empty($findanswerofques)){
                        $ansdrerfddf = $findanswerofques->user_ans;
                    } else {
                        $ansdrerfddf = "";
                    }

                    if(!empty($findthisquestionrecord)){
                        unset($tem_array[array_search($findthisquestionrecord->id, $tem_array)]);
                        $findthisquestionrecord->delete();
                        if(!empty($questiondetails->option_e)){
                             $return_array = array("Test_description"=>$questiondetails->explanation,"name"=>$testsdetails->test_name,"question"=>$questiondetails->title,"option"=>[["option"=>$questiondetails->option_a,"option_key"=>"option_a"],["option"=>$questiondetails->option_b,"option_key"=>"option_b"],["option"=>$questiondetails->option_c,"option_key"=>"option_c"],["option"=>$questiondetails->option_d,"option_key"=>"option_d"],["option"=>$questiondetails->option_e,"option_key"=>"option_e"]],"priorites"=>($input['priorites']+1),"finish_flag"=>$finish_flag,"start_flag"=>$start_flag,"question_id"=>(string)$questiondetails->id,"ans_record"=>implode(",", $tem_array),"answer"=>$ansdrerfddf);
                        } else {
                           $return_array = array("Test_description"=>$questiondetails->explanation,"name"=>$testsdetails->test_name,"question"=>$questiondetails->title,"option"=>[["option"=>$questiondetails->option_a,"option_key"=>"option_a"],["option"=>$questiondetails->option_b,"option_key"=>"option_b"],["option"=>$questiondetails->option_c,"option_key"=>"option_c"],["option"=>$questiondetails->option_d,"option_key"=>"option_d"]],"priorites"=>($input['priorites']+1),"finish_flag"=>$finish_flag,"start_flag"=>$start_flag,"question_id"=>(string)$questiondetails->id,"ans_record"=>implode(",", $tem_array),"answer"=>$ansdrerfddf);
                        }
                       
                    } else {
                        if(!empty($questiondetails->option_e)){
                               $return_array = array("Test_description"=>$questiondetails->explanation,"name"=>$testsdetails->test_name,"question"=>$questiondetails->title,"option"=>[["option"=>$questiondetails->option_a,"option_key"=>"option_a"],["option"=>$questiondetails->option_b,"option_key"=>"option_b"],["option"=>$questiondetails->option_c,"option_key"=>"option_c"],["option"=>$questiondetails->option_d,"option_key"=>"option_d"],["option"=>$questiondetails->option_e,"option_key"=>"option_e"]],"priorites"=>($input['priorites']+1),"finish_flag"=>$finish_flag,"start_flag"=>$start_flag,"question_id"=>(string)$questiondetails->id,"ans_record"=>implode(",", $tem_array),"answer"=>$ansdrerfddf);
                          } else {
                             $return_array = array("Test_description"=>$questiondetails->explanation,"name"=>$testsdetails->test_name,"question"=>$questiondetails->title,"option"=>[["option"=>$questiondetails->option_a,"option_key"=>"option_a"],["option"=>$questiondetails->option_b,"option_key"=>"option_b"],["option"=>$questiondetails->option_c,"option_key"=>"option_c"],["option"=>$questiondetails->option_d,"option_key"=>"option_d"]],"priorites"=>($input['priorites']+1),"finish_flag"=>$finish_flag,"start_flag"=>$start_flag,"question_id"=>(string)$questiondetails->id,"ans_record"=>implode(",", $tem_array),"answer"=>$ansdrerfddf);
                          }

                        
                    }
                }
            } else {
                if(empty($input['ans_record'])){
                    $insertans_array = array("question_id"=>$input['question_id'],"user_id"=>$input['user_id'],"user_ans"=>$input['answer']);

                    $inserted_id = Attempt::create($insertans_array);
                     if(!empty($questiondetails->option_e)){
                            $return_array = array("Test_description"=>$questiondetails->explanation,"name"=>$testsdetails->test_name,"question"=>$questiondetails->title,"option"=>[["option"=>$questiondetails->option_a,"option_key"=>"option_a"],["option"=>$questiondetails->option_b,"option_key"=>"option_b"],["option"=>$questiondetails->option_c,"option_key"=>"option_c"],["option"=>$questiondetails->option_d,"option_key"=>"option_d"],["option"=>$questiondetails->option_e,"option_key"=>"option_e"]],"priorites"=>($input['priorites']+1),"finish_flag"=>$finish_flag,"start_flag"=>$start_flag,"question_id"=>(string)$questiondetails->id,"ans_record"=>implode(",", array($inserted_id->id)),"answer"=>"");
                          } else {
                            $return_array = array("Test_description"=>$questiondetails->explanation,"name"=>$testsdetails->test_name,"question"=>$questiondetails->title,"option"=>[["option"=>$questiondetails->option_a,"option_key"=>"option_a"],["option"=>$questiondetails->option_b,"option_key"=>"option_b"],["option"=>$questiondetails->option_c,"option_key"=>"option_c"],["option"=>$questiondetails->option_d,"option_key"=>"option_d"]],"priorites"=>($input['priorites']+1),"finish_flag"=>$finish_flag,"start_flag"=>$start_flag,"question_id"=>(string)$questiondetails->id,"ans_record"=>implode(",", array($inserted_id->id)),"answer"=>"");
                          }
                    
                } else {
                    $tem_array = explode(",", $input['ans_record']);
                    $findthisquestionrecord = Attempt::whereIn('id',$tem_array)->where('question_id',$input['question_id'])->first();

                    $findanswerofques = Attempt::whereIn('id',$tem_array)->where('question_id',$questiondetails->id)->first();

                    if(!empty($findanswerofques)){
                        $ansdrerfddf = $findanswerofques->user_ans;
                    } else {
                        $ansdrerfddf = "";
                    }

                    if(!empty($findthisquestionrecord)){
                        $findthisquestionrecord->user_ans = $input['answer'];
                        $findthisquestionrecord->save();
                         if(!empty($questiondetails->option_e)){
                                $return_array = array("Test_description"=>$questiondetails->explanation,"name"=>$testsdetails->test_name,"question"=>$questiondetails->title,"option"=>[["option"=>$questiondetails->option_a,"option_key"=>"option_a"],["option"=>$questiondetails->option_b,"option_key"=>"option_b"],["option"=>$questiondetails->option_c,"option_key"=>"option_c"],["option"=>$questiondetails->option_d,"option_key"=>"option_d"],["option"=>$questiondetails->option_e,"option_key"=>"option_e"]],"priorites"=>($input['priorites']+1),"finish_flag"=>$finish_flag,"start_flag"=>$start_flag,"question_id"=>(string)$questiondetails->id,"ans_record"=>implode(",", $tem_array),"answer"=>$ansdrerfddf);
                          } else {
                              $return_array = array("Test_description"=>$questiondetails->explanation,"name"=>$testsdetails->test_name,"question"=>$questiondetails->title,"option"=>[["option"=>$questiondetails->option_a,"option_key"=>"option_a"],["option"=>$questiondetails->option_b,"option_key"=>"option_b"],["option"=>$questiondetails->option_c,"option_key"=>"option_c"],["option"=>$questiondetails->option_d,"option_key"=>"option_d"]],"priorites"=>($input['priorites']+1),"finish_flag"=>$finish_flag,"start_flag"=>$start_flag,"question_id"=>(string)$questiondetails->id,"ans_record"=>implode(",", $tem_array),"answer"=>$ansdrerfddf); 
                          }
                      
                    } else {
                        $insertans_array = array("question_id"=>$input['question_id'],"user_id"=>$input['user_id'],"user_ans"=>$input['answer']);

                         $inserted_id = Attempt::create($insertans_array);

                         $tem_array[] = $inserted_id->id;
                          if(!empty($questiondetails->option_e)){
                              $return_array = array("Test_description"=>$questiondetails->explanation,"name"=>$testsdetails->test_name,"question"=>$questiondetails->title,"option"=>[["option"=>$questiondetails->option_a,"option_key"=>"option_a"],["option"=>$questiondetails->option_b,"option_key"=>"option_b"],["option"=>$questiondetails->option_c,"option_key"=>"option_c"],["option"=>$questiondetails->option_d,"option_key"=>"option_d"],["option"=>$questiondetails->option_e,"option_key"=>"option_e"]],"priorites"=>($input['priorites']+1),"finish_flag"=>$finish_flag,"start_flag"=>$start_flag,"question_id"=>(string)$questiondetails->id,"ans_record"=>implode(",", $tem_array),"answer"=>$ansdrerfddf);
                          } else {
                             $return_array = array("Test_description"=>$questiondetails->explanation,"name"=>$testsdetails->test_name,"question"=>$questiondetails->title,"option"=>[["option"=>$questiondetails->option_a,"option_key"=>"option_a"],["option"=>$questiondetails->option_b,"option_key"=>"option_b"],["option"=>$questiondetails->option_c,"option_key"=>"option_c"],["option"=>$questiondetails->option_d,"option_key"=>"option_d"]],"priorites"=>($input['priorites']+1),"finish_flag"=>$finish_flag,"start_flag"=>$start_flag,"question_id"=>(string)$questiondetails->id,"ans_record"=>implode(",", $tem_array),"answer"=>$ansdrerfddf);
                          }
                        
                    }
                }
            }
        } else if($input['type'] == 'Prev'){
         if(($input['priorites'] - 1) == 1){
                    $finish_flag = "0";
                    $start_flag = "1";
                } else {
                    $finish_flag = "0";
                    $start_flag = "0";
                  
                }

            $questiondetails = DB::table('questions')->where(['test_id'=>$input['test_id'],'priorites'=>$input['priorites']-1])->first();
            if(empty($input['ans_record'])){
                $ansdrerfddf = "";
            } else {
                $tem_array = explode(",", $input['ans_record']);
                $findanswerofques = Attempt::whereIn('id',$tem_array)->where('question_id',$questiondetails->id)->first();

                if(!empty($findanswerofques)){
                    $ansdrerfddf = $findanswerofques->user_ans;
                } else {
                    $ansdrerfddf = "";
                }

            }

           // dd($findanswerofques);

            if(empty($input['ans_record']))
            {
                $ans_new = "";
            } else {
                $ans_new = $input['ans_record'];
            }
               if(!empty($questiondetails->option_e)){
                            $return_array = array("Test_description"=>$questiondetails->explanation,"name"=>$testsdetails->test_name,"question"=>$questiondetails->title,"option"=>[["option"=>$questiondetails->option_a,"option_key"=>"option_a"],["option"=>$questiondetails->option_b,"option_key"=>"option_b"],["option"=>$questiondetails->option_c,"option_key"=>"option_c"],["option"=>$questiondetails->option_d,"option_key"=>"option_d"],["option"=>$questiondetails->option_e,"option_key"=>"option_e"]],"priorites"=>($input['priorites']-1),"finish_flag"=>$finish_flag,"start_flag"=>$start_flag,"question_id"=>(string)$questiondetails->id,"ans_record"=>$ans_new,"answer"=>$ansdrerfddf);
                          } else {
                            $return_array = array("Test_description"=>$questiondetails->explanation,"name"=>$testsdetails->test_name,"question"=>$questiondetails->title,"option"=>[["option"=>$questiondetails->option_a,"option_key"=>"option_a"],["option"=>$questiondetails->option_b,"option_key"=>"option_b"],["option"=>$questiondetails->option_c,"option_key"=>"option_c"],["option"=>$questiondetails->option_d,"option_key"=>"option_d"]],"priorites"=>($input['priorites']-1),"finish_flag"=>$finish_flag,"start_flag"=>$start_flag,"question_id"=>(string)$questiondetails->id,"ans_record"=>$ans_new,"answer"=>$ansdrerfddf);
                          }
              
        
        } else if($input['type'] == 'Submit'){
            $tem_array = array();
            // $tem_array1w = explode(",", $input['ans_record']);
            // $findthisquestionrecord1w = Attempt::whereIn('id',$tem_array1w)->get();

            // if(!empty($findthisquestionrecord1w)){

            // }

            if(empty($input['answer'])){
                 if(empty($input['ans_record'])){

                    return $this->sendError($request->path(),"You don't have attempt question");

                } else {
                    $tem_array = explode(",", $input['ans_record']);
                    $findthisquestionrecord = Attempt::whereIn('id',$tem_array)->where('question_id',$input['question_id'])->first();

                    if(!empty($findthisquestionrecord)){
                        unset($tem_array[array_search($findthisquestionrecord->id, $tem_array)]);
                        $findthisquestionrecord->delete();
                    } 
                }
            } else {
                if(empty($input['ans_record'])){
                    $insertans_array = array("question_id"=>$input['question_id'],"user_id"=>$input['user_id'],"user_ans"=>$input['answer']);

                    $inserted_id = Attempt::create($insertans_array);

                     $tem_array[] = $inserted_id->id;

                } else {
                    $tem_array = explode(",", $input['ans_record']);
                    $findthisquestionrecord = Attempt::whereIn('id',$tem_array)->where('question_id',$input['question_id'])->first();

                    if(!empty($findthisquestionrecord)){
                        $findthisquestionrecord->user_ans = $input['answer'];
                        $findthisquestionrecord->save();

                       
                    } else {
                        $insertans_array = array("question_id"=>$input['question_id'],"user_id"=>$input['user_id'],"user_ans"=>$input['answer']);

                         $inserted_id = Attempt::create($insertans_array);

                         $tem_array[] = $inserted_id->id;


                    }
                }
            }

            if(empty($tem_array)){
                return $this->sendError($request->path(),"You don't have attempt question");
            } else {
                $instefdfdsfsdf = array("user_id"=>$input['user_id'],"questionids_list"=>implode(",", $tem_array));
                $details = Manageattempt::create($instefdfdsfsdf);

                 return $this->sendResponse(['status'=>'success','testresult_id'=>(string)$details->id,'test_id'=>$input['test_id']], 'Test Series submitted successfully',$request->path());
            }
        }

         return $this->sendResponse($return_array, 'Test Series data retrieved successfully',$request->path());
    }

    public function testlist(Request $request)
    {
        $listoftest = DB::table('tests')->orderby('id','desc')->where('admin_status','1')->get();
        
        $questionrecord = DB::table('questions')->join('tests','questions.test_id','=','tests.id')->orderby('questions.id','desc')->select("tests.description as Test_description","questions.id","questions.title","questions.option_a","questions.option_b","questions.option_c","questions.option_d","questions.option_e","questions.ans","questions.priorites","questions.test_id","questions.remark","questions.explanation","tests.time")->get();

        foreach ($questionrecord as $value) {
            $option = array();
            $option[] = array("option"=>$value->option_a,"option_key"=>"option_a","ans"=>$value->ans);
            $option[] = array("option"=>$value->option_b,"option_key"=>"option_b","ans"=>$value->ans);
            $option[] = array("option"=>$value->option_c,"option_key"=>"option_c","ans"=>$value->ans);
            $option[] = array("option"=>$value->option_d,"option_key"=>"option_d","ans"=>$value->ans);
             $option[] = array("option"=>$value->option_e,"option_key"=>"option_e","ans"=>$value->ans);

            $value->option = $option;
            $value->time = 60000*$value->time;
        }

        $questionrecord = $questionrecord->toJson();

        $return_array = array();

        $check_entry = array();
       
        foreach ($listoftest as $value) {
             if(in_array($value->series_name, $check_entry)){
                $return_array[array_search($value->series_name,$check_entry)]['series_name'] = $value->series_name;
                //$return_array[array_search($value->series_name,$check_entry)]['description'] = $value->description;

                if(time() < strtotime($value->validity)){
                    $display_status ="1";
                } else {
                    $display_status ="0";
                }
                
                 $return_array[array_search($value->series_name,$check_entry)]['tests_list'] = array();
            
                    $return_array[array_search($value->series_name,$check_entry)]['tests_list'][] = array("name"=> $value->test_name,"test_id"=>(string)$value->id,"duration"=>$value->time,"validity"=>"Expire On ".(string)$value->validity,"display_status"=>$display_status,"time"=>60000*$value->time,"num_of_ques"=>$value->num_of_ques); 
                
               
             } else {
                $check_entry[] = $value->series_name;
                $return_array[array_search($value->series_name,$check_entry)]['series_name'] = $value->series_name;
               // $return_array[array_search($value->series_name,$check_entry)]['description'] = $value->description;

                if(time() < strtotime($value->validity)){
                    $display_status ="1";
                } else {
                    $display_status ="0";
                }
               
                $return_array[array_search($value->series_name,$check_entry)]['tests_list'] = array();
                
                     $return_array[array_search($value->series_name,$check_entry)]['tests_list'][] = array("name"=> $value->test_name,"test_id"=>(string)$value->id,"duration"=>$value->time,"validity"=>"Expire On ".(string)$value->validity,"display_status"=>$display_status,"time"=>60000*$value->time,"num_of_ques"=>$value->num_of_ques);
                
             }
        }
        
        //$return_array['record'] = json_decode($questionrecord);

        return $this->sendResponse3($return_array,json_decode($questionrecord), 'Test list retrieved successfully',$request->path());
    }

    public function placeorder(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'token'=>'required',
            'user_id'=>'required',
            'book_id' => 'required',
            'address1'=>'required',
            'city'=>'required',
            'pincode'=>'required',
        ]);


        if($validator->fails()){
            return $this->sendError($request->path(),$validator->errors()->first());       
        }

        $check_token = Token::where(['user_id'=>$input['user_id'],'token'=>$input['token']])->first();

        if (empty($check_token)) {
            return $this->sendError($request->path(),'Login Token Expire');
        }

        if(!empty($input['cupon_code'])){
              $checkcupons = DB::table('cupons')->where(['code'=>$input['cupon_code'],'usedby'=>"0"])->first();

                if(!empty($checkcupons)){

                   $checkcupons->usedby = "1";

                   $checkcupons->save();
                }
        }

        Bookorder::create($input);

         return $this->sendResponse(['status'=>'success'], 'Order Placed successfully',$request->path());
    }

    public function bookdetails(Request $request,$id)
    {
        $assignments = DB::table('books')->where('id',$id)->first();

       $return_array[] = array("file"=>$assignments->file,"publisher"=>ucfirst($assignments->publisher),"title"=>ucfirst($assignments->title),"description"=>ucfirst($assignments->description),"image"=>$assignments->image,"book_id"=>(string)$assignments->id,"price"=>$assignments->price);  

        return $this->sendResponse($return_array, 'Book Details retrieved successfully',$request->path());

    }

    public function booklist(Request $request,$Token,$userid)
    {
        $check_token = Token::where(['user_id'=>$userid,'token'=>$Token])->first();

        if (empty($check_token)) {
            return $this->sendError($request->path(),'Login Token Expire');
        }

        $assignments = DB::table('books')->orderby('id','asc')->get();

        $return_array = array();

        $totalvideoyoutubearray = DB::table('banners')->where('type','book')->first();

       // $return_array[] = array("youtube_link"=>$totalvideoyoutubearray->image);

        if(!empty($totalvideoyoutubearray)){
          $youtube_link = $totalvideoyoutubearray->image;
        } else {
          $youtube_link = "gsm5S5OBBEs";
        }

        foreach ($assignments as $value) {
             $return_array[] = array("title"=>ucfirst($value->title),"description"=>ucfirst($value->description),"image"=>$value->image,"book_id"=>(string)$value->id,"youtube_link"=>$youtube_link);    
        }

        return $this->sendResponse($return_array, 'Book List retrieved successfully',$request->path());

    }

    public function assignmentlist(Request $request)
    {
        $assignments = DB::table('assignments')->orderby('id','asc')->get();

        $return_array = array();

        foreach ($assignments as $value) {
             $return_array[] = array("title"=>ucfirst($value->title),"file"=>$value->file,"solution"=>$value->solution);    
        }

        return $this->sendResponse($return_array, 'Assignments List retrieved successfully',$request->path());

    }

    public function subscriptionlist(Request $request)
    {
        $listdetails = DB::table('subscriptions')->orderby('id','ASC')->get();

        $return_array = array();

        foreach ($listdetails as  $value) {
          $facilities = array();
           if($value->live_classes == 1){
              $facilities[] = "Live Classes";
           }

           if($value->live_video_on_demand == 1){
              $facilities[] = "Live Video On Demand";
           }

           if(!empty($value->course_videos)){
              $facilities[] = "Course Videos";
           }

           if($value->weekly_test == 1){
              $facilities[] = "Weekly Test";
           }
           $return_array[] = array("subscription_id"=>(string)$value->id,"title"=>ucfirst($value->title),"description"=>ucfirst($value->description),"price"=>$value->price,"facilities"=>$facilities);
        }

        return $this->sendResponse($return_array, 'Subscription List retrieved successfully',$request->path());
    }

    public function changepassword(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'token'=>'required',
            'user_id'=>'required',
            'password' => 'required|min:6',
            'oldpassword'=>'required',
        ]);


        if($validator->fails()){
            return $this->sendError($request->path(),$validator->errors()->first());       
        }

        $check_token = Token::where(['user_id'=>$input['user_id'],'token'=>$input['token']])->first();

        if (empty($check_token)) {
            return $this->sendError($request->path(),'Login Token Expire');
        }

        $post = User::find($input['user_id']);

        if($post->password != $input['oldpassword']){
            return $this->sendError($request->path(),"You enter incorrect old password"); 
        }

        $post->password = $input['password'];

        $post->save();

        return $this->sendResponse(['status'=>'success'], 'Password updated successfully.',$request->path());
    }

    public function sendotp(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'phone'=>'required',
        ]);


        if($validator->fails()){
            return $this->sendError($request->path(),$validator->errors()->first());       
        }
        
         if($this->checkphone($input['phone']) == 1){
            return $this->sendError($request->path(),"Phone Number Already Exist");
         }

        $otp = rand(1000,9999);


        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://13.233.141.118:5800/sendsms/".$otp."/".$input['phone'],
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                // Set Here Your Requesred Headers
                'Content-Type: application/json',
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

         return $this->sendResponse(['status'=>"success",'otp'=>(string)$otp,"error"=>$err], 'Verification Otp sent to your phone number successfully' ,$request->path());

        

    }

    public function paidcoursedetails(Request $request,$id,$idjk = null,$token)
    {


        $check_token = Token::where(['user_id'=>$idjk,'token'=>$token])->first();

        if (empty($check_token)) {
            return $this->sendError($request->path(),'Login Token Expire');
        }

        $getdetails = Mycourse::where(['course_id'=>$id,'user_id'=>$idjk,"type"=>"paid"])->first();

        if(!empty($getdetails)){
            $flagofc = "1";
        } else {
            $flagofc = "0";
        }

        $plist = DB::table('uplv')->where('course_id',$id)->get();

         $finalresultplive = array();
       
        if(!empty($plist)){
            foreach ($plist as $value) {

              $finalresultplive[0]['module_name'] = "Previous Live Lectures";
              $viewcounting = View::where(['user_id'=>$idjk,"instance_id"=>"stream".$value->id])->count();

              $newcount = 3 - $viewcounting;
               $finalresultplive[0]['videodetails'][] = array("flag"=>$flagofc,"instance_id"=>"stream".$value->id,"title"=>$value->title,"description"=>$value->description,"video"=>$value->video,"thumbnail"=>"https://asecenglish.com/asecapi/public/logo-admin.png","duration"=>"","count"=>(string)$newcount);
            }
            
        }
       
         $freecoursedetails = DB::table('paidcourses')->where('id',$id)->first();

        $sample_array = explode(",", $freecoursedetails->modules_id);
        //dd($sample_array);
        $findmodule_count = sizeof($sample_array) + 1;

        $finalresult = array();

        
            $ikmj = 0;
            for ($i=1; $i < $findmodule_count; $i++) { 
                  $findmoduledetailsnew = DB::table('coursemodules')->where('mapid',$id.",".$i)->get()->toArray();
                  //dd($findmoduledetailsnew);
                  foreach($findmoduledetailsnew as $value_new) {
                       $viewcountinglive = View::where(['user_id'=>$idjk,"instance_id"=>"live".$value_new->id])->count();

                       $newcountlive = 3 - $viewcountinglive;
                       $finalresult[$i - 1]['module_name'] = ucfirst($value_new->name);
                       $finalresult[$i - 1]['videodetails'][] = array("flag"=>$flagofc,"instance_id"=>"live".$value_new->id,"title"=>$value_new->title,"description"=>$value_new->description,"video"=>$value_new->video,"thumbnail"=>$value_new->thumbnail,"duration"=>gmdate("H:i:s", round($value_new->duration)),"count"=>(string)$newcountlive);

                  }
            }


        $finalresult = array_merge($finalresultplive,$finalresult);    

        
        
       

        $return_array = array("course_id"=>$id,"title"=>$freecoursedetails->title,"description"=>$freecoursedetails->description,"intro_video"=>$freecoursedetails->intro_video,"duration"=>gmdate("H:i:s", round($freecoursedetails->duration)),"thumbnail"=>$freecoursedetails->thumbnail,"coursemodules"=>$finalresult,"flag"=>$flagofc,
            "price"=>$freecoursedetails->price);

        

        return $this->sendResponse($return_array, 'Paid Course Details retrieved successfully' ,$request->path());
    }

    public function viewprofile(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'token'=>'required',
            'user_id'=>'required',
        ]);


        if($validator->fails()){
            return $this->sendError($request->path(),$validator->errors()->first());       
        }

        $check_token = Token::where(['user_id'=>$input['user_id'],'token'=>$input['token']])->first();

        if (empty($check_token)) {
            return $this->sendError($request->path(),'Login Token Expire');
        }

        $userdetails = User::find($input['user_id']);
        

        if(!empty($userdetails->image)){
           $userdetails->image = url('/public/').$userdetails->image;
        } else {
        
          $userdetails->image = url('/public/')."/img/user_signup.png";
        }

        if(empty($userdetails->dob)){
            $userdetails->dob = "";
        }

        $return_array = array("user_id"=>(string)$userdetails->id,"image"=>$userdetails->image,"fullname"=>$userdetails->fullname,"address"=>$userdetails->address,"email"=>$userdetails->email,"phone"=>$userdetails->phone,"dob"=>$userdetails->dob);

        return $this->sendResponse($return_array, 'User details retrieved successfully' ,$request->path()); 

    }

    public function editprofile(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'token'=>'required',
            'user_id'=>'required',
            'fullname'=>'required',
            'email'=>'required',
            'phone'=>'required',
            'address'=>'required',
        ]);


        if($validator->fails()){
            return $this->sendError($request->path(),$validator->errors()->first());       
        }

        $check_token = Token::where(['user_id'=>$input['user_id'],'token'=>$input['token']])->first();

        if (empty($check_token)) {
            return $this->sendError($request->path(),'Login Token Expire');
        }

        $userdetails = User::find($input['user_id']);

           if($request->image){
             
             $filename = substr( md5( $userdetails->id . '-' . time() ), 0, 15) . '.' . $request->file('image')->getClientOriginalExtension();

             $path = public_path('users-photos/' . $filename);

             Image::make($request->image)->save($path);
             
             $userdetails->image = '/users-photos/'.$filename;
           

        }

        $userdetails->fullname = $input['fullname'];

        $userdetails->address = $input['address'];

        $userdetails->phone = $input['phone'];

        $userdetails->dob = $input['dob'];

        $checkemail = User::whereNotIn('id',array($input['user_id']))->where('email',$input['email'])->first();

        if(!empty($checkemail)){
            return $this->sendError($request->path(),'Email id Already Exist');
        }

        $userdetails->email = $input['email'];

          if(!empty($userdetails->image)){
           $datanameimage = url('/public/').$userdetails->image;
        } else {
        
          $datanameimage = url('/public/')."/img/user_signup.png";
        }


        //dd($userdetails);

        $data = array("data"=>$userdetails,"datanameimage"=>$datanameimage);

        $pdf = PDF::loadView('identicard', $data);

        $randfilename = time()."identity_card.pdf";

        file_put_contents("public/admitcard/".$randfilename, $pdf->output());

        $userdetails->identicard = url("public/admitcard/")."/".$randfilename;

        $userdetails->save();

        $userdetails1 = User::find($input['user_id']);


        if(!empty($userdetails1->image)){
           $userdetails1->image = url('/public/').$userdetails1->image;
        } else {
        
          $userdetails1->image = url('/public/')."/img/user_signup.png";
        }

        $return_array = array("user_id"=>(string)$userdetails1->id,"image"=>$userdetails1->image,"fullname"=>$userdetails1->fullname,"address"=>$userdetails1->address,"email"=>$userdetails1->email,"phone"=>$userdetails1->phone);

        $otp = $userdetails1->identicard;

          $email = $userdetails1->email;

                   $subject = "Asec Identity Card";
                 
                $postData ="";
                 try{
                     Mail::send('emails.otpverify3', ['otp' =>$otp], function($message) use ($postData,$email)
                                {
                                  $message->from('asecclass@asecenglish.awsapps.com', 'TMASEC');
                                  $message->to($email, 'TMASEC')->subject('TMASEC Identity Card');
                                });

                    

                 }
                  catch(Exception $e){
                                   
                                } 

        return $this->sendResponse($return_array, 'User profile updated successfully' ,$request->path()); 
    }

    public function myfreecourse(Request $request,$id)
    {
        $getyourcourseid = Mycourse::where(['user_id'=>$id,'type'=>'free'])->orderBy('id','asc')->pluck('course_id');

         $freecoursedetails = DB::table('freecourses')->whereIn('id',$getyourcourseid)->select('id as course_id','video','title','description','thumbnail','duration')->get();

        foreach ($freecoursedetails as  $value) {
             $value->course_id = (string)$value->course_id;
             $value->duration = gmdate("H:i:s", round($value->duration));
        }

         return $this->sendResponse($freecoursedetails, 'Your Free Course list retrieved successfully' ,$request->path()); 

    }

     public function mypaidcourse(Request $request,$id)
    {
        $getyourcourseid = Mycourse::where(['user_id'=>$id,'type'=>'paid'])->orderBy('id','asc')->pluck('course_id');
        
        $getyourcourseid11 = Mycourse::where(['user_id'=>$id,'type'=>'paid'])->orderBy('id','asc')->pluck('expdate');

         $freecoursedetails = DB::table('paidcourses')->whereIn('id',$getyourcourseid)->select('id as course_id','intro_video','title','description','thumbnail','duration')->get();
         
         $ik = 0;

        foreach ($freecoursedetails as  $value) {
             $value->course_id = (string)$value->course_id;
             $value->duration = gmdate("H:i:s", round($value->duration));
             $value->expdate = date("Y-m-d",strtotime("+3 months",strtotime($getyourcourseid11[$ik])));
             $ik++;
        }

         return $this->sendResponse($freecoursedetails, 'Your Paid Course list retrieved successfully' ,$request->path()); 

    }

    public function addtomycourse(Request $request)
    {
         $input = $request->all();

        $validator = Validator::make($input, [
            'token'=>'required',
            'user_id'=>'required',
            'course_id'=>'required',
        ]);


        if($validator->fails()){
            return $this->sendError($request->path(),$validator->errors()->first());       
        }

        $check_token = Token::where(['user_id'=>$input['user_id'],'token'=>$input['token']])->first();

        if (empty($check_token)) {
            return $this->sendError($request->path(),'Login Token Expire');
        }

        $checksameentry = Mycourse::where(['user_id'=>$input['user_id'],'type'=>'free','course_id'=>$input['course_id']])->first();

        if(!empty($checksameentry)){
             return $this->sendError($request->path(),'This Course Already Added');
        }

        Mycourse::create($input);

        return $this->sendResponse(['status'=>'success'], 'Course added successfully' ,$request->path());   

    }

    public function sendmasg(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'token'=>'required',
            'user_id'=>'required',
            'topic_id'=>'required',
            'msg'=>'required',
        ]);


        if($validator->fails()){
            return $this->sendError($request->path(),$validator->errors()->first());       
        }

        $check_token = Token::where(['user_id'=>$input['user_id'],'token'=>$input['token']])->first();

        if (empty($check_token)) {
            return $this->sendError($request->path(),'Login Token Expire');
        }

        unset($input['token']);

        $userdetails = User::find($input['user_id']);

        if(empty($userdetails)){
            return $this->sendError($request->path(),'User not found!!');
        }

        $insertquesy = Forumtopiccomment::create($input);

        $reply_array = array();

       // dd($insertquesy);
        $userdetails->fullname = "ASEC English:".$userdetails->id;
      if(!empty($userdetails->image)){
             $userdetails->image = url('/').'/public/'.$userdetails->image;   
          } else {
             $userdetails->image = url('/public/')."/img/user_signup.png";
          }

          $replylist = Reply::where('thread_id',$insertquesy->thread_id)->get();

               $reply_array = array();
              foreach($replylist as $valuenew) {
                  $otheruserdetails = User::find($valuenew->user_id);
                  if(!empty($otheruserdetails->image)){
                         $otheruserdetails->image = url('/').'/public/'.$otheruserdetails->image;   
                      } else {
                         $otheruserdetails->image = url('/public/')."/img/user_signup.png";
                      }
                   $valuenew->user_id = (string)$valuenew->user_id;
                   $otheruserdetails->fullname = "ASEC English:".$otheruserdetails->id;
                   $valuenew->created_at = Carbon::parse($valuenew->created_at)->diffForHumans();   
                  $reply_array[] = array("user_id"=>$valuenew->user_id,"image"=>$otheruserdetails->image,"msg"=>$valuenew->msg,"fullname"=>$otheruserdetails->fullname);
              }

        $return_array = array("user_id"=>$input['user_id'],"image"=>$userdetails->image,"msg"=>$input['msg'],"created_at"=>Carbon::now()->diffForHumans(),"fullname"=>$userdetails->fullname,"thread_id"=>$insertquesy->id,"replylist"=>$reply_array);
        
        $return_arraynew = array("user_id"=>$input['user_id'],"image"=>$userdetails->image,"msg"=>$input['msg'],"created_at"=>Carbon::now()->diffForHumans(),"fullname"=>$userdetails->fullname,"thread_id"=>$insertquesy->id,"replylist"=>$reply_array);

      

        $alluserid = DB::table('discussionjoins')->where('topic_id',$input['topic_id'])->pluck('user_id');

        $alltoken = Token::whereIn('user_id',$alluserid)->get();

        foreach ($alltoken as $value) {
              if($value->deviceType == 'android')
              {
                    
                   
                $this->android_push($value->deviceToken,$input['msg'],"chat",1,$return_arraynew);
                   
              }
        }

         return $this->sendResponse($return_array, 'Message sent successfully',$request->path());

    }

    public function discussionjoinbyuser(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'token'=>'required',
            'user_id'=>'required',
            'topic_id'=>'required',
        ]);


        if($validator->fails()){
            return $this->sendError($request->path(),$validator->errors()->first());       
        }

        $check_token = Token::where(['user_id'=>$input['user_id'],'token'=>$input['token']])->first();

        if (empty($check_token)) {
            return $this->sendError($request->path(),'Login Token Expire');
        }

        unset($input['token']);

        $findalreadyuser = DB::table('forumtopiccomments')->where('user_id',$input['user_id'])->first();

        if(!empty($findalreadyuser)){
             return $this->sendError($request->path(),'You are deleted By admin');
        }

        $insertquesy = DB::table('discussionjoins')->insert($input);

        return $this->sendResponse(['status'=>'success'], 'Discussion join successfully' ,$request->path());   

    }

    public function discussioncommentlist(Request $request,$id,$user_id)
    {
        $finddetails = DB::table('forumtopiccomments')->join('users','forumtopiccomments.user_id','=','users.id')->where('forumtopiccomments.topic_id',$id)->orderby('forumtopiccomments.id','desc')->select('forumtopiccomments.id as thread_id','forumtopiccomments.msg','users.fullname','users.id as user_id','users.image','forumtopiccomments.created_at')->get();

        $finjoindisccussion = DB::table('discussionjoins')->where(['topic_id'=>$id,'user_id'=>$user_id])->first();

       

        if(!empty($finjoindisccussion)){
            $join_flag = "1";
        } else {
            $join_flag = "0";
        }

        foreach ($finddetails as $value) {
              if(!empty($value->image)){
                 $value->image = url('/').'/public/'.$value->image;   
              } else {
                 $value->image = url('/public/')."/img/user_signup.png";
              }
              $value->fullname = "ASEC English:".$value->user_id;
              $value->thread_id = (string)$value->thread_id;

              $replylist = Reply::where('thread_id',$value->thread_id)->get();

               $reply_array = array();
              foreach($replylist as $valuenew) {
                  $otheruserdetails = User::find($valuenew->user_id);
                  if(!empty($otheruserdetails->image)){
                         $otheruserdetails->image = url('/').'/public/'.$otheruserdetails->image;   
                      } else {
                         $otheruserdetails->image = url('/public/')."/img/user_signup.png";
                      }
                   $valuenew->user_id = (string)$valuenew->user_id;
                   $otheruserdetails->fullname = "ASEC English:".$otheruserdetails->id;
                   $varnewcoll = $valuenew->created_at->format('Y-m-d H:i:s');

                   $valuenewhasfdfashd = Carbon::parse($varnewcoll)->diffForHumans();   
                  $reply_array[] = array("user_id"=>$valuenew->user_id,"image"=>$otheruserdetails->image,"msg"=>$valuenew->msg,"fullname"=>$otheruserdetails->fullname,"created_at"=>$valuenewhasfdfashd);
              }

              $value->replylist = $reply_array;
              $value->user_id = (string)$value->user_id;

              $value->created_at = Carbon::parse($value->created_at)->diffForHumans();
        }

         return $this->sendResponse(['topic_id'=>(string)$id,'join_flag'=>$join_flag,'comments'=>$finddetails], 'Comment list retrieved successfully' ,$request->path()); 
    }

    public function discussionlist(Request $request)
    {
        $detailstopics =  DB::table('forumtopics')->where('admin_status','1')->orderby('id','desc')->select('id as topic_id','name')->get();

         foreach ($detailstopics as  $value) {
             $value->topic_id = (string)$value->topic_id;
           
        }

         return $this->sendResponse($detailstopics, 'Discussion list retrieved successfully' ,$request->path()); 
    }

    public function Myforumvideo(Request $request,$id = null)
    {
        $freecoursedetails = DB::table('forumvideos')->orderby('id','desc')->select('id as video_id','video','title','description','thumbnail','duration')->get();
        
        $maincoursedetails = array();
        
        $maincoursedetails1 = array();

        foreach ($freecoursedetails as  $value) {
             $value->video_id = (string)$value->video_id;
             if($value->video_id == $id){
                 $maincoursedetails = $value;
             } else {
                 $maincoursedetails1[] = $value;
             }
        }
        
        if(!empty($id)){
             return $this->sendResponse(["videodetails"=>$maincoursedetails,"otherlisting"=>$maincoursedetails1], 'Forum video list retrieved successfully' ,$request->path()); 
        } else {
             if(!empty($maincoursedetails1)){
                 $maincoursedetails = $maincoursedetails1["0"];
                 //unset($maincoursedetails1["0"]);
             }
            
             return $this->sendResponse(["videodetails"=>$maincoursedetails,"otherlisting"=>$maincoursedetails1], 'Forum video list retrieved successfully' ,$request->path()); 
        }

       
    }

    public function freecourse(Request $request,$token,$userid,$id = null)
    {
        $check_token = Token::where(['user_id'=>$userid,'token'=>$token])->first();

        if (empty($check_token)) {
            return $this->sendError($request->path(),'Login Token Expire');
        }

        $freecoursedetails = DB::table('freecourses')->orderby('id','desc')->select('id as course_id','video','title','description','thumbnail','duration')->get();
        
        $maincoursedetails = array();
        
        $maincoursedetails1 = array();

        foreach ($freecoursedetails as  $value) {
             $value->course_id = (string)$value->course_id;
             if($value->course_id == $id){
                 $maincoursedetails = $value;
             } else {
                 $maincoursedetails1[] = $value;
             }

             $value->duration = gmdate("H:i:s", round($value->duration));
        }
        
        if(!empty($id)){
             return $this->sendResponse(["coursedetails"=>$maincoursedetails,"otherlisting"=>$maincoursedetails1], 'Free Course list retrieved successfully' ,$request->path()); 
        } else {
             return $this->sendResponse($maincoursedetails1, 'Free Course list retrieved successfully' ,$request->path()); 
        }

       
    }

    public function paidcourse(Request $request)
    {
        $freecoursedetails = DB::table('paidcourses')->orderby('id','desc')->get();

        $return_array = array();

        foreach ($freecoursedetails as  $value) {
             $number_of_video = sizeof(explode(",", $value->modules_id));
             $return_array[] = array("course_name"=>$value->title,"course_id"=>(string)$value->id,"number_of_video"=>(string)$number_of_video,"price"=>$value->price);
        }

        return $this->sendResponse($return_array, 'Paid Course list retrieved successfully' ,$request->path()); 
    }

    public function banners(Request $request,$type)
    {
        $details = DB::table('banners')->where('type',$type)->select('image')->get();


         return $this->sendResponse($details, 'Banners retrieved successfully' ,$request->path());   

    }

    public function forgotpassword(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
                'email'=>'required',
            ]);

         if($validator->fails()){
                return $this->sendError($request->path(),$validator->errors()->first());       
            }

         if($this->checkemail($input['email']) == 0){
            return $this->sendError($request->path(),"Email Id does not Exist");
         }

         $userdetails = User::where('email',$input['email'])->first();

         $otp = $userdetails->password;

          $email = $input['email'];

                   $subject = "Request for Forgot Password";
                 
                $postData ="";
                 try{
                     Mail::send('emails.otpverify', ['otp' =>$otp], function($message) use ($postData,$email)
                                {
                                  $message->from('asecclass@asecenglish.awsapps.com', 'TMASEC');
                                  $message->to($email, 'TMASEC')->subject('Request for Forgot Password');
                                });

                    

                 }
                  catch(Exception $e){
                                   
                                } 
                return $this->sendResponse(['status'=>'success'], 'Password sent to your email successfully' ,$request->path());   
    }
    
    
    public function signup(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
                'password' => 'required|confirmed|min:6',
                'fullname'=>'required',
                'email'=>'required',
                'phone'=>'required',
                'deviceType'=>'required',
                'deviceToken'=>'required',
            ]);

         if($validator->fails()){
                return $this->sendError($request->path(),$validator->errors()->first());       
            }

         if($this->checkemail($input['email']) == 1){
            return $this->sendError($request->path(),"Email Id Already Exist");
         }
         
           if($this->checkphone($input['phone']) == 1){
            return $this->sendError($request->path(),"Phone Number Already Exist");
         }

         $input['image'] = "";

         $input['address'] = "";

         $input['dob'] = "";

         $insertmember = User::create($input);

         $input['user_id'] = (string)$insertmember->id;

         $input['token'] = str_random(25);




         $newtokenarray = Token::create($input);


          $details['asec_userid'] = "ASEC English:".$input['user_id'];
          $details['image'] = url('/public/')."/img/user_signup.png";

         return $this->sendResponse($input, 'User created successfully.',$request->path());


    }


 

     public function login(Request $request)
    {
       $input = $request->all();

       $validator = Validator::make($input, [
            'email'=>'required',
            'password' => 'required',
            'deviceToken'=>'required',
            'deviceType'=>'required|in:android,ios',
        ]);


        if($validator->fails()){
            return $this->sendError($request->path(),$validator->errors()->first());       
        }

        $detailsother =  User::where(['phone'=>$input['email']])->first();

         if(empty($detailsother)) {
            return $this->sendError($request->path(),"You Are Not  a Registered User");
        }

        $details = User::where(['phone'=>$input['email'],'password'=>$input['password'],'admin_status'=>'1'])->first();

      //dd($details);

        if (empty($details)) {
            return $this->sendError($request->path(),"Your Id or Password is Incorrect");
        }

        $token_s = str_random(25);
        
        $findtokencolm = Token::where('user_id',$details['id'])->first();

        $msgg = "logout";

           $return_arraynew = array("course_id"=>"","viewlink"=>"","msg"=>$msgg,"title"=>"You are login on other device","image"=>"","time"=>date("Y-m-d h:i:s"));

             
                    if($findtokencolm->deviceType == 'android')
              {
                    
                   
                $this->android_push($findtokencolm->deviceToken,$msgg,"tokenexpire",1,$return_arraynew);
                   
              }

          


        if(!empty($findtokencolm)){

        $token_saver = Token::where('user_id',$details['id'])->update(['token'=>$token_s,'deviceToken'=>$input['deviceToken'],'deviceType'=>$input['deviceType']]);

        } else {
            $token_saver = Token::create(array("user_id"=>$details['id'],"token"=>$token_s,"deviceToken"=>$input['deviceToken'],"deviceType"=>$input['deviceType']));
        }


        $details['user_id'] =  (string)$details['id'];


        unset($details['id']);
        
        
        if(!empty($details['image'])){
           $details['image'] = url('/public/')."/".$details['image'];
        } else {
        
          $details['image'] = url('/public/')."/img/user_signup.png";
        }

         if(empty($details['dob'])){
            $userdetails['dob'] = "";
        }


        $details['asec_userid'] = "ASEC English:".$details['user_id'];
        
        $details['token'] = $token_s;


       return $this->sendResponse($details, 'User login successfully.',$request->path());
    }

    public function checkemail($email)
    {
        $email_inmembers = DB::table('users')->where('email',$email)->first();

        if(!empty($email_inmembers)){
            return 1;
        } else {
           
                return 0;
        
        }
    }
    
     public function checkphone($email)
    {
        $email_inmembers = DB::table('users')->where('phone',$email)->first();

        if(!empty($email_inmembers)){
            return 1;
        } else {
           
                return 0;
        
        }
    }

    


}
