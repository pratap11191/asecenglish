<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    
    protected $fillable = ['type', 'url', 'description','tag_children_id','creater_id'];
}
