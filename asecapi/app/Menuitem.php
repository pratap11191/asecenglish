<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menuitem extends Model
{
    
     protected $fillable = ['item_msg','type','user_id'];
}
