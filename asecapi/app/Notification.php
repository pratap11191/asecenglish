<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    //
    
     protected $fillable = [
        'receiver_id','msg','status','create_time'
    ];
}
