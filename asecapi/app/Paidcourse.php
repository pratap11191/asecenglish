<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paidcourse extends Model
{
    
    protected $fillable = ['user_id', 'token'];
}
