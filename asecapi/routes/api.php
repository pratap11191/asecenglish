<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('content/{type}',function($type){
    
    if($type == 'about'){
    return view('aboutus');
    } else {
     return view('contactus');
    }
});

Route::get('contact',function(){
    return view('contactus');
});

Route::get('testtandc','API\UserAPIController@testtandc');

Route::get('tandc','API\UserAPIController@tandc');

Route::get('viewbook/{id}','API\UserAPIController@viewbook');

Route::get('/sendpostnotify/{id}','API\UserAPIController@sendpostnotify');

Route::get('/exptoken/{id}','API\UserAPIController@exptoken');

Route::get('/downloadPDF/{id}','API\UserAPIController@downloadPDF');

Route::get('/downloadPDFall','API\UserAPIController@downloadPDFall');

Route::get('/viewcount/{id1}/{id2}','API\UserAPIController@viewcount');
Route::get('/viewcount1/{id1}/{id2}','API\UserAPIController@viewcount1');

Route::get('faqandhelp','API\UserAPIController@faqandhelp');

Route::post('testpdf','API\UserAPIController@testpdf');

Route::get('test_instructions',function(){
    return view('test_instructions');
});

Route::get('paymentnotification/{id}/{type}/{type1}','API\UserAPIController@paymentnotification');

Route::get('invoicesender/{id}','API\UserAPIController@invoicesender');

Route::get('notifylist/{id}','API\UserAPIController@notifylist');

Route::get('newbooknotify/{id}','API\UserAPIController@newbooknotify');

Route::get('newpaidnotify/{id}','API\UserAPIController@newpaidnotify');

Route::get('newfreenotify/{id}','API\UserAPIController@newfreenotify');

Route::get('testcreatenotify/{id}','API\UserAPIController@testcreatenotify');

Route::get('livestreambycourse/{id}/{token}/{id2}','API\UserAPIController@livestreambycourse');

Route::get('livestartleccreate/{id}','API\UserAPIController@livestartleccreate');

Route::get('livestartlec/{id}','API\UserAPIController@livestartlec');

Route::get('myPDF/{id}/{id11}','API\UserAPIController@myPDF');

Route::get('doubtsesion','API\UserAPIController@doubtsesion');

Route::post('doubtsesionadmitcard','API\UserAPIController@doubtsesionadmitcard');

Route::post('notifystatus','API\UserAPIController@notifystatus');

Route::get('myorder/{id}','API\UserAPIController@myorder');

Route::post('helpus','API\UserAPIController@helpus');

Route::post('reply','API\UserAPIController@reply');

Route::post('checkcupon','API\UserAPIController@checkcupon');

Route::post('sendmsg','API\UserAPIController@sendmasg');

Route::post('discussionjoinbyuser','API\UserAPIController@discussionjoinbyuser');

Route::get('discussioncommentlist/{id}/{user_id}','API\UserAPIController@discussioncommentlist');

Route::get('discussioncommentlist/{id}','API\UserAPIController@discussioncommentlist1');

Route::get('discussionlist','API\UserAPIController@discussionlist');


Route::post('viewresult','API\UserAPIController@viewresult');

Route::post('viewanswers','API\UserAPIController@viewanswers');

Route::post('teststart','API\UserAPIController@teststart');

Route::get('testlist','API\UserAPIController@testlist');

Route::post('placeorder','API\UserAPIController@placeorder');

Route::get('bookdetails/{id}','API\UserAPIController@bookdetails');

Route::get('booklist/{token}/{userid}','API\UserAPIController@booklist');

Route::get('assignmentlist','API\UserAPIController@assignmentlist');

Route::get('subscriptionlist','API\UserAPIController@subscriptionlist');

Route::post('changepassword','API\UserAPIController@changepassword');

Route::post('signup','API\UserAPIController@signup');

Route::post('login','API\UserAPIController@login');

Route::post('forgotpassword','API\UserAPIController@forgotpassword');

Route::get('banners/{type}','API\UserAPIController@banners');

Route::get('freecourse/{token}/{userid}/{id?}','API\UserAPIController@freecourse');

Route::get('myforumvideo/{id?}','API\UserAPIController@Myforumvideo');

Route::get('paidcourse','API\UserAPIController@paidcourse');

Route::post('addtomycourse','API\UserAPIController@addtomycourse');

Route::get('myfreecourse/{id}','API\UserAPIController@myfreecourse');

Route::get('mypaidcourse/{id}','API\UserAPIController@mypaidcourse');

Route::post('viewprofile','API\UserAPIController@viewprofile');

Route::post('editprofile','API\UserAPIController@editprofile');

Route::get('paidcoursedetails/{id}/{idf?}/{token}','API\UserAPIController@paidcoursedetails');

Route::post('sendotp','API\UserAPIController@sendotp');


Route::group(array('before' => 'csrf'), function()
{



Route::get('info',function(){

return view('info');

});


});



