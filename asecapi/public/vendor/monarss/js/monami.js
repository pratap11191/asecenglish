function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}


$(".hambergmenu").click(function (e) {
    e.stopPropagation();
    $(".menu-sidebar").toggleClass('opensidemenu');
});

$(".closemenuico").click(function (e) {
    e.stopPropagation();
    $(".menu-sidebar").removeClass('opensidemenu');
});


 
$(".mychatopn").click(function (e) {
    e.stopPropagation();
    $(".mychatping").removeClass('openchatmenupan');
});
