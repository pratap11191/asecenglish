 <!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ASEC | Services</title>
<link rel="shortcut icon" href="http://mobulous.in/homefuud/totrr_favicon.ico"/>
<style type="text/css">
  .content {
    width: 60%;
    margin: 50px auto;
    padding: 20px;
  }
  .content h1 {
    font-weight: 400;
    text-transform: uppercase;
    margin: 0;
  }
  .content h2 {
    font-weight: 400;
    text-transform: uppercase;
    color: #333;
    margin: 0 0 20px;
  }
  .content p {
    font-size: 1em;
    font-weight: 300;
    line-height: 1.5em;
    margin: 0 0 20px;
  }
  .content p:last-child {
    margin: 0;
  }
  .content a.button {
    display: inline-block;
    padding: 10px 20px;
    background: #ff0;
    color: #000;
    text-decoration: none;
  }
  .content a.button:hover {
    background: #000;
    color: #ff0;
  }
  .content.title {
    position: relative;
    background: none;
    border: 2px dashed #333;
  }
  .content.title h1 span.demo {
    display: inline-block;
    font-size: .5em;
    padding: 5px 10px;
    background: #000;
    color: #fff;
    vertical-align: top;
    margin: 7px 0 0;
  }
  .content.title .back-to-article {
    position: absolute;
    bottom: -20px;
    left: 20px;
  }
  .content.title .back-to-article a {
    padding: 10px 20px;
    background: #f60;
    color: #fff;
    text-decoration: none;
  }
  .content.title .back-to-article a:hover {
    background: #f90;
  }
  .content.title .back-to-article a i {
    margin-left: 5px;
  }
  .content.white {
    background: #fff;
    box-shadow: 0 0 10px #999;
  }
  .content.black {
    background: #000;
  }
  .content.black p {
    color: #999;
  }
  .content.black p a {
    color: #08c;
  }

  .accordion-container {
    width: 100%;
    margin: 0 0 20px;
    clear: both;
  }
  .accordion-toggle {
    margin-bottom: 15px;
    position: relative;
    display: block;
    padding: 15px;
    font-size: 1.2em;
    font-weight: 300;
       background: #41406f;
    color: #fff;
    text-decoration: none;
    font-weight: 600;
    font-size: 14px;
}
  .accordion-toggle.open {
    background: #1e2e65;
    color: #fff;
  }
  .accordion-toggle:hover {
    background: rgb(80, 105, 27);
    color: #fff!important;
}
  .accordion-toggle span.toggle-icon {
    position: absolute;
    top: 17px;
    left: 20px;
    font-size: 1.5em;
  }
  .accordion-content {
    display: none;
    padding: 20px;
    overflow: auto;
  }
  .accordion-content img {
    display: block;
    float: left;
    margin: 0 15px 10px 0;
    max-width: 100%;
    height: auto;
  }

  /* media query for mobile */
  @media (max-width: 767px) {
    .content {
      width: auto;
    }
    .accordion-content {
      padding: 10px 0;
      overflow: inherit;
    }
  }
  
  body {
    font-family: "Poppins", sans-serif;
}


.page-header.myresources{
text-align: center;
}
</style>
<div class="container pages">
    <div class="row text-left" style="padding:10px;">
      <h2 style="text-align:center;"></h2>
        <div class="page-header myresources">
    <h3>ASEC Service Listing</h3>
    <img src="{{url('/')}}/public/logo-admin.png" style="width: 148px;">
    </div>
        <div class="page-header">Service Url : http://mobuloustech.com/asecapi/api/</div><br/>
     


<!-- *************************signup normal*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">1) signup</span>
      </a>
      <div class="accordion-content">
         <p>devicetype = (android or ios)</p>
          <p>
              <h5>Parameters: </h5>
              <pre>
fullname:required
email:required
password:required
phone:required
deviceType:required
deviceToken:required
password_confirmation:required

                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>deviceType: android or ios</p>
          <p> <h5>requestKey: </h5>signup</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "signup": {
        "fullname": "parent",
        "email": "parent221@gmail.com",
        "password": "12345678",
        "phone": "9876543223",
        "deviceType": "android",
        "deviceToken": "fhsdfhdsgfhdshfdshfdshfhdsfhdshfhdsfhdsv",
        "password_confirmation": "12345678",
        "user_id": "7",
        "token": "mLU8BEWApTfs7VFIKuVdonWkp"
    },
    "message": "User created successfully.",
    "requestKey": "api/signup"
}     </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>


<!-- *************************login normal*********************************************** -->
      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">2)  login</span>
      </a>
      <div class="accordion-content">
         <p>devicetype = (android or ios)</p>
          <p>
              <h5>Parameters: </h5>
              <pre>
email:required
password:required
deviceType:required
deviceToken:required
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>deviceType: android or ios</p>
          <p> <h5>requestKey: </h5>login</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "login": {
        "fullname": "raja",
        "email": "prat@gmail.com",
        "phone": "7412583636",
        "admin_status": "1",
        "image": "http://mobuloustech.com/asecapi/public/img/user_signup.png",
        "created_at": "2018-12-04 06:52:51",
        "updated_at": "2018-12-04 11:53:38",
        "user_id": "3",
        "token": "LKgzCjrVgtKgIl9KioLXxJe2N"
    },
    "message": "User login successfully.",
    "requestKey": "api/login"
}</pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5></p>
      </div>
      
<!-- ***************************** check email and send otp ****************** -->

  <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">3) Forgot password</span>
      </a>
      <div class="accordion-content">
         <p>devicetype = (android or ios)</p>
          <p>
              <h5>Parameters: </h5>
              <pre>
email:required

                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>deviceType: android or ios</p>
          <p> <h5>requestKey: </h5>forgotpassword</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "forgotpassword": {
        "status": "success"
    },
    "message": "Password sent to your email successfully",
    "requestKey": "api/forgotpassword"
}  </pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5> In case status is 1 message show in toast otherwise go ahead.</p>
      </div>

      <!--********************************* social login ******************************** -->

      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">4) Banners</span>
      </a>
      <div class="accordion-content">
         <p>devicetype = (android or ios)</p>
          <p>
              <h5>Parameters: </h5>
              <pre>


                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>deviceType: android or ios</p>
          <p> <h5>requestKey: </h5>banners/type</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "banners": [
        {
            "image": "https://www.heysigmund.com/wp-content/uploads/Studying-9-Scientifically-Proven-Ways-to-Supercharge-Your-Learning.jpg"
        },
        {
            "image": "https://d3jdihwi7h4aqv.cloudfront.net/resources/media/user/1482855209_1482852652_study_header.jpg"
        }
    ],
    "message": "Banners retrieved successfully",
    "requestKey": "api/banners/home"
}</pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5> type is home or book </p>
      </div>

      <!--********************************* social login ******************************** -->

      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">5) Free course list</span>
      </a>
      <div class="accordion-content">
         <p>devicetype = (android or ios)</p>
          <p>
              <h5>Parameters: </h5>
              <pre>


                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>deviceType: android or ios</p>
          <p> <h5>requestKey: </h5>freecourse</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "freecourse": [
        {
            "course_id": "2",
            "video": "https://www.youtube.com/embed/TTpe2lOGF4g?ecver=1",
            "title": "What is Android",
            "description": "DURGASOFT is INDIA's No.1 Software Training Center offers  \r\nonline training on various technologies like JAVA, .NET , "
        },
        {
            "course_id": "1",
            "video": "https://www.youtube.com/embed/xBYr9DxDqyU?ecver=1",
            "title": "Create a API",
            "description": "Learn how to quickly build a Node.js API that uses json web tokens to protect routes. Check out the related blog post here:"
        }
    ],
    "message": "Free Course list retrieved successfully",
    "requestKey": "api/freecourse"
}</pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5> </p>
      </div>

      <!--********************************* social login ******************************** -->

      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">6) Add free course to your my free course section</span>
      </a>
      <div class="accordion-content">
         <p>devicetype = (android or ios)</p>
          <p>
              <h5>Parameters: </h5>
              <pre>
                token:required
                user_id:required
                course_id:required

                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>deviceType: android or ios</p>
          <p> <h5>requestKey: </h5>addtomycourse</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "addtomycourse": {
        "status": "success"
    },
    "message": "Course added successfully",
    "requestKey": "api/addtomycourse"
}</pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5> </p>
      </div>

      <!--********************************* social login ******************************** -->

      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">7) My free course list</span>
      </a>
      <div class="accordion-content">
         <p>devicetype = (android or ios)</p>
          <p>
              <h5>Parameters: </h5>
              <pre>


                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>deviceType: android or ios</p>
          <p> <h5>requestKey: </h5>myfreecourse/user_id</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "myfreecourse": [
        {
            "course_id": "2",
            "video": "https://www.youtube.com/embed/TTpe2lOGF4g?ecver=1",
            "title": "What is Android",
            "description": "DURGASOFT is INDIA's No.1 Software Training Center offers  \r\nonline training on various technologies like JAVA, .NET , "
        },
        {
            "course_id": "1",
            "video": "https://www.youtube.com/embed/xBYr9DxDqyU?ecver=1",
            "title": "Create a API",
            "description": "Learn how to quickly build a Node.js API that uses json web tokens to protect routes. Check out the related blog post here:"
        }
    ],
    "message": "Your Free Course list retrieved successfully",
    "requestKey": "api/myfreecourse/2"
}</pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5>  </p>
      </div>


  <!--********************************* social login ******************************** -->

      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">8)View profile</span>
      </a>
      <div class="accordion-content">
         <p>devicetype = (android or ios)</p>
          <p>
              <h5>Parameters: </h5>
              <pre>
                user_id:required
                token:required

                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          
          <p> <h5>requestKey: </h5>viewprofile</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "viewprofile": {
        "user_id": "2",
        "image": "http://mobuloustech.com/asecapi/public/img/user_signup.png",
        "fullname": "ram",
        "address": " ",
        "email": "ram@gmail.com",
        "phone": "7891425360"
    },
    "message": "User details retrieved successfully",
    "requestKey": "api/viewprofile"
}</pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5>  </p>
      </div>


  <!--********************************* social login ******************************** -->

      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">9) Edit profile</span>
      </a>
      <div class="accordion-content">
         <p>devicetype = (android or ios)</p>
          <p>
              <h5>Parameters: </h5>
              <pre>
              fullname:required
                user_id:required
                token:required
                email:required
                address:required
                phone:required
                image:

                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>deviceType: android or ios</p>
          <p> <h5>requestKey: </h5>editprofile</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "editprofile": {
        "user_id": "2",
        "image": "http://mobuloustech.com/asecapi/public/users-photos/1b3432695118be3.jpeg",
        "fullname": "ali",
        "address": "noida",
        "email": "pra@ali.com",
        "phone": "7788779955"
    },
    "message": "User profile updated successfully",
    "requestKey": "api/editprofile"
}</pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5>  </p>
      </div>

<!--********************************* social login ******************************** -->

      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">10) Paid course list</span>
      </a>
      <div class="accordion-content">
         <p>devicetype = (android or ios)</p>
          <p>
              <h5>Parameters: </h5>
              <pre>
            

                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>deviceType: android or ios</p>
          <p> <h5>requestKey: </h5>paidcourse</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "paidcourse": [
        {
            "course_id": "1",
            "number_of_video": 8,
            "price": "3000"
        }
    ],
    "message": "Paid Course list retrieved successfully",
    "requestKey": "api/paidcourse"
}</pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5>  </p>
      </div>

     
     <!--********************************* social login ******************************** -->

      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">11) Paid course details</span>
      </a>
      <div class="accordion-content">
         <p>devicetype = (android or ios)</p>
          <p>
              <h5>Parameters: </h5>
              <pre>
            

                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>deviceType: android or ios</p>
          <p> <h5>requestKey: </h5>paidcoursedetails/course_id</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "paidcoursedetails/1": {
        "course_id": "1",
        "title": "SSC GD MATH",
        "description": "Percentage Tricks/Shortcuts/Formula | Percentage Problems Tricks and Shortcuts | DSSSB, CTET, KVS",
        "intro_video": "https://www.youtube.com/embed/gbR_m1byDns?ecver=1",
        "duration": "21:51",
        "thumbnail": "http://mobuloustech.com/asec_admin/public/images/icon/logoadmin.png",
        "coursemodules": {
            "1": {
                "module_name": "Module 1",
                "videodetails": [
                    {
                        "title": "Problem on Ages Tricks",
                        "description": "Problem on Ages Tricks in Hindi | Ages Problem Short Cut/Concept/Formula |problem based on ages | DSSSB, ALP, CTET, Bank PO",
                        "video": "https://www.youtube.com/embed/U8K0V2EChfs?ecver=1",
                        "thumbnail": "http://mobuloustech.com/asec_admin/public/images/icon/logoadmin.png",
                        "duration": "21:42"
                    },
                    {
                        "title": "SQUARE TRICK",
                        "description": "HOW TO FIND SQUARE ROOT",
                        "video": "https://www.youtube.com/embed/VukZuYlvSLQ?ecver=1",
                        "thumbnail": "http://mobuloustech.com/asec_admin/public/images/icon/logoadmin.png",
                        "duration": "14:42"
                    },
                    {
                        "title": "Non Perfect Square Root",
                        "description": "Classes New Schedule",
                        "video": "https://www.youtube.com/embed/LJ2OdwQmiyM?ecver=1",
                        "thumbnail": "http://mobuloustech.com/asec_admin/public/images/icon/logoadmin.png",
                        "duration": "9:56"
                    }
                ]
            },
            "2": {
                "module_name": "Module 2",
                "videodetails": [
                    {
                        "title": "Counting of figures",
                        "description": "Classes New Schedule",
                        "video": "https://www.youtube.com/embed/IRFOFmKhPLo?ecver=1",
                        "thumbnail": "http://mobuloustech.com/asec_admin/public/images/icon/logoadmin.png",
                        "duration": "28:26"
                    },
                    {
                        "title": "Dice reasoning Short tricks",
                        "description": "Classes New Schedule fdsf gfds fsdfgjsdgf dshfsdgf",
                        "video": "https://www.youtube.com/embed/K8tZcV8SOLo?ecver=1",
                        "thumbnail": "http://mobuloustech.com/asec_admin/public/images/icon/logoadmin.png",
                        "duration": "10:04"
                    },
                    {
                        "title": "SQUARE TRICK",
                        "description": "HOW TO FIND SQUARE ROOT",
                        "video": "https://www.youtube.com/embed/VukZuYlvSLQ?ecver=1",
                        "thumbnail": "http://mobuloustech.com/asec_admin/public/images/icon/logoadmin.png",
                        "duration": "14:42"
                    }
                ]
            },
            "3": {
                "module_name": "Module 3",
                "videodetails": [
                    {
                        "title": "SQUARE TRICK",
                        "description": "HOW TO FIND SQUARE ROOT",
                        "video": "https://www.youtube.com/embed/VukZuYlvSLQ?ecver=1",
                        "thumbnail": "http://mobuloustech.com/asec_admin/public/images/icon/logoadmin.png",
                        "duration": "14:42"
                    },
                    {
                        "title": "Counting of figures",
                        "description": "Classes New Schedule",
                        "video": "https://www.youtube.com/embed/IRFOFmKhPLo?ecver=1",
                        "thumbnail": "http://mobuloustech.com/asec_admin/public/images/icon/logoadmin.png",
                        "duration": "28:26"
                    }
                ]
            }
        },
        "flag": "0",
        "price": "3000"
    },
    "message": "Paid Course Details retrieved successfully",
    "requestKey": "api/paidcoursedetails/1"
}</pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5>  </p>
      </div>

      
<!--********************************* social login ******************************** -->

      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">12) change password</span>
      </a>
      <div class="accordion-content">
         <p>devicetype = (android or ios)</p>
          <p>
              <h5>Parameters: </h5>
              <pre>
                token:required
                user_id:required
                password:required
                oldpassword:required

                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>deviceType: android or ios</p>
          <p> <h5>requestKey: </h5>changepassword</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "changepassword": {
        "status": "success"
    },
    "message": "Password updated successfully.",
    "requestKey": "api/changepassword"
}</pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5>  </p>
      </div>

      <!--********************************* social login ******************************** -->

      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">13) send otp</span>
      </a>
      <div class="accordion-content">
         <p>devicetype = (android or ios)</p>
          <p>
              <h5>Parameters: </h5>
              <pre>
                phone:required

                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>deviceType: android or ios</p>
          <p> <h5>requestKey: </h5>sendotp</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "sendotp": {
        "status": "success",
        "otp": "8831",
    },
    "message": "Verification Otp sent to your phone number successfully",
    "requestKey": "api/sendotp"
}</pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5>  </p>
      </div>

       <!--********************************* social login ******************************** -->

      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">14) about us  and contact us webview </span>
      </a>
      <div class="accordion-content">
         <p>devicetype = (android or ios)</p>
          <p>
              <h5>Parameters: </h5>
              <pre>
               

                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>deviceType: android or ios</p>
          <p> <h5>requestKey: </h5>content/type</p>
          <p> <h5>Sample Response: </h5>
            <pre>
            webview for about us or contact us 
</pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5> type => about or contact </p>
      </div>


 <!--********************************* social login ******************************** -->

      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">15) Free course details</span>
      </a>
      <div class="accordion-content">
         <p>devicetype = (android or ios)</p>
          <p>
              <h5>Parameters: </h5>
              <pre>


                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>deviceType: android or ios</p>
          <p> <h5>requestKey: </h5>freecourse/course_id</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "freecourse": {
        "coursedetails": {
            "course_id": "2",
            "video": "https://www.youtube.com/embed/TTpe2lOGF4g?ecver=1",
            "title": "What is Android",
            "description": "DURGASOFT is INDIA's No.1 Software Training Center offers  \r\nonline training on various technologies like JAVA, .NET , ",
            "thumbnail": "http://mobuloustech.com/asec_admin/public/images/icon/logoadmin.png",
            "duration": "14:22"
        },
        "otherlisting": [
            {
                "course_id": "1",
                "video": "https://www.youtube.com/embed/xBYr9DxDqyU?ecver=1",
                "title": "Create a API",
                "description": "Learn how to quickly build a Node.js API that uses json web tokens to protect routes. Check out the related blog post here:",
                "thumbnail": "http://mobuloustech.com/asec_admin/public/images/icon/logoadmin.png",
                "duration": "7:54"
            }
        ]
    },
    "message": "Free Course list retrieved successfully",
    "requestKey": "api/freecourse/2"
}</pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5> </p>
      </div>
      <!-- ********************* subscription list ********************************* -->

       <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">16) Subscription list</span>
      </a>
      <div class="accordion-content">
         <p>devicetype = (android or ios)</p>
          <p>
              <h5>Parameters: </h5>
              <pre>


                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>deviceType: android or ios</p>
          <p> <h5>requestKey: </h5>subscriptionlist</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "subscriptionlist": [
        {
            "subscription_id": "1",
            "title": "English In Competitions",
            "description": "This course is mainly helpfull for Banking sector students",
            "price": "3500",
            "facilities": [
                "Live Classes",
                "Live Video On Demand",
                "Course Videos",
                "Weekly Test"
            ]
        },
        {
            "subscription_id": "2",
            "title": "Hindi In Competitions",
            "description": "This course is mainly helpfull for Banking sector students",
            "price": "5000",
            "facilities": [
                "Live Classes",
                "Live Video On Demand",
                "Course Videos",
                "Weekly Test"
            ]
        }
    ],
    "message": "Subscription List retrieved successfully",
    "requestKey": "api/subscriptionlist"
}</pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5> </p>
      </div>

      <!-- ********************* assignments list ********************************* -->

       <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">17) Assignments list</span>
      </a>
      <div class="accordion-content">
         <p>devicetype = (android or ios)</p>
          <p>
              <h5>Parameters: </h5>
              <pre>


                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>deviceType: android or ios</p>
          <p> <h5>requestKey: </h5>assignments</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "assignmentlist": [
        {
            "title": "Assignments A",
            "file": "Http://mobuloustech.com/asecapi/public/assignments/ssc-mathematics-chapterwise(sscguides.com).pdf",
            "solution": "Http://mobuloustech.com/asecapi/public/assignments/ssc-mathematics-chapterwise(sscguides.com).pdf"
        },
        {
            "title": "Assignments B",
            "file": "Http://mobuloustech.com/asecapi/public/assignments/ssc-mathematics-chapterwise(sscguides.com).pdf",
            "solution": "Http://mobuloustech.com/asecapi/public/assignments/ssc-mathematics-chapterwise(sscguides.com).pdf"
        }
    ],
    "message": "Assignments List retrieved successfully",
    "requestKey": "api/assignmentlist"
}</pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5> </p>
      </div>

       <!-- ********************* Book list ********************************* -->

       <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">18) Book list</span>
      </a>
      <div class="accordion-content">
         <p>devicetype = (android or ios)</p>
          <p>
              <h5>Parameters: </h5>
              <pre>


                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>deviceType: android or ios</p>
          <p> <h5>requestKey: </h5>booklist</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "booklist": [
        {
            "title": "SSC Mathematics",
            "description": "This book is helpfull for ssc students",
            "image": "http://mobuloustech.com/asecapi/public/book/maths.jpeg",
            "book_id": "1"
        },
        {
            "title": "SSC History",
            "description": "This book is helpfull for ssc students",
            "image": "http://mobuloustech.com/asecapi/public/book/history.jpg",
            "book_id": "2"
        },
        {
            "title": "SSC English",
            "description": "This book is helpfull for ssc students",
            "image": "http://mobuloustech.com/asecapi/public/book/eng.jpg",
            "book_id": "3"
        }
    ],
    "message": "Book List retrieved successfully",
    "requestKey": "api/booklist"
}
</pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5> </p>
      </div>

        <!-- ********************* Book list ********************************* -->

       <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">19) Book Details</span>
      </a>
      <div class="accordion-content">
         <p>devicetype = (android or ios)</p>
          <p>
              <h5>Parameters: </h5>
              <pre>


                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>deviceType: android or ios</p>
          <p> <h5>requestKey: </h5>booklist</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "bookdetails": [
        {
            "title": "SSC Mathematics",
            "description": "This book is helpfull for ssc students",
            "image": "http://mobuloustech.com/asecapi/public/book/maths.jpeg",
            "book_id": "1",
            "price": "500"
        }
    ],
    "message": "Book Details retrieved successfully",
    "requestKey": "api/bookdetails/1"
}
</pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5> </p>
      </div>

       <!-- ********************* Book list ********************************* -->

       <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">20) Place Book order</span>
      </a>
      <div class="accordion-content">
         <p>devicetype = (android or ios)</p>
          <p>
              <h5>Parameters: </h5>
              <pre>
                token:required
                user_id:required
                book_id:required
                address1:required
                city:required
                pincode:required
                address2:required

                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>deviceType: android or ios</p>
          <p> <h5>requestKey: </h5>booklist</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "placeorder": {
        "status": "success"
    },
    "message": "Order Placed successfully",
    "requestKey": "api/placeorder"
}
</pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5> </p>
      </div>

      <!-- **************** ******************************* test list -->

      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">21) Test List</span>
      </a>
      <div class="accordion-content">
        
          <p>
              <h5>Parameters: </h5>
              <pre>
              

                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
        
          <p> <h5>requestKey: </h5>testlist</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "testlist": [
        {
            "series_name": "GENARAL AWARENESS SSC CGL",
            "tests_list": [
                {
                    "name": "Test 1",
                    "test_id": "2"
                }
            ]
        },
        {
            "series_name": "Mathematics SSC CGL",
            "tests_list": [
                {
                    "name": "Test 1",
                    "test_id": "1"
                }
            ]
        }
    ],
    "message": "Test list retrieved successfully",
    "requestKey": "api/testlist"
}
</pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5> </p>
      </div>

  <!-- **************** ******************************* test list -->

      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">22) Start Test</span>
      </a>
      <div class="accordion-content">
        
          <p>
              <h5>Parameters: </h5>
              <pre>
                token:required
                user_id:required
                test_id:required
                priorites:(in start value will be zero otherwise send response value)
                question_id:(in start value will be zero otherwise send response value)
                type:Submit ie .. Next,Prev,Submit(blank in case test start)
                answer:(optional)
                ans_record:in start value will be blank otherwise send response value

                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
        
          <p> <h5>requestKey: </h5>teststart</p>
          <p> <h5>Sample Response: </h5>
          <h6>In Case Test will be satrt( State when type is blank , priorites = 0, question_id= 0)</h6>
            <pre>
{
    "status": "SUCCESS",
    "teststart": {
        "Test_description": "This is a concept-building practice test and may not have exact structure as you would expect in the actual exam. ",
        "name": "Test 1",
        "question": "Successive discount of 10% , 20% and 50% will be equivalent to a single discount of-",
        "option_a": "34%",
        "option_b": "64%",
        "option_c": "80%",
        "option_d": "56%",
        "priorites": 1,
        "finish_flag": "0",
        "start_flag": "1",
        "question_id": "1",
        "ans_record": "",
        "answer": ""
    },
    "message": "Test Series data retrieved successfully",
    "requestKey": "api/teststart"
}
</pre>
       <h6>In that case ( State when type is Next or Prev , priorites != 0, question_id != 0)</h6>
            <pre>
{
    "status": "SUCCESS",
    "teststart": {
        "Test_description": "",
        "name": "Test 1",
        "question": "Of three numbers, the second is thrice the first and the third number is three-fourth of the first. If the average of the three numbers is 114, the largest number is –",
        "option_a": "72",
        "option_b": "216",
        "option_c": "354",
        "option_d": "726",
        "priorites": 2,
        "finish_flag": "0",
        "start_flag": "0",
        "question_id": "2",
        "ans_record": "5",
        "answer": ""
    },
    "message": "Test Series data retrieved successfully",
    "requestKey": "api/teststart"
}
</pre>
<h6>In that case ( State when type is Submit)</h6>
            <pre>
{
    "status": "SUCCESS",
    "teststart": {
        "status": "success",
        "testresult_id": "2",
        "test_id": "1"
    },
    "message": "Test Series submitted successfully",
    "requestKey": "api/teststart"
}
</pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5> </p>
      </div>
  <!-- **************** ******************************* test list -->

      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">23)Review Answer</span>
      </a>
      <div class="accordion-content">
        
          <p>
              <h5>Parameters: </h5>
              <pre>
                token:required
                user_id:required
                testresult_id:required
                test_id:required
                priorites:(intial that is 0 otherwise send me response value)
                question_id:(intial that is 0 otherwise send me response value)
                type:(3 state blank,Next,Prev)

                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
        
          <p> <h5>requestKey: </h5>viewanswers</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "viewanswers": {
        "Test_description": "This is a concept-building practice test and may not have exact structure as you would expect in the actual exam. ",
        "name": "Test 1",
        "question": "Successive discount of 10% , 20% and 50% will be equivalent to a single discount of-",
        "option_a": "34%",
        "option_b": "64%",
        "option_c": "80%",
        "option_d": "56%",
        "priorites": 3,
        "finish_flag": "0",
        "start_flag": "0",
        "question_id": "1",
        "correct_answer": "B",
        "your_answer": "",
        "explanation": "Nothing"
    },
    "message": "Test Series result retrieved successfully",
    "requestKey": "api/viewanswers"
}
</pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5> </p>
      </div>

       <!-- **************** ******************************* test list -->

      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">24)Review Result</span>
      </a>
      <div class="accordion-content">
        
          <p>
              <h5>Parameters: </h5>
              <pre>
                token:required
                user_id:required
                testresult_id:required
                test_id:required
               
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
        
          <p> <h5>requestKey: </h5>viewanswers</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "viewresult": {
        "number_of_correct": 1,
        "number_of_incorrect": 2,
        "otherlisting": [
            {
                "section_name": "Discount",
                "total_question": 3,
                "correct_question": 0
            },
            {
                "section_name": "Series",
                "total_question": 2,
                "correct_question": 1
            }
        ],
        "total": 5
    },
    "message": "Test Series result retrieved successfully",
    "requestKey": "api/viewresult"
}
</pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5> </p>
      </div>


      <!--********************************* social login ******************************** -->

      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">25) Forum videos</span>
      </a>
      <div class="accordion-content">
         <p>devicetype = (android or ios)</p>
          <p>
              <h5>Parameters: </h5>
              <pre>


                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>deviceType: android or ios</p>
          <p> <h5>requestKey: </h5>myforumvideo/video_id</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "myforumvideo": {
        "videodetails": {
            "video_id": "1",
            "video": "http://mobuloustech.com/asec_admin/public/assets/freevideo/4373591.mp4",
            "title": "asdasd",
            "description": "asdasda",
            "thumbnail": "http://mobuloustech.com/asec_admin/public/images/icon/logoadmin.png",
            "duration": "5.533333"
        },
        "otherlisting": []
    },
    "message": "Forum video list retrieved successfully",
    "requestKey": "api/myforumvideo/1"
}</pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5> </p>
      </div>

      <!--********************************* social login ******************************** -->

      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">26) Forum topics</span>
      </a>
      <div class="accordion-content">
         <p>devicetype = (android or ios)</p>
          <p>
              <h5>Parameters: </h5>
              <pre>


                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>deviceType: android or ios</p>
          <p> <h5>requestKey: </h5>discussionlist</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "discussionlist": [
        {
            "topic_id": "3",
            "name": "gtdfgdfg"
        }
    ],
    "message": "Discussion list retrieved successfully",
    "requestKey": "api/discussionlist"
}</pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5> </p>
      </div>

        <!--********************************* social login ******************************** -->

      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">27) Forum topics comments list</span>
      </a>
      <div class="accordion-content">
         <p>devicetype = (android or ios)</p>
          <p>
              <h5>Parameters: </h5>
              <pre>


                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>deviceType: android or ios</p>
          <p> <h5>requestKey: </h5>discussioncommentlist/topic_id/user_id</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "discussioncommentlist": {
        "topic_id": "3",
        "join_flag": "0",
        "comments": [
            {
                "thread_id": "4",
                "msg": "fdsgfhgsdfgdsgf",
                "fullname": "gff",
                "user_id": "5",
                "image": "http://mobuloustech.com/asecapi/public/img/user_signup.png",
                "created_at": "5 hours ago"
            },
            {
                "thread_id": "3",
                "msg": "fdsgfhgsdfgdsgf",
                "fullname": "gff",
                "user_id": "5",
                "image": "http://mobuloustech.com/asecapi/public/img/user_signup.png",
                "created_at": "5 hours ago"
            },
            {
                "thread_id": "2",
                "msg": "fdsgfhgsdfgdsgf",
                "fullname": "gff",
                "user_id": "5",
                "image": "http://mobuloustech.com/asecapi/public/img/user_signup.png",
                "created_at": "5 hours ago"
            },
            {
                "thread_id": "1",
                "msg": "hello",
                "fullname": "mahipal",
                "user_id": "7",
                "image": "http://mobuloustech.com/asecapi/public/img/user_signup.png",
                "created_at": "8 hours ago"
            }
        ]
    },
    "message": "Comment list retrieved successfully",
    "requestKey": "api/discussioncommentlist/3/7"
}</pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5> </p>
      </div>

        <!--********************************* social login ******************************** -->

      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">28) Forum discussion join</span>
      </a>
      <div class="accordion-content">
         <p>devicetype = (android or ios)</p>
          <p>
              <h5>Parameters: </h5>
              <pre>
                token:required
                user_id:required
                topic_id:required

                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>deviceType: android or ios</p>
          <p> <h5>requestKey: </h5>discussionjoinbyuser</p>
          <p> <h5>Sample Response: </h5>
            <pre>
{
    "status": "SUCCESS",
    "discussionjoinbyuser": {
        "status": "success"
    },
    "message": "Discussion join successfully",
    "requestKey": "api/discussionjoinbyuser"
}</pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5> </p>
      </div>

        <!--********************************* social login ******************************** -->

      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">29) Forum discussion send msg</span>
      </a>
      <div class="accordion-content">
         <p>devicetype = (android or ios)</p>
          <p>
              <h5>Parameters: </h5>
              <pre>
                token:required
                user_id:required
                topic_id:required
                msg:required

                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>deviceType: android or ios</p>
          <p> <h5>requestKey: </h5>sendmsg</p>
          <p> <h5>Sample Response: </h5>
{
    "status": "SUCCESS",
    "sendmsg": {
        "user_id": "5",
        "image": "http://mobuloustech.com/asecapi/public/img/user_signup.png",
        "msg": "fdsgfhgsdfgdsgf",
        "time": "1 second ago",
        "fullname": "gff"
    },
    "message": "Message sent successfully",
    "requestKey": "api/sendmsg"
}</pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5> </p>
      </div>
 

  <!--********************************* social login ******************************** -->

      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">30) check cupon code</span>
      </a>
      <div class="accordion-content">
         <p>devicetype = (android or ios)</p>
          <p>
              <h5>Parameters: </h5>
              <pre>
                token:required
                user_id:required
                cupon_code:required 

                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>deviceType: android or ios</p>
          <p> <h5>requestKey: </h5>checkcupon</p>
          <p> <h5>Sample Response: </h5>
{
    "status": "SUCCESS",
    "checkcupon": {
        "status": "success"
    },
    "message": "Cupon available",
    "requestKey": "api/checkcupon"
}</pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5> </p>
      </div>


  <!--********************************* social login ******************************** -->

      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">31)reply</span>
      </a>
      <div class="accordion-content">
         <p>devicetype = (android or ios)</p>
          <p>
              <h5>Parameters: </h5>
              <pre>
                token:required
                user_id:required
                thread_id:required 
                msg:required

                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>deviceType: android or ios</p>
          <p> <h5>requestKey: </h5>reply</p>
          <p> <h5>Sample Response: </h5>
{
    "status": "SUCCESS",
    "checkcupon": {
        "status": "success"
    },
    "message": "Msg sent successfully",
    "requestKey": "api/checkcupon"
}</pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5> </p>
      </div>

  <!--********************************* social login ******************************** -->

      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">32)Help us</span>
      </a>
      <div class="accordion-content">
         <p>devicetype = (android or ios)</p>
          <p>
              <h5>Parameters: </h5>
              <pre>
                token:required
                user_id:required
                msg:required

                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>deviceType: android or ios</p>
          <p> <h5>requestKey: </h5>helpus</p>
          <p> <h5>Sample Response: </h5>
{
    "status": "SUCCESS",
    "helpus": {
        "status": "success"
    },
    "message": "Help Message sent successfully",
    "requestKey": "api/helpus"
}</pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5> </p>
      </div>

 
<!--********************************* social login ******************************** -->

      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">33)My Order</span>
      </a>
      <div class="accordion-content">
         <p>devicetype = (android or ios)</p>
          <p>
              <h5>Parameters: </h5>
              <pre>
                

                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>deviceType: android or ios</p>
          <p> <h5>requestKey: </h5>myorder/user_id</p>
          <p> <h5>Sample Response: </h5>
<pre>
{
    "status": "SUCCESS",
    "myorder": {
        "past_order": [
            {
                "order_id": 8,
                "title": "SSC History",
                "image": "http://mobuloustech.com/asecapi/public/book/history.jpg",
                "price": "800",
                "order_date": "08 Jan 2019",
                "status": "3",
                "address1": "sadf",
                "address2": "asfcaf",
                "city": "afdaf",
                "pincode": "asdfadf",
                "shipment_date": "09 Jan 2019",
                "qty": "1",
                "status_name": "Shipped"
            }
        ],
        "ongoing_order": [
            {
                "order_id": 6,
                "title": "SSC Mathematics",
                "image": "http://mobuloustech.com/asecapi/public/book/maths.jpeg",
                "price": "500",
                "order_date": "08 Jan 2019",
                "status": "0",
                "address1": "jjfpj",
                "address2": "jajpfj",
                "city": "kadaj[f[a[",
                "pincode": "2066011",
                "shipment_date": "08 Jan 2019",
                "qty": "1",
                "status_name": "Order"
            },
            {
                "order_id": 7,
                "title": "SSC Mathematics",
                "image": "http://mobuloustech.com/asecapi/public/book/maths.jpeg",
                "price": "500",
                "order_date": "08 Jan 2019",
                "status": "0",
                "address1": "fg",
                "address2": "bxcb",
                "city": "cbb",
                "pincode": "206001",
                "shipment_date": "08 Jan 2019",
                "qty": "1",
                "status_name": "Order"
            }
        ]
    },
    "message": "My Order list retrieved successfully",
    "requestKey": "api/myorder/8"
}
</pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5> </p>
      </div>


<!--********************************* social login ******************************** -->

      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">34)Notification status</span>
      </a>
      <div class="accordion-content">
         <p>devicetype = (android or ios)</p>
          <p>
              <h5>Parameters: </h5>
              <pre>
                  token:required
                user_id:required

                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>deviceType: android or ios</p>
          <p> <h5>requestKey: </h5>notifystatus</p>
          <p> <h5>Sample Response: </h5>
<pre>
{
    "status": "SUCCESS",
    "notifystatus": {
        "status": "0"
    },
    "message": "Notification status change successfully",
    "requestKey": "api/notifystatus"
}
</pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5> </p>
      </div>


<!--********************************* social login ******************************** -->

      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">35)Get Doubts Session</span>
      </a>
      <div class="accordion-content">
         <p>devicetype = (android or ios)</p>
          <p>
              <h5>Parameters: </h5>
              <pre>
                

                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>deviceType: android or ios</p>
          <p> <h5>requestKey: </h5>doubtsesion</p>
          <p> <h5>Sample Response: </h5>
<pre>
{
    "status": "SUCCESS",
    "doubtsesion": {
        "id": "1",
        "name": "Travel Agent Meetup",
        "venue": "Team Co Work 55, 2nd Floor, Westend Marg, Saidullajab, Near Saket Metro Station Gate, Gali Number 2, Saiyad Ul Ajaib, Sainik Farm, New Delhi, Delhi 110030, India",
        "start_time": "Sat, 16 Feb 1:00PM - 5:00PM",
        "created_at": "2019-01-25 09:34:22",
        "updated_at": "0000-00-00 00:00:00"
    },
    "message": "Doubtsesion retrieved successfully",
    "requestKey": "api/doubtsesion"
}
</pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5> </p>
      </div>


<!--********************************* social login ******************************** -->

      <a href="#" class="accordion-toggle"><span class="toggle-icon"><i class="fa fa-plus-circle"></i></span>
          <span style="margin-left:50px;line-height: 1.5em;">36)User registration and admit card </span>
      </a>
      <div class="accordion-content">
         <p>devicetype = (android or ios)</p>
          <p>
              <h5>Parameters: </h5>
              <pre>
                  
                </pre>
          </p>
          <p> <h5>Response Required: </h5>SUCCESS or FAILURE</p>
          <p> <h5>Brief Description: </h5>deviceType: android or ios</p>
          <p> <h5>requestKey: </h5>myPDF/user_id/event_id</p>
          <p> <h5>Sample Response: </h5>
<pre>
{
    "status": "SUCCESS",
    "myPDF": {
        "admitcard": "http://mobuloustech.com/asecapi/public/admitcard1548421365admitcard.pdf"
    },
    "message": "Doubtsesion retrieved successfully",
    "requestKey": "api/myPDF/51/1"
}
</pre>
          </p>
          <p> <h5>Test Status: </h5></p>
          <p> <h5>Notes: </h5> </p>
      </div>

 

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function () {
      $('.accordion-toggle').on('click', function(event){
        event.preventDefault();

        // create accordion variables
        var accordion = $(this);
        var accordionContent = accordion.next('.accordion-content');
        var accordionToggleIcon = $(this).children('.toggle-icon');

        // toggle accordion link open class
        accordion.toggleClass("open");

        // toggle accordion content
        accordionContent.slideToggle(250);

        // change plus/minus icon
        if (accordion.hasClass("open")) {
          accordionToggleIcon.html("<i class='fa fa-minus-circle'></i>");
        } else {
          accordionToggleIcon.html("<i class='fa fa-plus-circle'></i>");
        }
      });
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function () {
      $('ul.nav a').each(function(){
        if(location.href === this.href){
          $(this).addClass('active');
          $('ul.nav a').not(this).removeClass('active');
          return false;
        }
      });
    });
    </script>
</body>
</html>