<!-- NAVBAR -->
<nav class="navbar navbar-default navbar-fixed-top">
     <!-- <div class="brand">
       <a href="javascript::void(0);">
            <img alt="Linqq Logo" width="50px" class="img-responsive logo" src="{{url('/')}}/public/img/logo-admin.png"/>
        </a> 
    </div> -->
    <div class="container-fluid">
        
        <form class="navbar-form navbar-left">
            
        </form>
        
        
        <header class="header-desktop">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            <form class="form-header" action="" method="POST">
                                <!--<input class="au-input au-input--xl" type="text" name="search" placeholder="Search for datas &amp; reports...">
                                <button class="au-btn--submit" type="submit">
                                    <i class="zmdi zmdi-search"></i>
                                </button> -->
                            </form>
                            <div class="header-button">
                                <div class="hambergmenu"><i class="fas fa-bars"></i></div>
								
								<!-- For Notification Panel Here-->
								
                                <div class="noti-wrap">
								<div class="noti__item js-item-menu">
                                        <i class="zmdi zmdi-notifications"></i>
                                        <span class="quantity">3</span>
                                        <div class="notifi-dropdown js-dropdown">
                                            <div class="notifi__title">
                                                <p>You have 3 Notifications</p>
                                            </div>
                                            <div class="notifi__item">
                                                <div class="bg-c1 img-cir img-40">
                                                    <i class="zmdi zmdi-email-open"></i>
                                                </div>
                                                <div class="content">
                                                    <p>You got a email notification</p>
                                                    <span class="date">April 12, 2018 06:50</span>
                                                </div>
                                            </div>
                                            <div class="notifi__item">
                                                <div class="bg-c2 img-cir img-40">
                                                    <i class="zmdi zmdi-account-box"></i>
                                                </div>
                                                <div class="content">
                                                    <p>Your account has been blocked</p>
                                                    <span class="date">April 12, 2018 06:50</span>
                                                </div>
                                            </div>
                                            <div class="notifi__item">
                                                <div class="bg-c3 img-cir img-40">
                                                    <i class="zmdi zmdi-file-text"></i>
                                                </div>
                                                <div class="content">
                                                    <p>You got a new file</p>
                                                    <span class="date">April 12, 2018 06:50</span>
                                                </div>
                                            </div>
                                            <div class="notifi__footer">
                                                <a href="#">All notifications</a>
                                            </div>
                                        </div>
                                    </div>
									<!-- For Notification Panel Ends Here-->
									
									<!-- For chat Panel -->
									
                                    <div class="noti__item js-sidebar-btn mychatopn">
                                        <i class="zmdi zmdi-comment-more"></i>
                                        <span class="quantity">1</span>
                                    </div>
                                   <div class="setting-menu  js-right-sidebar d-none d-lg-block">
                                    
                                    <div class=" js-item-menu show-dropdown">

                                        <div class="">
                                            <div class="mess__title">
                                                <p>You have 2 news message</p>
                                            </div>
                                            <div class="mess__item">
                                                <a href="http://mobuloustech.com/monami/chat"><div class="image img-cir img-40">
                                                    <img src="/monami/images/icon/avatar-01.jpg" alt="Michelle Moreno">
                                                </div>
                                                <div class="content  mychatopn">
                                                    <h6>Michelle Moreno</h6>
                                                    <p>Have sent a photo</p>
                                                    <span class="time">3 min ago</span>
                                                </div> 
											    </a>
                                            </div>
                                           <div class="mess__item">
                                                <a href="http://mobuloustech.com/monami/chat"><div class="image img-cir img-40">
                                                    <img src="/monami/images/icon/avatar-01.jpg" alt="Michelle Moreno">
                                                </div>
                                                <div class="content  mychatopn">
                                                    <h6>Michelle Moreno</h6>
                                                    <p>Have sent a photo</p>
                                                    <span class="time">3 min ago</span>
                                                </div> 
											    </a>
                                            </div>
											
											<div class="mess__item">
                                                <a href="http://mobuloustech.com/monami/chat"><div class="image img-cir img-40">
                                                    <img src="/monami/images/icon/avatar-01.jpg" alt="Michelle Moreno">
                                                </div>
                                                <div class="content  mychatopn">
                                                    <h6>Michelle Moreno</h6>
                                                    <p>Have sent a photo</p>
                                                    <span class="time">3 min ago</span>
                                                </div> 
											    </a>
                                            </div>
                                            
                                        </div>
                                    </div>
									
									
								<div class="mess__footer">
                                                <a href="http://mobuloustech.com/monami/chatroom">View all messages</a>
                                            </div>
                                </div>
								
								
								<!-- For chat Panel ENDS HERE-->
                                    
                                </div>
                                <div class="account-wrap">
                                    <div class="account-item clearfix js-item-menu">
                                        <div class="image">
                                            <img src="{{url('/')}}/public/{{Auth::user()->image}}" alt="John Doe">
                                        </div>
                                        <div class="content">
                                            <a class="js-acc-btn" href="#"> {{Auth::user()->fullname}}</a>
                                        </div>
                                        <div class="account-dropdown js-dropdown">
                                            <div class="info clearfix">
                                              <!--   <div class="image">
                                                    <a href="#">
                                                        <img src="{{url('/')}}/public/{{Auth::user()->image}}" alt="John Doe"> 
                                                    </a>
                                                </div> -->
								<div class="image"><img alt="Avatar" class="img-circle" src="{{url('/')}}/public/{{Auth::user()->image}}"></div>
                                                <div class="content">
                                                    <h5 class="name">
                                                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                       
                            <span>
                                {{Auth::user()->fullname}}
                            </span>
                           
                            </i>
                        </img>
                    </a>
                                                    </h5>
                                                    <span class="email">{{Auth::user()->email}}</span>
                                                </div>
                                            </div>
                                            <!-- <div class="account-dropdown__body">
                                                <div class="account-dropdown__item">
                                                   <a href="{{ url('admin/profile') }}">
                                                        <i class="zmdi zmdi-account"></i>Account</a>
                                                </div>
                                                <div class="account-dropdown__item">
                                                    <a href="#">
                                                        <i class="zmdi zmdi-settings"></i>Setting</a>
                                                </div>
                                            </div> -->
                                            <div class="account-dropdown__footer">
                                                <a href="{{ url('logout') }}">
                                                    <i class="zmdi zmdi-power"></i>Logout</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
      <!--  <div id="navbar-menu">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <img alt="Avatar" class="img-circle" src="{{url('/')}}/public/profile/{{Auth::user()->image}}">
                            <span>
                                {{Auth::user()->name}}
                            </span>
                            <i class="icon-submenu lnr lnr-chevron-down">
                            </i>
                        </img>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="{{ url('profile') }}">
                                <i class="lnr lnr-user">
                                </i>
                                <span>
                                    My Profile
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('logout') }}">
                                <i class="lnr lnr-exit">
                                </i>
                                <span>
                                    Logout
                                </span>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div> -->
    </div>
</nav>
<!-- END NAVBAR -->
