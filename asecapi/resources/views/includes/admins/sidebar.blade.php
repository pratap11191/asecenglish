<!-- LEFT SIDEBAR -->
<aside class="menu-sidebar d-none d-lg-block">
<div class="logo">

<div class="closemenuico"><i class="fas fa-times"></i></div>
                <a href="index.html">
                    <img alt="Linqq Logo" width="50px" class="img-responsive logo" src="{{url('/')}}/public/img/logo-admin.png"/>
                </a>
            </div>
        <div id="sidebar-nav" class="">
		
		<!-- <div class="menu-sidebar__content js-scrollbar1 ps">
                <nav class="navbar-sidebar">
                    <ul class="list-unstyled navbar__list">
					 <li class="tablinks active" onclick="openCity(event, 'Home')">
                            <a href="#">
                                <i class="fas fa-home"></i>Home</a>
                        </li>
						
						
						<li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-tachometer-alt"></i>Nursery Profile</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li class="tablinks" onclick="openCity(event, 'Definegrades')">
									<a href="#"><i class="fas fa-graduation-cap"></i>Define Grades</a>
								</li>
                                <li class="tablinks" onclick="openCity(event, 'Addclassrooms')">
                                    <a href="#"><i class="fas fa-users"></i>Add Classrooms</a>
                                </li>
                                <li class="tablinks" onclick="openCity(event, 'Addnannies')">
                                   <a href="#"><i class="fas fa-female"></i>Add Nannies</a>
                                </li>
                                <li class="tablinks" onclick="openCity(event, 'Addchildren')">
                                    <a href="#"><i class="fas fa-child"></i>Add Children</a>
                                </li>
                            </ul>
                        </li>
						
					
						
						<li>
                            <a href="#">
                                <i class="fas fa-child"></i>View Children Profile</a>
                        </li>
						
						<li class="tablinks" onclick="openCity(event, 'askforsup')">
                            <a href="#">
                                <i class="fas fa-headphones"></i>Ask for support</a>
                        </li>
						
						<li class="tablinks" onclick="openCity(event, 'foodmenu')">
                            <a href="#">
                                <i class="fas fa-utensils"></i>Define Menu</a>
                        </li>
                    </ul>
                </nav>
            <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div> -->
			
			<div class="menu-sidebar__content js-scrollbar1 ps">
            <div class="sidebar-scroll">
                <nav class="navbar-sidebar">
                    <?php $url=Request::url(); ?>
                    <?php
                        if(Auth::user()->is_admin == '1'){
                    ?>
                    <ul class="nav list-unstyled navbar__list">
                        <li>
                            <a href="{{ url('/user_list') }}" <?php if(strpos($url, "user_list")): ?> class="active"<?php  endif ?>> <i class="fas fa-users"></i> Nursery List</a>
                        </li>
                       
                        <li>
                            <a href="{{ url('/parent_list') }}" <?php if(strpos($url, "/parent_list")): ?> class="active"<?php  endif ?>><i class="fas fa-female"></i>Parents List</a>
                        </li>

<div id="google_translate_element"></div>
                       
                    </ul>
                    <?php
                        } else { ?>

                        <ul class="nav list-unstyled navbar__list">
						
						<li class="has-sub">
						<a class="js-arrow" href="#">
                                <i class="fas fa-tachometer-alt"></i>Nursery Profile</a>
							<ul class="list-unstyled navbar__sub-list js-sub-list">	
                        <li>
                            <a href="{{ url('/grades_list') }}" <?php if(strpos($url, "/grades_list")): ?> class="active"<?php  endif ?>><i class="fas fa-graduation-cap"></i> <span>Define Grades</span></a>
                        </li>
                       
                        <li>
                            <a href="{{ url('/classroom_list') }}" <?php if(strpos($url, "/classroom_list")): ?> class="active"<?php  endif ?>><i class="fas fa-users"></i> <span>Classroom List</span></a>
                        </li>

                          <li>
                            <a href="{{ url('/nannies_list') }}" <?php if(strpos($url, "/nannies_list")): ?> class="active"<?php  endif ?>><i class="fas fa-female"></i><span>Nannies List</span></a>
                        </li>

                          <li>
                            <a href="{{ url('/children_list') }}" <?php if(strpos($url, "/children_list")): ?> class="active"<?php  endif ?>><i class="fas fa-child"></i><span>Children List</span></a>
                        </li>
						
						<li>
                            <a href="{{ url('/report_list') }}" <?php if(strpos($url, "/report_list")): ?> class="active"<?php  endif ?>><i class="fas fa-child"></i><span>View Reports</span></a>
                        </li>
						</li>
						</ul>
						
						<li class="tablinks" onclick="openCity(event, 'askforsup')">
                            <a href="{{url('/faqnursery')}}" <?php if(strpos($url, "/faqnursery")): ?> class="active"<?php  endif ?>>
                                <i class="fas fa-headphones"></i>Ask for support</a>
                        </li>
						
						<li class="tablinks" onclick="openCity(event, 'foodmenu')">
                            <a href="{{url('/menu_list')}}" <?php if(strpos($url, "/menu_list")): ?> class="active"<?php  endif ?>>
                                <i class="fas fa-utensils"></i>Define Menu</a>
                        </li>
                       
                    </ul>
                        <?php

                        }
                    ?>
                </nav>
            </div>
			</div>
        </div>
<!-- END LEFT SIDEBAR -->
</aside>

<script type="text/javascript">

function googleTranslateElementInit() {
 new google.translate.TranslateElement({
 pageLanguage: 'en',
 includedLanguages: "en,pt",

 multilanguagePage: true
 }, 'google_translate_element');

if(typeof(document.querySelector) == 'function') {
        document.querySelector('.goog-logo-link').setAttribute('style', 'display: none');
        document.querySelector('.goog-te-gadget').setAttribute('style', 'font-size: 0');
    }


 }

   
</script>


<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
