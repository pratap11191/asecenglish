 
<link href="{{ asset('/public/vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet"></link>
<link href="{{ asset('/public/vendor/linearicons/style.css') }}" rel="stylesheet"></link>
<link href="{{ asset('/public/css/main.css') }}" rel="stylesheet"></link>
<link href="{{ asset('/public/css/demo.css') }}" rel="stylesheet"></link>
<link href="{{ asset('/public/DataTables/datatables.min.css') }}" rel="stylesheet"></link>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>




<!-- Fontfaces CSS-->
    <link href="{{ asset('/public/vendor/monarss/css/font-face.css') }}" rel="stylesheet" media="all">
	 
	 <link href="{{ asset('/public/vendor/monarss/font-awesome-4.7/css/font-awesome.min.css') }}" rel="stylesheet" media="all">
	 <link href="{{ asset('/public/vendor/monarss/font-awesome-5/css/fontawesome-all.min.css') }}" rel="stylesheet" media="all">
	 
    <link href="{{ asset('/public/vendor/monarss/mdi-font/css/material-design-iconic-font.min.css') }}" rel="stylesheet" media="all">
	<link rel="icon" href="{{ asset('/public/vendor/monarss/img/icon/appicon_2.png') }}" type="image/png" sizes="16x16">

    <!-- Bootstrap CSS-->
	 <link href="{{ asset('/public/vendor/monarss/bootstrap-4.1/bootstrap.min.css') }}" rel="stylesheet" media="all">
    

    <!-- Vendor CSS-->
	 <link href="{{ asset('/public/vendor/monarss/animsition/animsition.min.css') }}" rel="stylesheet" media="all">
	 <link href="{{ asset('/public/vendor/monarss/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css') }}" rel="stylesheet" media="all">
	 <link href="{{ asset('/public/vendor/monarss/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css') }}" rel="stylesheet" media="all">
     <link href="{{ asset('/public/vendor/monarss/wow/animate.css') }}" rel="stylesheet" media="all">
	 <link href="{{ asset('/public/vendor/monarss/css-hamburgers/hamburgers.min.css') }}" rel="stylesheet" media="all">
	 <link href="{{ asset('/public/vendor/monarss/slick/slick.css') }}" rel="stylesheet" media="all">
	 <link href="{{ asset('/public/vendor/monarss/select2/select2.min.css') }}" rel="stylesheet" media="all">
	 <link href="{{ asset('/public/vendor/monarss/perfect-scrollbar/perfect-scrollbar.css') }}" rel="stylesheet" media="all">
    
   
     

    <!-- Main CSS-->
 <link href="{{ asset('/public/vendor/monarss/css/theme.css') }}" rel="stylesheet" media="all">
  <script type="text/javascript" src="{{ asset('/public/vendor/monarss/jquery-3.2.1.min.js') }}"></script>
 <script type="text/javascript" src="{{ asset('/public/vendor/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/public/scripts/klorofil-common.js') }}"></script>
<script type="text/javascript" src="{{ asset('/public/DataTables/datatables.min.js') }}"></script>

 
 
 <script src="{{ asset('/public/vendor/monarss/wow/wow.min.js') }}"></script>
	   <script src="{{ asset('/public/vendor/monarss/animsition/animsition.min.js') }}"></script> 
    
    <script src="{{ asset('/public/vendor/monarss/bootstrap-progressbar/bootstrap-progressbar.min.js') }}"></script>
	<script src="{{ asset('/public/vendor/monarss/counter-up/jquery.waypoints.min.js') }}"></script>
	<script src="{{ asset('/public/vendor/monarss/circle-progress/circle-progress.min.js') }}"></script>
	
	<script src="{{ asset('/public/vendor/monarss/counter-up/jquery.counterup.min.js') }}"></script>
	<script src="{{ asset('/public/vendor/monarss/perfect-scrollbar/perfect-scrollbar.js') }}"></script>
	<script src="{{ asset('/public/vendor/monarss/chartjs/Chart.bundle.min.js') }}"></script>
	<script src="{{ asset('/public/vendor/monarss/select2/select2.min.js') }}"></script>
    
	


    <!-- Main JS-->
	
	 <script src="{{ asset('/public/vendor/monarss/js/main.js') }}"></script>
	<script src="{{ asset('/public/vendor/monarss/js/monami.js') }}"></script> 
	
	
	