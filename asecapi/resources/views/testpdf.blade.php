<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Asec Test</title>
	
   <style>
      

a {
  color: #5D6975;
  text-decoration: underline;
}

body {
  position: relative;
  width: 18cm;
  height: 29.7cm;
  margin: 0 auto;
  color: #001028;
  background: #FFFFFF;
  font-family: Arial, sans-serif;
  font-size: 12px;
  font-family: Arial;
  border: 1px solid #ddd;
  padding: 10px;
}

header {
  padding: 10px 0;
  margin-bottom: 30px;
}

#logo {
  text-align: center;
  margin-bottom: 10px;
}

#logo img {
  width: 151px;
}

h1 {
  border-top: 1px solid  #5D6975;
  border-bottom: 1px solid  #5D6975;
  color: #5D6975;
  font-size: 2.4em;
  line-height: 1.4em;
  font-weight: normal;
  text-align: left;
  margin: 0 0 20px 0;
  background: url(dimension.png);
}



#project span {
  color: #000000;
  text-align: left;
  margin-right: 10px;
  font-size: 11px;
  font-weight: 700;
  text-transform: uppercase;
}

#company {
  float: left;
  text-align: left;
  width:100%;
  margin-bottom:10px;
}

h1{text-align:center;}


#notices .notice {
  color: #5D6975;
  font-size: 1.2em;
}

footer {
  color: #5D6975;
  width: 100%;
  height: 30px;
  position: absolute;
  bottom: 0;
  border-top: 1px solid #C1CED9;
  padding: 8px 0;
  text-align: center;
}

#company h2 {
    margin: 0;
}

div#basicdtls {
    margin: 20px 0 0;
  float:left;
  width:100%;
}




   </style>
  </head>
  <body>
    <header class="clearfix">
      <div id="logo">
        <img src="{{url('/')}}/public/logo-admin.png">
      </div>
    </header>
      <h1>{{ucfirst($testtitle)}}</h1>

      <?php

        $counter = 0;

          if(!empty($dbac)){

            foreach ($dbac as $value) {
          
             $counter++;   
      ?>

    <div class="setquest">
    <div class="questasec">
     <h3>Direction:{{$value->explanation}}</h3> 
    <h3>Q-{{$counter}} : {{$value->title}}</h3>
    <div class="optplaysec">
    <ul>
      <li>Option A: {{$value->option_a}}</li>
       <li>Option B: {{$value->option_b}}</li>
        <li>Option C: {{$value->option_c}}</li>
         <li>Option D: {{$value->option_d}}</li>
         <?php if(!empty($value->option_e)){ ?>
          <li>Option E: {{$value->option_e}}</li>
          <?php } ?>
    </ul>
     <div class="answerasec">

      <p class="answersecd"> Correct Answer:
      <?php 

        if($value->ans == "option_a"){
            echo "A";
        } else if($value->ans == "option_d"){
             echo "D";
        } else if($value->ans == "option_c"){
             echo "C";
        }
        else if($value->ans == "option_b"){
             echo "B";
        }else if($value->ans == "option_e"){
             echo "E";
        }


      ?>
      </p>
    </div>
    <div class="answerasec">
     
      <p class="answersecd"> Your Answer:
      <?php 

        if($value->user_ans == "option_a"){
            echo "A";
        } else if($value->user_ans == "option_d"){
             echo "D";
        } else if($value->user_ans == "option_c"){
             echo "C";
        }
        else if($value->user_ans == "option_b"){
             echo "B";
        }else if($value->user_ans == "option_e"){
             echo "E";
        }


      ?>
      </p>
    </div>
    </div>

    </div>

   
    </div>

  <?php } } ?>
    
    
  
    
    <footer>
     Test Series was created on a computer and is valid without the signature and seal.
    </footer>
  
</body>
</html>