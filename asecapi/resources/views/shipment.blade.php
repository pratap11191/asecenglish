<!DOCTYPE html>
<html lang="en" >

<head>
  <meta charset="UTF-8">
  <title>Shipment details</title>
  
  
  
     <style type="text/css">
     	body {
			background-color: #d7d6d3;
			font-family:'verdana';
		}
		.id-card-holder {
			width: 439px;
			padding: 10px;
			margin: 0 auto;
			background-color: #ffffff;
			border-radius: 20px;
			position: relative;
		}
		
		
		.id-card {
			background-color: #fff;
			padding: 10px;
			border-radius: 20px;
			text-align: center;
			height: 449px;
			border: 1px solid #000;
		}
		.id-card img {
			margin: 0 auto;
		}
		.header img {
			width: 100px;
    		margin-top: 15px;
		}
		.photo img {
			width: 80px;
			margin-top: 0;
		}
		h2 {
			font-size: 15px;
			margin: 5px 0;
		}
		h3 {
			font-size: 12px;
			margin: 2.5px 0;
			font-weight: 300;
		}
		.qr-code img {
			width: 100%;
		}
		p {
			font-size: 10px;
			margin: 2px;
		}
		.id-card-hook {
			background-color: #000;
		    width: 70px;
		    margin: 0 auto;
		    height: 15px;
		    border-radius: 5px 5px 0 0;
		}
		.id-card-hook:after {
			content: '';
		    background-color: #d7d6d3;
		    width: 47px;
		    height: 6px;
		    display: block;
		    margin: 0px auto;
		    position: relative;
		    top: 6px;
		    border-radius: 4px;
		}
		.id-card-tag-strip {
			width: 45px;
		    height: 40px;
		    background-color: #0950ef;
		    margin: 0 auto;
		    border-radius: 5px;
		    position: relative;
		    top: 9px;
		    z-index: 1;
		    border: 1px solid #0041ad;
		}
		.id-card-tag-strip:after {
			content: '';
		    display: block;
		    width: 100%;
		    height: 1px;
		    background-color: #c1c1c1;
		    position: relative;
		    top: 10px;
		}
		.id-card-tag {
			width: 0;
			height: 0;
			border-left: 100px solid transparent;
			border-right: 100px solid transparent;
			border-top: 100px solid #0958db;
			margin: -10px auto -30px auto;
		}
		.id-card-tag:after {
			content: '';
		    display: block;
		    width: 0;
		    height: 0;
		    border-left: 50px solid transparent;
		    border-right: 50px solid transparent;
		    border-top: 100px solid #d7d6d3;
		    margin: -10px auto -30px auto;
		    position: relative;
		    top: -130px;
		    left: -50px;
		}
		
		.photo {
    float: left;
    width: 63px;
}

.photo img {
    width: 100%;
}



.detailedinf h2 {
    font-size: 11px;
}

hr {
    float: left;
    width: 100%;
}

.orgdtls {
    float: left;
    width: 100%;
}

.detailedinf label {
    width: 100px;
    float: left;
    color: #505256;
    text-align: left;
}

.detailedinf{
	margin:20px 0;
}

.qr-code {
    float: right;
    width: 61%;
    margin: 0px 0 0 0;
}

.refodrs ul {
    list-style-type: none;
    padding: 0;
    margin: 0;
}

.refodrs {
    float: left;
    width: 100%;
    text-align: left;
    border-top: 1px solid #000;
    border-bottom: 1px solid #000;
    margin: 9px 0;
}

.refodrs ul li {
    display: inline-block;
    width: 49%;
    font-size: 12px;
}

.productcode {
    float: left;
}

.productcode span {
    font-size: 15px;
    font-weight: 500;
    text-align: left;
    float: left;
}

.detailedinf span{
	text-align: left;

}

.imglogo{
	text-align: center;
	margin: 40px 0;
	width: 100%;
	float: left;

}
table .Form span {
    font-size: 12px;
}

.imglogo img{
	width:80px;
}
.spans {
    margin: 0px 24px;
    font-size: 12px;
}
table p{
	font-size: 14px;
}
table .Form {
    border: 1px solid#000;
}
.Form {
   
    border: 1px solid;
}
   </style>


  
</head>

<body>

 
	
	
			
		 <div class="Form">
				  <center> <img style="text-align: center;" src="https://asecenglish.com/asecapi/public/logo-admin.png"></center>
				   
			   </div>
		<table border="1">
  <tr>
    <th>
                <div class="Form">
				     <h2><strong>1. From ( Collection Address )</strong></h2>
				     <p>M-1, A-40/41 Ansal Building, Commerical Complex, Mukherjee Nagar, New Delhi, Delhi 110009
</p>
				</div>
	</th>
    <th>
         <div class="Form">
				     <p>Name :{{$fullname}}</p>
				     <p>Address:{{$user->address1}} </p>
				    <p><span>City: {{$user->city}}</span>
				     <spans>Postel/Zip Code:{{$user->pincode}}</spans></p>
				     <p> <span>Provincence:</span>
				     <spans>Countyr: Denmark</spans></p>
				     <p> <span>Contact Name: Flemming Petersen</span>
				     <spans>Tel No: 424444000</spans></p>
				     <p><strong>3. Goods</strong></p>
				</div>
			   </th>
    
  </tr>
  
			
  </table>
</br>
</br>
</br>
 <div class="Form">
				  <center> <img style="text-align: center;" src="https://asecenglish.com/asecapi/public/logo-admin.png"></center>
			   </div>
		<table border="1">
  <tr>
    <th>
                <div class="Form">
				     <h2><strong>1. From ( Collection Address )</strong></h2>
				     <p>M-1, A-40/41 Ansal Building, Commerical Complex, Mukherjee Nagar, New Delhi, Delhi 110009
</p>
				</div>
	</th>
    <th>
         <div class="Form">
				     <p>Name :{{$fullname}}</p>
				     <p>Address:{{$user->address1}} </p>
				    <p><span>City: {{$user->city}}</span>
				     <spans>Postel/Zip Code:{{$user->pincode}}</spans></p>
				     <p> <span>Provincence:</span>
				     <spans>Countyr: Denmark</spans></p>
				     <p> <span>Contact Name: Flemming Petersen</span>
				     <spans>Tel No: 424444000</spans></p>
				     <p><strong>3. Goods</strong></p>
				</div>
			   </th>
    
  </tr>
  
			
  </table>

  </br>
  </br>
  </br>
   <div class="Form">
				  <center> <img style="text-align: center;" src="https://asecenglish.com/asecapi/public/logo-admin.png"></center>
			   </div>
		<table border="1">
  <tr>
    <th>
                <div class="Form">
				     <h2><strong>1. From ( Collection Address )</strong></h2>
				     <p>M-1, A-40/41 Ansal Building, Commerical Complex, Mukherjee Nagar, New Delhi, Delhi 110009
</p>
				</div>
	</th>
    <th>
         <div class="Form">
				     <p>Name :{{$fullname}}</p>
				     <p>Address:{{$user->address1}} </p>
				    <p><span>City: {{$user->city}}</span>
				     <spans>Postel/Zip Code:{{$user->pincode}}</spans></p>
				     <p> <span>Provincence:</span>
				     <spans>Countyr: Denmark</spans></p>
				     <p> <span>Contact Name: Flemming Petersen</span>
				     <spans>Tel No: 424444000</spans></p>
				     <p><strong>3. Goods</strong></p>
				</div>
			   </th>
    
  </tr>
  
			
  </table>
  </br>
  </br>
  </br>
   <div class="Form">
				 <center> <img style="text-align: center;" src="https://asecenglish.com/asecapi/public/logo-admin.png"></center>
			   </div>
		<table border="1">
  <tr>
    <th>
                <div class="Form">
				     <h2><strong>1. From ( Collection Address )</strong></h2>
				     <p>M-1, A-40/41 Ansal Building, Commerical Complex, Mukherjee Nagar, New Delhi, Delhi 110009
</p>
				</div>
	</th>
    <th>
         <div class="Form">
				     <p>Name :{{$fullname}}</p>
				     <p>Address:{{$user->address1}} </p>
				    <p><span>City: {{$user->city}}</span>
				     <spans>Postel/Zip Code:{{$user->pincode}}</spans></p>
				     <p> <span>Provincence:</span>
				     <spans>Countyr: Denmark</spans></p>
				     <p> <span>Contact Name: Flemming Petersen</span>
				     <spans>Tel No: 424444000</spans></p>
				     <p><strong>3. Goods</strong></p>
				</div>
			   </th>
    
  </tr>
  
			
  </table>
</body>

</html>