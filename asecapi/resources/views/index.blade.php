<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Register</title>

    <!-- Fontfaces CSS-->
    <link href="{{ asset('/public/css1/font-face.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('/public/vendor1/font-awesome-4.7/css/font-awesome.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('/public/vendor1/font-awesome-5/css/fontawesome-all.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('/public/vendor1/mdi-font/css/material-design-iconic-font.min.css') }}" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="{{ asset('/public/vendor1/bootstrap-4.1/bootstrap.min.css') }}" rel="stylesheet" media="all">

    <!-- vendor1 CSS-->
    <link href="{{ asset('/public/vendor1/animsition/animsition.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('/public/vendor1/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('/public/vendor1/wow/animate.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('/public/vendor1/css-hamburgers/hamburgers.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('/public/vendor1/slick/slick.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('/public/vendor1/select2/select2.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('/public/vendor1/perfect-scrollbar/perfect-scrollbar.css') }}" rel="stylesheet" media="all">

    <!-- Main CSS-->

    <link href="{{ asset('/public/css1/theme.css') }}" rel="stylesheet" media="all">
    <style type="text/css">
  .formerror{
    color: red;
  }
  
  h4 {
    margin-bottom: 30px;
    font-size: 21px;
}
</style>
</head>

<body class="animsition">
    <div class="page-wrapper">
        <div class="page-content--bge5">
            <div class="container">
                <div class="login-wrap">
                    <div class="login-content row">
												<div class="login-logo" style="text-align: center; width: 100%;">
													<a href="#">
														<img src="{{ asset('/public/images/icon/logo.png') }}" alt="monamilogo">
													</a>
												</div>
												

						<!-- <div class="login-form col-md-6 admincred">
													 
						<h4 style="">Login As Admin</h4>
						<div class="centerams">
						<i class="fa fa-user"></i>
							<div class="">
							   <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit">Login As Admin</button>
							</div>
						</div>
                            
                        </div> -->
						
						<div class="login-form col-md-12" style="margin: 30px auto;">
						<h4 style="">Sign Up For New User</h4>
                            <form action="{{ route('signup') }}" method="post" enctype="multipart/form-data">
                               <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                                <div class="form-group">
                                    <label>Nursery Name</label>
                                    <input class="au-input au-input--full" type="text" name="fullname" placeholder="Username" value="<?php if(isset($_POST["fullname"])){ echo $_POST["fullname"]; } ?>">

                                    <span class="formerror"><?php
                                    if(isset($messages['fullname']['0']) && !empty($messages['fullname']['0'])){
                                        echo $messages['fullname']['0'];
                                    }
                                ?></span>
                                </div>
								<div class="form-group">
                                    <label>Address</label>
                                    <input class="au-input au-input--full" type="text" name="address" placeholder="address" value="<?php if(isset($_POST["address"])){ echo $_POST["address"]; } ?>">
                                    <span class="formerror">
                                     <?php
                                    if(isset($messages['address']['0']) && !empty($messages['address']['0'])){
                                        echo $messages['address']['0'];
                                    }
                                ?></span>
                                </div>
								
								<div class="form-group">
                                    <label>Telephone number</label>
                                    <input class="au-input au-input--full" type="text" name="phone" placeholder="phone" value="<?php if(isset($_POST["phone"])){ echo $_POST["phone"]; } ?>">
                                    <span class="formerror">
                                   <?php
                                    if(isset($messages['phone']['0']) && !empty($messages['phone']['0'])){
                                        echo $messages['phone']['0'];
                                    }
                                ?></span>
                                </div>
                                <div class="form-group">
                                    <label>Email Address</label>
                                    <input class="au-input au-input--full" type="email" name="email" placeholder="Email" value="<?php if(isset($_POST["email"])){ echo $_POST["email"]; } ?>">
                                    <span class="formerror">
                                     <?php
                                    if(isset($messages['email']['0']) && !empty($messages['email']['0'])){
                                        echo $messages['email']['0'];
                                    }
                                ?></span>
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input class="au-input au-input--full" type="password" name="password" placeholder="Password">
                                    <span class="formerror">
                                    <?php
                                    if(isset($messages['password']['0']) && !empty($messages['password']['0'])){
                                        echo $messages['password']['0'];
                                    }
                                ?></span>
                                </div>
                                 <div class="form-group">
                                    <label>Confirm Password</label>
                                    <input class="au-input au-input--full" type="password" name="password_confirmation" placeholder="Confirm Password">
                                    <span class="formerror">
                                    <?php
                                    if(isset($messages['password_confirmation']['0']) && !empty($messages['password_confirmation']['0'])){
                                        echo $messages['password_confirmation']['0'];
                                    }
                                ?></span>
                                </div>
								
								<div class="form-group">
                                    <label>Upload Nursery logo (optional)</label>
                                    <input type="file" id="file-input" name="image" class="form-control-file" value="<?php if(isset($_POST["image"])){ echo $_POST["image"]; } ?>">
                                    <span class="formerror">
                                     <?php
                                    if(isset($messages['image']['0']) && !empty($messages['image']['0'])){
                                        echo $messages['image']['0'];
                                    }
                                ?></span>
                                </div>
								
                                <div class="login-checkbox">
                                    <label>
                                        <input type="checkbox" name="aggree" data-toggle="modal" data-target="#mediumModal8" required>Agreement with Terms and Conditions
                                    </label>
                                </div>
                                <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit">register</button>
                               <!--  <div class="social-login-content">
                                    <div class="social-button">
                                        <button class="au-btn au-btn--block au-btn--blue m-b-20">register with facebook</button>
                                        <button class="au-btn au-btn--block au-btn--blue2">register with twitter</button>
                                    </div>
                                </div> -->
                            </form>
                            <div class="register-link">
                                <p>
                                    Already have account?
                                    <a href="{{url('/')}}">Login</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
	
	
	<div class="modal fade" id="mediumModal8" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" style="display: none;" aria-hidden="true">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="mediumModalLabel">Terms and Policy</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">×</span>
							</button>
						</div>
						<div class="modal-body">
							<div class="card">
                                   
                                    <div class="card-body card-block">
                                       
                                     <p>The aim of this use-case is to define the meals to be served to the children during the week. The input shall be done by grades. The meals shall be associated to all the children of the referred grade and shall be visible from each child profile. The meals to be defined are: breakfast, morning snack, lunch and afternoon snack. To be filled in a weekly basis.Yes The meals shall be associated to all the children of the referred grade and shall be visible from each child profile.</p>
									 
									 </p>
                                            
                                            
                                    </div>
                                    
                                </div>
						</div>
						
					</div>
				</div>
			</div>

    <!-- Jquery JS-->
    <script src="{{ asset('/public/vendor1/jquery-3.2.1.min.js') }}"></script>
    <!-- Bootstrap JS-->
    <script src="{{ asset('/public/vendor1/bootstrap-4.1/popper.min.js') }}"></script>
    <script src="{{ asset('/public/vendor1/bootstrap-4.1/bootstrap.min.js') }}"></script>
    <!-- vendor1 JS       -->
    <script src="{{ asset('/public/vendor1/slick/slick.min.js') }}">
    </script>
    <script src="{{ asset('/public/vendor1/wow/wow.min.js') }}"></script>
    <script src="{{ asset('/public/vendor1/animsition/animsition.min.js') }}"></script>
    <script src="{{ asset('/public/vendor1/bootstrap-progressbar/bootstrap-progressbar.min.js') }}">
    </script>
    <script src="{{ asset('/public/vendor1/counter-up/jquery.waypoints.min.js') }}"></script>
    <script src="{{ asset('/public/vendor1/counter-up/jquery.counterup.min.js') }}">
    </script>
    <script src="{{ asset('/public/vendor1/circle-progress/circle-progress.min.js') }}"></script>
    <script src="{{ asset('/public/vendor1/perfect-scrollbar/perfect-scrollbar.js') }}"></script>
    <script src="{{ asset('/public/vendor1/chartjs/Chart.bundle.min.js') }}"></script>
    <script src="{{ asset('/public/vendor1/select2/select2.min.js') }}">
    </script>

    <!-- Main JS-->
    <script src="{{ asset('/public/js1/main.js') }}"></script>

</body>

</html>
<!-- end document--> 