    @extends('layouts.admins') 
@section('title', 'Grade List')
@section('content')
    <div class="main-content">
	<style>
	.innercrc a{
		margin:auto;
		padding:40%;
		z-index:999999;
		color:#484848;
	}
	.table-data3 tbody tr td:last-child{text-align:left;}
	
	.shownotbg {
    opacity: 0;
    visibility: hidden;
    position: absolute;
	    -webkit-transition: all 1s ease;
    -o-transition: all 0.3s ease;
    -moz-transition: all 0.3s ease;
    transition: all 1s ease;
	    top: 3px;
    z-index: 9999999;
    width: 180px;
    border-radius: 100%;
    height: 180px;
    left: 2px;
}
.shownotbg.showinrop {
    opacity: 1;
    visibility: visible;
    top: 3px;
    z-index: 9999999;
    background: #ddd;
    width: 180px;
    border-radius: 100%;
    height: 180px;
    left: 2px;
}

.gradecricle ul li .shownotbg ul li{
	width: auto;
    float: left;
    text-align: left;
	    margin: 0;
		
}
.gradecricle ul li .shownotbg ul{    position: absolute; top: 70px; left: 15px;}
.gradecricle ul li .shownotbg ul li a {
    padding: 0;
	font-weight: 500;
} 

span.endask {
    font-weight: 600;
    font-size: 20px;
    color: #de1412;
    cursor: pointer;
}
	</style>
                    <div class="section__content section__content--p30">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="overview-wrap">
                                        <h2 class="title-1">Define Grades</h2>
										<a style="float:right;margin-bottom:10px;" class="au-btn au-btn-icon au-btn--blue" data-toggle="modal" data-target="#mediumModal9">
                                 <i class="zmdi zmdi-plus"></i> Add Grade                                  
                                </a>
                                        <!-- <button class="au-btn au-btn-icon au-btn--blue">
                                            <i class="zmdi zmdi-plus"></i>Add New Grade</button> -->
                                              @if(session()->has('message'))
		    <div class="alert alert-success">
		        {{ session()->get('message') }}
		    </div>
		@endif
                                    </div>
                                </div>
                                
                                <div class="col-md-12">
                                    <div class="gradecricle tabdatabga">
                                    <ul>
                                    <?php

                                      if(!empty($users)){

                                        foreach ($users as $key => $value) {
                                            
                                    ?>
                                        <li id="gradebtns{{$value['id']}}"><div class="innercrc"><p class="gradebtns" >{{$value['name']}}</p> 
										<div class="shownotbg" id="shownotbg{{$value['id']}}">
											<ul>
												<li><a href="{{url('/')}}/classroom_list?grade_id={{$value['id']}}"> Classroom Lists</a></li>
												<li><a href="{{url('/')}}/report_list?grade_id={{$value['id']}}"> Reports</a></li>
												<span aria-hidden="true" class="endask" id="endask{{$value['id']}}">×</span>
											</ul>
											
										</div>
										 
										</div>

                                        <script>
                                                $("#gradebtns{{$value['id']}}").click(function (e) {
                                    e.stopPropagation();
                                    $("#shownotbg{{$value['id']}}").toggleClass('showinrop');
                                }); 

                                $("#endask{{$value['id']}}").click(function (e) {
                                    e.stopPropagation();
                                    $("#shownotbg{{$value['id']}}").removeClass('showinrop');
                                });
                                </script>
										
										</li>
                                       <?php
                                            }
                                       }else { ?>
                                      <p style="color:red;">Please create a grade</p>
<?php }

                                       ?>           
                                        
                                    </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                             
                    </div>
                </div>
				
				
               
                @endsection