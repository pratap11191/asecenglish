@extends('layouts.admins') 
@section('title', 'Children List')
@section('content')
<style type="text/css">
    .formerror{
    color: red;
    }
</style>
<div class="row"></div>
<div class="col-md-12">
    <!-- BORDERED TABLE -->
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">
                Children Management List
            </h3>
            @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
            @endif
            <?php  if(!empty($users1)){ ?>
            <a style="float:right;margin-bottom:10px;" class="au-btn au-btn-icon au-btn--blue" data-toggle="modal" data-target="#mediumModal">
            <i class="zmdi zmdi-plus"></i> Add Children                                  
            </a>
            <?php } else { ?>
            <a style="float:right;margin-bottom:10px;" class="au-btn au-btn-icon au-btn--blue" href="{{url('/')}}/classroom_list">
            <i class="zmdi zmdi-plus"></i> Add Children                                  
            </a>
            <?php
                } ?>
        </div>
        <div class="panel-body">
            <div class="table-responsive m-b-40">
                <table class="table table-borderless table-data3" id="data">
                    <thead>
                        <tr>
                            <th>
                                #
                            </th>
                            <th>
                                CUID
                            </th>
                            <th>SCUID</th>
                            <th>
                                First Name
                            </th>
                            <th>
                                Last Name
                            </th>
                            <th>
                                Age
                            </th>
                            <th>
                                Father
                            </th>
                            <th>
                                Parent Email
                            </th>
                            <th>
                                Parents Phone Number
                            </th>
                            <th>
                                Action 
                            </th>
                            <th>
                                Status
                            </th>
                        </tr>
                    </thead>
                    @if(!empty($users))
                    <tbody>
                        <?php $i=0;?>
                        @foreach($users as $user)
                        <tr>
                            <td>
                                {{ ++$i }}
                            </td>
                            <td>
                                {{$user->CUID}}
                            </td>
                              <td>
                                {{$user->SCUID}}
                            </td>
                            <td>
                                {{$user->name}}
                            </td>
                            <td>
                                {{$user->surname}}
                            </td>
                            <td>
                                {{$user->age}}
                            </td>
                            <td>
                                {{$user->father}}
                            </td>
                            <td>
                                {{$user->email}}
                            </td>
                            <td>
                                {{$user->phone}}
                            </td>
                            <td>
                                <div class="table-data-feature"> 
                                    <button class="item" data-toggle="modal" data-target="#mediumModal{{$user->id}}" data-placement="top" title="" data-original-title="Edit">
                                    <i class="zmdi zmdi-edit"></i>
                                    </button>
                                    <a class="action_an" href="{{url('common_delete')}}/{{$user->id}}/childrens" > 
                                    <button class="item" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete">
                                    <i class="zmdi zmdi-delete"></i>
                                    </button>
                                    </a>
                                </div>
                            </td>
                            <td>
                                @if($user->admin_status == 0)
                                <a class="action_an btn btn-danger" href="{{url('change_statusChildren')}}/{{$user->id}}" >
                                <span class="dlt_icon">
                                <?php echo "Inactive"; ?>
                                </span>
                                </a>
                                @else
                                <a class="action_an btn btn-success" href="{{url('change_statusChildren')}}/{{$user->id}}" >
                                <span class="dlt_icon">
                                <?php echo "Active" ?>
                                </span>
                                </a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    @endif
                </table>
            </div>
        </div>
    </div>
    <!-- END BORDERED TABLE -->
</div>
</div>
<?php
    if(!empty($users)){
    
        foreach ($users as $user) {
            
        
    
    ?>
<div class="modal fade" id="mediumModal{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel{{$user->id}}" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div id="resultpass{{$user->id}}"></div>
            <div class="modal-header">
                <h5 class="modal-title" id="mediumModalLabel{{$user->id}}">Add Children</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="card-body card-block">
                        <form  action="{{url('/')}}/editchildes1" method="post" enctype="multipart/form-data" class="form-horizontal">
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="text-input" class=" form-control-label">First Name </label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="text" id="name{{$user->id}}" name="name" placeholder="Name" class="form-control" value="{{$user->name}}">
                                    <span class="formerror ername{{$user->id}}"></span>
                                </div>
                            </div>
                            <input type="hidden" name="id" value="{{$user->id}}">
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="text-input" class=" form-control-label">Last Name</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="text" id="surename{{$user->id}}" name="surname" placeholder="Last Name" class="form-control" value="{{$user->surname}}">
                                    <span class="formerror ersurename{{$user->id}}"></span>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="text-input" class=" form-control-label">Age (birth date)</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="text" id="age{{$user->id}}" name="age" placeholder="Age" class="form-control" value="{{$user->age}}">
                                    <span class="formerror erage{{$user->id}}"></span>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="text-input" class=" form-control-label">Profile photo (optional)</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="file" id="image{{$user->id}}" name="image" class="form-control-file">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="text-input" class=" form-control-label">Father Name</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="text" id="father{{$user->id}}" name="father" placeholder="Father" class="form-control" value="{{$user->father}}">
                                    <span class="formerror erfather{{$user->id}}"></span>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="text-input" class=" form-control-label">Mother Name </label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="text" id="mother{{$user->id}}" name="mother" placeholder="Mother" class="form-control" value="{{$user->mother}}">
                                    <span class="formerror ermother{{$user->id}}"></span>
                                </div>
                            </div>
                            <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="text-input" class=" form-control-label">Email</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="email" id="email{{$user->id}}" name="email" placeholder="Email" class="form-control" value="{{$user->email}}">
                                    <span class="formerror eremail{{$user->id}}"></span>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="text-input" class=" form-control-label">Phone</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="text" id="phone{{$user->id}}" name="phone" placeholder="Phone" class="form-control" value="{{$user->phone}}">
                                    <span class="formerror erphone{{$user->id}}"></span>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="select" class=" form-control-label">Classroom</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <select name="classroom_id" id="classroom_id{{$user->id}}" class="form-control">
                                        <option value="0" >Please Select a Classroom</option>
                                        <?php
                                            if(!empty($users1)){
                                            
                                                foreach ($users1 as $users12) {
                                                    
                                            
                                            ?>
                                        <option value="{{$users12['id']}}" <?php if($user->classroom_id == $users12['id']){ echo "selected"; } ?>>{{$users12['name']}}  {{$users12['number']}}</option>
                                        <?php
                                            }
                                            
                                            }
                                            
                                            ?>
                                    </select>
                                    <span class="formerror erclassroom_id{{$user->id}}"></span>
                                </div>
                            </div>
                    </div>
                    <div class="card-footer">
                    <button id="getpasschange{{$user->id}}" data="{{$user->id}}" type="button" class="btn btn-primary btn-sm commonchildedit">
                    <i class="fa fa-dot-circle-o"></i> Submit
                    </button>
                    <button type="reset" class="btn btn-danger btn-sm">
                    <i class="fa fa-ban"></i> Reset
                    </button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
    }
    
    }
    ?>
<div class="modal fade" id="mediumModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div id="resultpass"></div>
            <div class="modal-header">
                <h5 class="modal-title" id="mediumModalLabel">Add Children</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="card-body card-block">
                        <form  action="{{url('/')}}/addchildes1" method="post" enctype="multipart/form-data" class="form-horizontal">
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="text-input" class=" form-control-label">First Name </label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="text" id="name" name="name" placeholder="Name" class="form-control">
                                    <span class="formerror ername"></span>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="text-input" class=" form-control-label">Last Name</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="text" id="surename" name="surname" placeholder="Last Name" class="form-control">
                                    <span class="formerror ersurename"></span>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="text-input" class=" form-control-label">Age (birth date)</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="text" id="age" name="age" placeholder="Age" class="form-control">
                                    <span class="formerror erage"></span>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="text-input" class=" form-control-label">Profile photo (optional)</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="file" id="image" name="image" class="form-control-file">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="text-input" class=" form-control-label">Father Name</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="text" id="father" name="father" placeholder="Father" class="form-control">
                                    <span class="formerror erfather"></span>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="text-input" class=" form-control-label">Mother Name </label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="text" id="mother" name="mother" placeholder="Mother" class="form-control">
                                    <span class="formerror ermother"></span>
                                </div>
                            </div>
                            <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="text-input" class=" form-control-label">Email</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="email" id="email" name="email" placeholder="Email" class="form-control">
                                    <span class="formerror eremail"></span>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="text-input" class=" form-control-label">Phone</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="text" id="phone" name="phone" placeholder="Phone" class="form-control">
                                    <span class="formerror erphone"></span>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="select" class=" form-control-label">Classroom</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <select name="classroom_id" id="classroom_id" class="form-control">
                                        <option value="0" >Please Select a Classroom</option>
                                        <?php
                                            if(!empty($users1)){
                                            
                                                foreach ($users1 as $users12) {
                                                    
                                            
                                            ?>
                                        <option value="{{$users12['id']}}">{{$users12['name']}}  {{$users12['number']}}</option>
                                        <?php
                                            }
                                            
                                            }
                                            
                                            ?>
                                    </select>
                                    <span class="formerror erclassroom_id"></span>
                                </div>
                            </div>
                    </div>
                    <div class="card-footer">
                    <button id="getpasschange" type="button" class="btn btn-primary btn-sm">
                    <i class="fa fa-dot-circle-o"></i> Submit
                    </button>
                    <button type="reset" class="btn btn-danger btn-sm">
                    <i class="fa fa-ban"></i> Reset
                    </button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="deletesucchealthapp">
    <div class="modal fade" id="myModal4" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="succhealth">
                        <i class="fa fa-check"></i>
                    </div>
                    <div class="setbtnoks">
                        <h1>Success!</h1>
                        <p> Classroom created successfully!</p>
                        <button class="close allokbtns datadis12" >Ok</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="deletesucchealthapp">
    <div class="modal fade" id="myModal6" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="succhealth">
                        <i class="fa fa-check"></i>
                    </div>
                    <div class="setbtnoks">
                        <h1>Success!</h1>
                        <p> Classroom deleted successfully!</p>
                        <button class="close allokbtns datadis12" >Ok</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="deletesucchealthapp">
    <div class="modal fade" id="myModal5" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="succhealth">
                        <i class="fa fa-check"></i>
                    </div>
                    <div class="setbtnoks">
                        <h1>Success!</h1>
                        <p> Classroom updated successfully!</p>
                        <button class="close allokbtns datadis12" >Ok</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(".commonchildedit").click(function(){
    
        var nodechildflag = $(this).attr("data");
    
    if($('#name'+nodechildflag).val() == ''){
                $('.ername'+nodechildflag).text('Please Enter Child Name');
            }
            
    if($('#surename'+nodechildflag).val() == ''){
                $('.ersurename'+nodechildflag).text('Please Enter Child Surname');
            } 
    
    if($('#father'+nodechildflag).val() == ''){
                $('.erfather'+nodechildflag).text('Please Enter Father Name');
            }
    if($('#mother'+nodechildflag).val() == ''){
                $('.ermother'+nodechildflag).text('Please Enter Mother Name');
            }
            
    if($('#email'+nodechildflag).val() == ''){
                $('.eremail'+nodechildflag).text('Please Enter Email');
            } 
            
    if($('#phone'+nodechildflag).val() == ''){
                $('.erphone'+nodechildflag).text('Please Enter Phone Number');
            }
    if($('#age'+nodechildflag).val() == ''){
                $('.erage'+nodechildflag).text('Please enter Child Age');
            }
    if($('#classroom_id'+nodechildflag).val() == '0'){
                $('.erclassroom_id'+nodechildflag).text('Please Select a Classroom');
    }                
                                                 
           
    
            if($('#name'+nodechildflag).val() != '' && $('#email'+nodechildflag).val() != '' && $('#father'+nodechildflag).val() != '' && $('#mother'+nodechildflag).val() != '' && $('#email'+nodechildflag).val() != '' && $('#phone'+nodechildflag).val() != ''  && $('#age'+nodechildflag).val() != '' && $('#surename'+nodechildflag).val() != '' && $('#classroom_id'+nodechildflag).val() != '0'){
                    //
                    //alert("bdsfdsfgjsdgfjsdgfjgdsvjkfvdsjgfvgsjdfg");
                    $('#getpasschange'+nodechildflag).attr('type','submit');
                    $('#getpasschange'+nodechildflag).click();
            }
    
    });
</script>
<script type="text/javascript">
    $("#getpasschange").click(function(){
    
         if($('#name').val() == ''){
                             $('.ername').text('Please Enter Child Name');
                         }
                         
         if($('#surename').val() == ''){
                             $('.ersurename').text('Please Enter Child Surname');
                         } 
    
          if($('#father').val() == ''){
                             $('.erfather').text('Please Enter Father Name');
                         }
         if($('#mother').val() == ''){
                             $('.ermother').text('Please Enter Mother Name');
                         }
                         
         if($('#email').val() == ''){
                             $('.eremail').text('Please Enter Email');
                         } 
                         
          if($('#phone').val() == ''){
                             $('.erphone').text('Please Enter Phone Number');
                         }
         if($('#age').val() == ''){
                             $('.erage').text('Please enter Child Age');
                         }
         if($('#classroom_id').val() == '0'){
                             $('.erclassroom_id').text('Please Select a Classroom');
         }                
                                                              
                        
    
                         if($('#name').val() != '' && $('#email').val() != '' && $('#father').val() != '' && $('#mother').val() != '' && $('#email').val() != '' && $('#phone').val() != ''  && $('#age').val() != '' && $('#surename').val() != '' && $('#classroom_id').val() != '0'){
                                 //
                                 //alert("bdsfdsfgjsdgfjsdgfjgdsvjkfvdsjgfvgsjdfg");
                                 $('#getpasschange').attr('type','submit');
                                 $('#getpasschange').click();
                         }
    
    });
    
    
    
     
      $(".datadis12").click(function(){
         location.reload();
     });
    
    
       $(".gettingp").click(function(){
         var name = $("#name"+$(this).attr('id')).val();
    
     var number = $("#number"+$(this).attr('id')).val();
    
      var number_of_nannies = $("#number_of_nannies"+$(this).attr('id')).val();
    
     var number_of_children = $("#number_of_children"+$(this).attr('id')).val();
    
     var grade_id = $("#grade_id"+$(this).attr('id')).val();
    
     var _token = $("#csrf-token").val();
    
     $.post("{{url('/')}}/editchildes",{name:name,number:number,number_of_nannies:number_of_nannies,number_of_children:number_of_children,_token:_token,grade_id:grade_id,id:$(this).attr('id')},function(result){
             var data = JSON.parse(result);
    
             if(data[0] == 1){
                     $('#mediumModal'+data[2]).modal('hide'); 
                     $('#myModal5').modal('show'); 
             } else {
                 $("#resultpass"+data[2]).html('<div class="alert alert-danger">'+data[1]+'</div>');
             }
             //alert(data);
     });
    });
    
     $(".delclassroom").click(function(){
             $.post("{{url('/')}}/deletechild",{id:$(this).attr('id')},function(result){
             var data = JSON.parse(result);
    
             if(data[0] == 1){
                     
                     $('#myModal6').modal('show'); 
             }
             //alert(data);
     });
     });  
    
</script>
@endsection