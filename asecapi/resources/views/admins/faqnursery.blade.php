@extends('layouts.admins') 
@section('title', 'Support')
@section('content')

<style type="text/css">
  .formerror{
    color: red;
  }
</style>
<div id="askforsup" class="tabcontent" >
        <div class="main-content">
            <div class="section__content section__content--p30">
              <div class="container-fluid">
              @if(session()->has('message'))
		    <div class="alert alert-success">
		        {{ session()->get('message') }}
		    </div>
		@endif
              <div class="overview-wrap">
                    <h2 class="title-1">Ask for support</h2>
                    
                  </div>
              <div class="col-md-12 tabdatabga">
              
                  <div id="accordion">
                      <div class="card">
                      <div class="card-header" id="headingOne">
                        <h5 class="mb-0">
                        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                          What is the aim of this use-case is to define the meals to be served ?
                          <i class="fas fa-plus"></i>
                          <i class="fas fa-minus"></i>
                        </button>
                        </h5>
                      </div>

                      <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">
                        The aim of this use-case is to define the meals to be served to the children during the week. The input shall be done by grades. The
                        meals shall be associated to all the children of the referred grade and shall be visible from each child profile. The meals to be defined
                        are: breakfast, morning snack, lunch and afternoon snack. To be
                        filled in a weekly basis.
                        </div>
                      </div>
                      </div>
                      <div class="card">
                      <div class="card-header" id="headingTwo">
                        <h5 class="mb-0">
                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                          Is The meals shall be associated to all the children of the referred grade ?
                           <i class="fas fa-plus"></i>
                          <i class="fas fa-minus"></i>
                        </button>
                        </h5>
                      </div>
                      <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-body">
                         Yes The meals shall be associated to all the children of the referred grade and shall be visible from each child profile. 
                        </div>
                      </div>
                      </div>
                      <div class="card">
                      <div class="card-header" id="headingThree">
                        <h5 class="mb-0">
                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                          What Are The Meals to be Defined ?
                           <i class="fas fa-plus"></i>
                          <i class="fas fa-minus"></i>
                        </button>
                        </h5>
                      </div>
                      <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                        <div class="card-body">
                        The meals to be defined are: breakfast, morning snack, lunch and afternoon snack. To be filled in a weekly basis.
                        </div>
                      </div>
                      </div>
                  </div>
                
                <div class="supportadmin">
                  <h4> Have Any Questions ? Let us know !</h4>
                  <div class="card">
                   <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
                   <div class="form-group">
                                               
                                                <div class="col-12 col-md-9">
                        
                        <label for="textarea-input" class=" form-control-label"><strong>Write Your Query Here</strong></label>
                                                    <textarea name="msg" id="msg1" rows="9" placeholder="Content..." class="form-control"></textarea>
                                                     <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                                                    <p id="errormsg" style="color: red;"></p>
                          <button id="submitquery" type="button" class="btn btn-primary">Submit Query</button>
                                                </div>
                        
                         <div class="col col-md-3">
                                                    
                                                </div>
                        
                                            </div>
                   </form>
                  </div>
                </div>
                </div>
              </div>
            </div>
        </div>
      </div>
<script type="text/javascript">
  
  $("#submitquery").click(function(){
        if($("#msg1").val() == ''){
            $("#errormsg").text("Please write something meaningful to submit query");
        } else {
          $("#submitquery").attr("type","submit");
          $("#submitquery").click();
        }
  });
</script>
@endsection