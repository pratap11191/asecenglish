@extends('layouts.admins') 
@section('title', 'Edit Nursury')
@section('content')

<style type="text/css">
  .formerror{
    color: red;
  }
</style>
<div class="row">
</div>
    <div class="col-md-12">
        <!-- BORDERED TABLE -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Edit Nursery
                </h3>
              
            </div>
            <div class="panel-body">
      
      <div class="container" style="width: 70%">
  
<form action="{{url('/')}}/admin/editnursury/{{$id}}" method="post">
  <fieldset>
    <legend>Nany Info</legend>
   
 <div class="form-group">
      <label for="exampleInputEmail1">Full Name</label>
      <input type="text" class="form-control" name="fullname" id="exampleInputname" aria-describedby="emailHelp" placeholder="Enter Full name" value="<?php if(isset($userdeatils["fullname"]) && !empty($userdeatils["fullname"])){ echo $userdeatils["fullname"]; } ?>">
      <span class="formerror"><?php
                                    if(isset($messages['fullname']['0']) && !empty($messages['fullname']['0'])){
                                        echo $messages['fullname']['0'];
                                    }
                                ?></span>
    </div>


    
    <div class="form-group">
      <label for="exampleInputEmail1">Email address</label>
      <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" value="<?php if(isset($userdeatils["email"]) && !empty($userdeatils["email"])){ echo $userdeatils["email"]; } ?>">
       <span class="formerror"><?php
                                    if(isset($messages['email']['0']) && !empty($messages['email']['0'])){
                                        echo $messages['email']['0'];
                                    }
                                ?></span>
      <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
    </div>

    
    
    <div class="form-group">
      <label for="exampleInputPassword1">Password</label>
      <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password" value="<?php if(isset($userdeatils["password"]) && !empty($userdeatils["password"])){ echo $userdeatils["password"]; } ?>">
      <span class="formerror"><?php
                                    if(isset($messages['password']['0']) && !empty($messages['password']['0'])){
                                        echo $messages['password']['0'];
                                    }
                                ?></span>
    </div>

     

    <div class="form-group">
      <label for="exampleInputEmail1">Phone Number</label>
      <input type="number" class="form-control" name="phone" id="exampleInputphone" aria-describedby="emailHelp" placeholder="Enter Phone Number" value="<?php if(isset($userdeatils["phone"]) && !empty($userdeatils["phone"])){ echo $userdeatils["phone"]; } ?>" maxlength="10">
      <span class="formerror"><?php
                                    if(isset($messages['phone']['0']) && !empty($messages['phone']['0'])){
                                        echo $messages['phone']['0'];
                                    }
                                ?></span>
    </div>
    <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />

    <button type="submit" class="btn btn-primary">Submit</button>
   <a href="{{url('/')}}/admin/nursury_list" class="btn btn-primary">Back</a>
  </fieldset>
</form>

</div>
               
            </div>
        </div>
       
        <!-- END BORDERED TABLE -->
    </div>
</div>

@endsection