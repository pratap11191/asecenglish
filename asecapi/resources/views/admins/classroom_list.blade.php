@extends('layouts.admins') 
@section('title', 'Classrooms List')
@section('content')

<div class="row">
</div>

<style>
input.myfilecsv {
    opacity: 0;
    z-index: 999999;
    float: left;
    margin: 0 0 0 0px;
    padding: 1%;
    position: absolute;
    width: 49px;
    cursor: pointer;
    left: 17px;
}

.pdftype {
    position: relative;
    top: 0;
    right: 0;
    z-index: 0;
    cursor: pointer;
    font-size: 30px;
}

.csvupdate {
    float: left;
    position: relative;
    width: auto;
    z-index: 999999;
    text-align: center;
	    color: #000;
}

.addlischd {
    padding: 7px 11px;
    font-size: 12px;
    line-height: 14px;
}

#mediumModal828 .table-data3 tbody td{    padding: 11px 30px;}
</style>
    <div class="col-md-12">
        <!-- BORDERED TABLE -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Classrooms Management List
                </h3>


                    <?php  if(!empty($users1)){ ?>
                
                                             <a style="float:right;margin-bottom:10px;" class="au-btn au-btn-icon au-btn--blue" data-toggle="modal" data-target="#mediumModal">
                                 <i class="zmdi zmdi-plus"></i> Add Classrooms                                  
                                </a>
                                <?php } else { ?>
                                    <a style="float:right;margin-bottom:10px;" class="au-btn au-btn-icon au-btn--blue" href="{{url('/')}}/grades_list">
                                 <i class="zmdi zmdi-plus"></i> Add Classrooms                                  
                                </a>

                                <?php

                                } ?>
            </div>
            <div class="panel-body">
            
            
                                        
        <div class="table-responsive m-b-40">
               <table class="table table-borderless table-data3" id="data">
                  <thead>
                        <tr>
                            <th>
                                #
                            </th>
                            <th>
                                Classroom Name
                            </th>
                            <th>
                                Classroom Number
                            </th>
                            <th>
                               Number Of Children
                            </th>
                           
                           
                            <th>
                                Number Of Nannies
                            </th>
                            <th>
                             Action 
                             </th>
                            
                        </tr>
                    </thead>
                    @if(!empty($users))
                    <tbody>
                        <?php $i=0;?>
                        @foreach($users as $user)
                        <tr>
                            <td>
                                {{ ++$i }}
                            </td>
                            
                                 
                              
                                <td >
                                 <a href="{{url('/')}}/gradedetails/{{$user->id}}" >{{$user->name}}</a>
                                 </td>
                              
                           
                            <td>
                                {{$user->number}}
                            </td>
                            <td>
                                {{$user->number_of_children}}
                            </td>
                              <td>
                                {{$user->number_of_nannies}}
                            </td>
                            
                             <td>
                             <div class="table-data-feature"> 
                             
                                 <button class="item" data-toggle="modal" data-target="#mediumModal{{$user->id}}" data-placement="top" title="" data-original-title="Edit" id="oknewway{{$user->id}}">
                                    <i class="zmdi zmdi-edit"></i>
                                 </button>
                            
                                                        
                             <a class="action_an" href="{{url('common_delete')}}/{{$user->id}}/classrooms" > 
                                 <button class="item" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete">
                                    <i class="zmdi zmdi-delete"></i>
                                 </button>
                             </a>
                            </div>
                             </td>
                            
                        </tr>

                        <script type="text/javascript">
                            $("#oknewway{{$user->id}}").click(function(){
                                 $("#mediumModalLabel122018{{$user->id}}").text("Edit");
                            });
                        </script>
                        
                        @endforeach
                    </tbody>
                    @endif
                </table>
        </div>
            </div>
        </div>
       
        <!-- END BORDERED TABLE -->
    </div>
</div>

<?php

if(!empty($users)){

    foreach ($users as $user) {
        
    

?>

<div class="modal fade" id="mediumModal{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel{{$user->id}}" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="mediumModalLabel122018{{$user->id}}">Edit</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="card">
                                   <div id="resultpass{{$user->id}}"></div>
                                    <div class="card-body card-block">
                                       <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="text-input" class=" form-control-label">Classroom Name</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <input type="text" id="name{{$user->id}}" name="text-input" placeholder="Classroom Name" class="form-control" value="{{$user->name}}">
                                                    
                                                </div>
                                            </div>
                                            
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="select" class=" form-control-label">Classroom Number</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                     <input type="number" id="number{{$user->id}}" name="number" placeholder="Classroom Number" value="{{$user->number}}" class="form-control">
                                                   
                                                </div>
                                            </div>
                                            
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="text-input" class=" form-control-label">Number of Children</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <input type="number" id="number_of_children{{$user->id}}" name="number_of_children" placeholder="Number of Children" value="{{$user->number_of_children}}" class="form-control">
                                                    
                                                </div>
                                            </div>
                                             <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="text-input" class=" form-control-label">Number of Nannies</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <input type="number" id="number_of_nannies{{$user->id}}" name="number_of_nannies" placeholder="Number of Nannies" value="{{$user->number_of_nannies}}" class="form-control">
                                                    
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="text-input" class=" form-control-label">Select Grade</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                   <select name="grade_id" id="grade_id{{$user->id}}" class="form-control">
                                                         <option value="0" >Please Select a Grade</option>
                                                        <?php

                                                            if(!empty($users1)){ 


                                                                    foreach ($users1 as $key => $value) {
                                                                       
                                                                ?>

                                                                <option value="{{$value['id']}}" <?php if($user->grade_id == $value['id']){ echo "selected";} ?>>{{$value['name']}}</option>

                                                            <?php

                                                        }

                                                            }

                                                        ?>
                                                        

                                                    </select>
                                                    
                                                </div>
                                            </div>
                                      

                                            
                                            
                                    </div>
                                    <div class="card-footer">
                                        <button id="{{$user->id}}" type="button" class="btn btn-primary btn-sm gettingp">
                                            <i class="fa fa-dot-circle-o"></i> Submit
                                        </button>
                                        <button type="reset" class="btn btn-danger btn-sm">
                                            <i class="fa fa-ban"></i> Reset
                                        </button>
                                    </div>
                                      </form>
                                </div>
                        </div>
                        
                    </div>
                </div>
            </div>

<?php

}

}
?>
<div class="modal fade" id="mediumModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="mediumModalLabel">Add Classess</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="card">
                                   <div id="resultpass"></div>
                                    <div class="card-body card-block">
                                       <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="text-input" class=" form-control-label">Classroom Name</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <input type="text" id="name" name="text-input" placeholder="Classroom Name" class="form-control">
                                                    
                                                </div>
                                            </div>
                                            
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="select" class=" form-control-label">Classroom Number</label>
                                                </div>
                                              <div class="col-12 col-md-9">
                                                     <input type="number" id="number" name="number" placeholder="Classroom Number" value="" class="form-control">
                                                   
                                                </div>
                                            </div>
                                            
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="text-input" class=" form-control-label">Number of Children</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <input type="number" id="number_of_children" name="number_of_children" placeholder="Number of Children" class="form-control">
                                                    
                                                </div>
                                            </div>
                                             <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="text-input" class=" form-control-label">Number of Nannies</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <input type="number" id="number_of_nannies" name="number_of_nannies" placeholder="Number of Nannies" class="form-control">
                                                    
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="text-input" class=" form-control-label">Select Grade</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                   <select name="grade_id" id="grade_id" class="form-control">
                                                         <option value="0" >Please Select a Grade</option>
                                                        <?php

                                                            if(!empty($users1)){ 


                                                                    foreach ($users1 as $key => $value) {
                                                                       
                                                                ?>

                                                                <option value="{{$value['id']}}">{{$value['name']}}</option>

                                                            <?php

                                                        }

                                                            }

                                                        ?>
                                                        

                                                    </select>
                                                    
                                                </div>
                                            </div>
                                      

                                            
                                            
                                    </div>
                                    <div class="card-footer">
                                        <button id="getpasschange" type="button" class="btn btn-primary btn-sm">
                                            <i class="fa fa-dot-circle-o"></i> Submit
                                        </button>
                                        <button type="reset" class="btn btn-danger btn-sm">
                                            <i class="fa fa-ban"></i> Reset
                                        </button>
                                    </div>
                                      </form>
                                </div>
                        </div>
                        
                    </div>
                </div>
            </div>
 <div class="deletesucchealthapp">
  <div class="modal fade" id="myModal4" style="display: none;">
        <div class="modal-dialog">
          <div class="modal-content">
          <div class="modal-body">
          <div class="succhealth">
           <i class="fa fa-check"></i>
           </div>
          
         
         
          <div class="setbtnoks">
           <h1>Success!</h1>
          <p> Classroom created successfully!</p>
           <button class="close allokbtns datadis12" >Ok</button>
          </div>
          </div>

          </div>
          </div>
          </div>
   </div>

   <div class="deletesucchealthapp">
  <div class="modal fade" id="myModal6" style="display: none;">
        <div class="modal-dialog">
          <div class="modal-content">
          <div class="modal-body">
          <div class="succhealth">
           <i class="fa fa-check"></i>
           </div>
          
         
         
          <div class="setbtnoks">
           <h1>Success!</h1>
          <p> Classroom deleted successfully!</p>
           <button class="close allokbtns datadis12" >Ok</button>
          </div>
          </div>

          </div>
          </div>
          </div>
   </div>

   <div class="deletesucchealthapp">
  <div class="modal fade" id="myModal5" style="display: none;">
        <div class="modal-dialog">
          <div class="modal-content">
          <div class="modal-body">
          <div class="succhealth">
           <i class="fa fa-check"></i>
           </div>
          
         
         
          <div class="setbtnoks">
           <h1>Success!</h1>
          <p> Classroom updated successfully!</p>
           <button class="close allokbtns datadis12" >Ok</button>
          </div>
          </div>

          </div>
          </div>
          </div>
   </div>

   <?php

    $conectionbyphp = mysqli_connect("localhost","mob_db","ram123456","mob_db");

     if(!empty($users)){

        foreach ($users as $userv) {

            $listofchilddren = mysqli_query($conectionbyphp,"SELECT * FROM `childrens` WHERE `classroom_id`='".$userv->id."'");

             $listofchilddren1 = mysqli_query($conectionbyphp,"SELECT * FROM `nannies` WHERE `classroom_id`='".$userv->id."'");
          
   ?>

<div class="modal fade" id="mediumModal8{{$userv->id}}" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">                 
                    <div class="modal-content"> 
                        <div class="modal-header">  
                        <h5 class="modal-title" id="mediumModalLabel">Classroom {{ucfirst($userv->name)}} List of Children</h5>                            
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>                       
                        </div>
                        <div class="modal-body">
                        <div class="card">
                                <div id="resultpass"></div>
                                <div class="card-body card-block">
                                     <div class="table-responsive m-b-40">
               <table class="table table-borderless table-data3 col-md-6" id="data">
                  <thead>
                        <tr>
                            
                            
                            <th>
                               No of Children
                            </th>
                           
                           
                            <th>
                              Name
                            </th>
							
							 <th>
                               CUID
                            </th>
                            
                             <th>
                               Parent Email
                            </th>
                        </tr>
                    </thead>
                      

                         <tr><td>
						 {{$userv->number_of_children}} </td>
<td><?php if($userv->number_of_children == 0){ ?>
                           <button class="item au-btn au-btn-icon au-btn--blue addlischd" data-toggle="modal" data-target="#mediumModal{{$userv->id}}" data-placement="top" title="" data-original-title="Edit" id="okchild2018{{$userv->id}}">
                                    <i class="zmdi zmdi-plus"></i> Add Children
                                 </button>
								
                                 <?php }?></td>
                               <td>   
							   <button class="item au-btn au-btn-icon au-btn--blue addlischd"title="" data-original-title="Edit" >
                                   Submit
                                 </button>
							   
							   <div class="csvupdate">
								 <input type="file" class="myfilecsv"> 
								 
								 <i class="far fa-file pdftype"></i>
								 <p>upload List</p>
								 
								 </div>
								 </td>
                        </tr>
                      
                    </tbody>   
                  
                </table>


                 <table class="table table-borderless table-data3 col-md-6" id="data">
                  <thead>
                        <tr>
                            
                            
                          
                            <th>
                               No of Nannies
                            </th>
                           
                           
                            
                            
                        </tr>
                    </thead>
                   
                     
                         <tr>
                         <td>  {{$userv->number_of_nannies}} </td>
                       
                        </tr>
                        
                     
                    </tbody>
                  
                </table>
        </div>
                                </div>
                                

                            </div>
                        </div>
                    </div>              
                    </div>          
               </div>

               <script type="text/javascript">
                   $("#okchild2018{{$userv->id}}").click(function(){
                        $('#mediumModal8{{$userv->id}}').modal('hide'); 
                        $("#mediumModalLabel122018{{$userv->id}}").text("Add Number Of Children");
                   });
               </script>

   <?php
        } }
   ?>
            <script type="text/javascript">

   $("#getpasschange").click(function(){
        var name = $("#name").val();

    var number = $("#number").val();

     var number_of_nannies = $("#number_of_nannies").val();

    var number_of_children = $("#number_of_children").val();

    var grade_id = $("#grade_id").val();

    var _token = $("#csrf-token").val();

    if(grade_id != '0'){

    $.post("{{url('/')}}/addchildes",{name:name,number:number,number_of_nannies:number_of_nannies,number_of_children:number_of_children,_token:_token,grade_id:grade_id},function(result){
            var data = JSON.parse(result);

            if(data[0] == 1){
                    $('#mediumModal').modal('hide'); 
                    $('#myModal4').modal('show'); 
            } else {
                $("#resultpass").html('<div class="alert alert-danger">'+data[1]+'</div>');
            }
            //alert(data);
    });

    } else {
         $("#resultpass").html('<div class="alert alert-danger">Please Select a Grade</div>');
    }
   });
    
     $(".datadis12").click(function(){
        location.reload();
    });


      $(".gettingp").click(function(){
        var name = $("#name"+$(this).attr('id')).val();

    var number = $("#number"+$(this).attr('id')).val();

     var number_of_nannies = $("#number_of_nannies"+$(this).attr('id')).val();

    var number_of_children = $("#number_of_children"+$(this).attr('id')).val();

    var grade_id = $("#grade_id"+$(this).attr('id')).val();

    var _token = $("#csrf-token").val();

    if(grade_id != '0'){

    $.post("{{url('/')}}/editchildes",{name:name,number:number,number_of_nannies:number_of_nannies,number_of_children:number_of_children,_token:_token,grade_id:grade_id,id:$(this).attr('id')},function(result){
            var data = JSON.parse(result);

            if(data[0] == 1){
                    $('#mediumModal'+data[2]).modal('hide'); 
                    $('#myModal5').modal('show'); 
            } else {
                $("#resultpass"+data[2]).html('<div class="alert alert-danger">'+data[1]+'</div>');
            }
            //alert(data);
    }); } else {
        $("#resultpass"+$(this).attr('id')).html('<div class="alert alert-danger">Please Select a Grade</div>');
    }
   });

    $(".delclassroom").click(function(){
            $.post("{{url('/')}}/deletechild",{id:$(this).attr('id')},function(result){
            var data = JSON.parse(result);

            if(data[0] == 1){
                    
                    $('#myModal6').modal('show'); 
            }
            //alert(data);
    });
    });  

</script>
<!-- <script type="text/javascript">
  $("#number").keypress(function (evt) {
  
  var keycode = evt.charCode || evt.keyCode;
    //alert(keycode);
  if (keycode  == 48) { //Enter key's keycode
    return false;
  }
});
</script> -->
@endsection