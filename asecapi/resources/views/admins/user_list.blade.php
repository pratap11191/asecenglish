@extends('layouts.admins') 
@section('title', 'Nursery List')
@section('content')

<div class="row">
</div>
    <div class="col-md-12">
        <!-- BORDERED TABLE -->
        <div class="panel">
            <div class="panel-heading">
               <!-- <h3 class="panel-title">
                    Nursery Management List
                </h3>-->

<p>
                    Nursery Management Lists
                </p>
				
				
											
                  <a style="float:right;margin-bottom:10px;" class="au-btn au-btn-icon au-btn--blue" href="{{url('addnursury')}}" >
                                 <i class="zmdi zmdi-plus"></i> Create New                                         
                                </a>
            </div>
            <div class="panel-body">
			
			
										
		<div class="table-responsive m-b-40">
               <table class="table table-borderless table-data3" id="data">
				  <thead>
                        <tr>
                            <th>
                                #
                            </th>
                            <th>
                                Email
                            </th>
                            <th>
                                Name
                            </th>
                            <th>
                               Phone No. 
                            </th>
                           
                           
                            <th>
                                Profile Image
                            </th>
                            <th>
                             Action 
                             </th>
                            <th>
                                Status
                            </th>
                        </tr>
                    </thead>
                    @if(!empty($users))
                    <tbody>
                        <?php $i=0;?>
                        @foreach($users as $user)
                        <tr>
                            <td>
                                {{ ++$i }}
                            </td>
                            <td>
                                {{$user->email}}
                            </td>
                            <td>
                                {{$user->fullname}}
                            </td>
                            <td>
                                {{$user->phone}}
                            </td>
                            
                            
                            
                            <td>
                                <img alt="Avatar" class="img-circle" src="{{url('/')}}/public/{{$user->image}}" style="height:50px">
                                </img>
                            </td>
                             <td>
							 <div class="table-data-feature"> 
							 <a href="{{url('editnursury')}}/{{$user->id}}">
								 <button class="item" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit">
									<i class="zmdi zmdi-edit"></i>
								 </button>
							 </a>
							  							
                             <a class="action_an" href="{{url('common_delete')}}/{{$user->id}}/users" > 
								 <button class="item" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete">
									<i class="zmdi zmdi-delete"></i>
								 </button>
                             </a>
							</div>
                             </td>
                            <td>
                                 @if($user->admin_status == 0)
                                         <a class="action_an btn btn-danger" href="{{url('change_status')}}/{{$user->id}}" >
                                    <span class="dlt_icon">
                                        <?php echo "Inactive"; ?>
                                         </span>
                                </a>
                                        @else
                                         <a class="action_an btn btn-success" href="{{url('change_status')}}/{{$user->id}}" >
                                    <span class="dlt_icon">
                                        <?php echo "Active" ?>
                                         </span>
                                </a>
                                        @endif
                                   
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    @endif
                </table>
		</div>
            </div>
        </div>
       
        <!-- END BORDERED TABLE -->
    </div>
</div>

@endsection