@extends('layouts.admins') 
@section('title', 'Classrooms List')
@section('content')


<div class="mychatbars allmessages">
<div class="col-md-12">
<div class="panel">
	<div class="panel-heading">
            <h3 class="panel-title">
                All Chat Messages
            </h3>             
    </div>
	
	<div class="panel-body">
	
	
		<div class="mess__item">
				<a href="http://mobuloustech.com/monami/chat"><div class="image img-cir img-40">
					<img src="/monami/images/icon/avatar-01.jpg" alt="Michelle Moreno">
				</div>
				<div class="content  mychatopn">
					<h6>Michelle Moreno</h6>
					<p>Have sent a photo</p>
					<span class="time">3 min ago</span>
				</div> 
				</a>
        </div>

		<div class="mess__item">
				<a href="http://mobuloustech.com/monami/chat"><div class="image img-cir img-40">
					<img src="/monami/images/icon/avatar-01.jpg" alt="Michelle Moreno">
				</div>
				<div class="content  mychatopn">
					<h6>Michelle Moreno</h6>
					<p>Have sent a photo</p>
					<span class="time">3 min ago</span>
				</div> 
				</a>
        </div>
		
		
		<div class="mess__item">
				<a href="http://mobuloustech.com/monami/chat"><div class="image img-cir img-40">
					<img src="/monami/images/icon/avatar-01.jpg" alt="Michelle Moreno">
				</div>
				<div class="content  mychatopn">
					<h6>Michelle Moreno</h6>
					<p>Have sent a photo</p>
					<span class="time">3 min ago</span>
				</div> 
				</a>
        </div>
		
		
		<div class="mess__item">
				<a href="http://mobuloustech.com/monami/chat"><div class="image img-cir img-40">
					<img src="/monami/images/icon/avatar-01.jpg" alt="Michelle Moreno">
				</div>
				<div class="content  mychatopn">
					<h6>Michelle Moreno</h6>
					<p>Have sent a photo</p>
					<span class="time">3 min ago</span>
				</div> 
				</a>
        </div>
	
	 
	</div> 
</div>
</div> 
</div>

@endsection