@extends('layouts.admins') 
@section('title', 'Grade Details')
@section('content')

 <div class="col-md-12">
        <!-- BORDERED TABLE -->
        <div class="panel">
            @if(Session::has('message'))
<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
@endif
            <div class="panel-body">
			<div class="newdtlrow">
			<div class="row">
			
			<div class="col-md-12">
            <ul>
				<li>Class Room: {{ucfirst($classroomsdetails_name)}}</li>
							
			</ul>
			</div>
			
			<div class="col-md-6">
            <ul>
					
				<li>Children List</li>	
                <li>Total No. Of Children: {{sizeof($usersgradedetails)}}</li>				
			</ul>
			</div>
			
			<div class="col-md-6 newswag">
            <ul>
				<li> 
					<div class="csvupdate">
                                <form action="{{url('/')}}/uploadcsv/{{$classroomsdetails_id}}" method="post" enctype="multipart/form-data">
								 <input type="file" class="myfilecsv" name="filename"> 
								 
								 <i class="pdftype">Upload New CSV</i>

                                 <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                                 <button type="submit">Submit</button>
                                 </form>
					</div>
				</li>				
				<li> <a class="item au-btn au-btn-icon au-btn--blue addlischd" data-toggle="modal" data-target="#mediumModalgedetails">
                                   <i class="zmdi zmdi-plus"></i>Add Children  
                                 </a>
</li>
			</ul>
			
			</div>
			</div>
			</div>
                                        
        <div class="table-responsive m-b-40">
               <table class="table">
                  <thead class="thead-dark">
                        <tr>
                            <th>
                                No
                            </th>
                            <th>
                               Name
                            </th>
                            <th>
                               CUID
                            </th>
                            <th>
                              Parent Name
                            </th>
                           
                           
                            <th>
                                Email
                            </th>
                            <th>
                             Phone
                             </th>
                             <th>Action</th>
                             <th>Status</th>
                            
                        </tr>
                    </thead>
                    @if(!empty($usersgradedetails))
                    <tbody>
                        <?php $i=0;?>
                        @foreach($usersgradedetails as $user)
                        <tr>
                            <td>
                                {{ ++$i }}
                            </td>
                            <td>
                                {{$user->name}}
                            </td>
                            <td>
                                {{$user->CUID}}
                            </td>
                            <td>
                                {{$user->father}}
                            </td>
                              <td>
                                {{$user->email}}
                            </td>
                             <td>
                                {{$user->phone}}
                            </td>
                            
                             <td>
                           <!--  <div class="table-data-feature">  -->
               
                 <button class="item" data-toggle="modal" data-target="#mediumModalgradedetails{{$user->id}}" data-placement="top" title="" data-original-title="Edit">
                  <i class="zmdi zmdi-edit"></i>
                 </button>
              
                              
                             <a class="action_an" href="{{url('common_delete')}}/{{$user->id}}/childrens" > 
                 <button class="item" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete">
                  <i style="color:red;" class="zmdi zmdi-delete"></i>
                 </button>
                             </a>
           <!--    </div> -->
                             </td>
                             <td>
                                @if($user->admin_status == 0)
                                <a class="action_an btn btn-danger" href="{{url('change_statusChildren')}}/{{$user->id}}" >
                                <span class="dlt_icon">
                                <?php echo "Inactive"; ?>
                                </span>
                                </a>
                                @else
                                <a class="action_an btn btn-success" href="{{url('change_statusChildren')}}/{{$user->id}}" >
                                <span class="dlt_icon">
                                <?php echo "Active" ?>
                                </span>
                                </a>
                                @endif
                            </td>
                            
                        </tr>
                        
                        @endforeach
                    </tbody>
                    @endif
                </table>
        </div>
            </div>
			
			
			 <div class="">
            
            <div class="panel-body">
			<div class="newdtlrow spacenewt">
			<div class="row">
			<div class="col-md-6">
            <ul>
				<li>Nanny  List</li>	
                <li>Total No. Of Nannies: {{sizeof($users1gradedetails)}}</li>  			
			</ul>
			</div>
			
			<div class="col-md-6 newswag singlebths">
            <ul>
								
				<li><a class="item au-btn au-btn-icon au-btn--blue addlischd" data-toggle="modal" data-target="#mediumModalgradedetailsnanny">
                                   <i class="zmdi zmdi-plus"></i> Add Nanny
                                 </a></li>
			</ul>
			
			</div>
			</div>
			</div>
                                        
        <div class="table-responsive m-b-40">
               <table class="table">
                  <thead class="thead-dark">
                        <tr>
                           <th>
                                No
                            </th>
                            <th>
                                First Name
                            </th>
                            <th>
                               Last Name
                            </th>
                            <th>
                              Email Id
                            </th>
                           
                           
                            <th>
                                Password
                            </th>
                            <th>
                             Action 
                             </th>
                            
                        </tr>
                    </thead>
                    @if(!empty($users1gradedetails))
                    <tbody>
                        <?php $i=0;?>
                        @foreach($users1gradedetails as $user)
                        <tr>
                          <td>
                                {{ ++$i }}
                            </td>
                            <td>
                                {{$user->name}}
                            </td>
                            <td>
                                {{$user->surename}}
                            </td>
                            <td>
                                {{$user->email}}
                            </td>
                              <td>
                                {{$user->password}}
                            </td>
                            
                             <td>
                            <!--  <div class="table-data-feature">  -->
                             
                                 <button class="item" data-toggle="modal" data-target="#mediumModalgradedetailsnanny{{$user->id}}" data-placement="top" title="" data-original-title="Edit">
                                    <i class="zmdi zmdi-edit"></i>
                                 </button>
                            
                                                        
                             <a class="action_an" href="{{url('common_delete')}}/{{$user->id}}/nannies" > 
                                 <button class="item" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete">
                                    <i style="color:red;" class="zmdi zmdi-delete"></i>
                                 </button>
                             </a>
                            <!-- </div> -->
                             </td>
                            
                        </tr>
                        
                        @endforeach
                    </tbody>
                    @endif
                </table>
        </div>
            </div>
        </div>
       
        <!-- END BORDERED TABLE -->
    </div>


	<style>
	.newdtlrow {
    float: left;
    width: 100%;
	    margin-bottom: 20px;
}
.newswag.singlebths ul li {
    width: 100%;
}

.csvupdate button {
    color: #de7898;
    margin: 4px 0 0;
    font-size: 16px;
    border: 1px solid;
    padding: 0 6px;
}

.newdtlrow ul{list-style-type:none;}
.newdtlrow ul li{
        font-size: 16px;
    font-weight: 700;
    color:#333;
}
.newswag ul li {
    width: 48%;
    float: left;
    margin: 0 15px 0 0;
	    text-align: right;
}

.newswag ul li:last-child{margin-right:0;}

input.myfilecsv {
    opacity: 0;
    z-index: 999999;
    float: left;
    margin: 0 0 0 0px;
    padding: 1%;
    position: absolute;
    width: 100%;
    cursor: pointer;
    left: 0;
}

.pdftype {
    position: relative;
    top: 0;
    right: 0;
    z-index: 0;
    cursor: pointer;
    font-size: 16px;
    float: left;
        width: 64%;
    background: #de7898;
    box-shadow: #000 0px 1px 8px 0px;
    font-weight: 400;
    font-style: initial;
    color: #FFF;
    padding: 1px 0;
    margin: 4px 0 0 0;
}

.csvupdate {
    float: left;
    position: relative;
    width: 100%;
    z-index: 999999;
    text-align: center;
    color: #000;
}

.addlischd {
    padding: 7px 11px;
    font-size: 12px;
    line-height: 14px;
}

.spacenewt {
    margin-bottom: 20px;
    border-top: 1px solid #212529;
    padding: 56px 0 0 0;
    margin-top: 21px;
}
	</style>


<style type="text/css">
    .formerror{
    color: red;
    }
</style>
	
@endsection