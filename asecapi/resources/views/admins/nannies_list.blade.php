@extends('layouts.admins') 
@section('title', 'Nannies List')
@section('content')

<div class="row">
</div>
    <div class="col-md-12">
        <!-- BORDERED TABLE -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Nanny Management List
                </h3>

                     <?php  if(!empty($users1)){ ?>
                
                
                                             <a style="float:right;margin-bottom:10px;" class="au-btn au-btn-icon au-btn--blue" data-toggle="modal" data-target="#mediumModal">
                                 <i class="zmdi zmdi-plus"></i> Add Nanny                                  
                                </a>
                                  <?php } else { ?>
                                    <a style="float:right;margin-bottom:10px;" class="au-btn au-btn-icon au-btn--blue" href="{{url('/')}}/classroom_list">
                                 <i class="zmdi zmdi-plus"></i> Add Nanny                                  
                                </a>

                                <?php

                                } ?>
            </div>
            <div class="panel-body">
            
            
                                        
        <div class="table-responsive m-b-40">
               <table class="table table-borderless table-data3" id="data">
                  <thead>
                        <tr>
                            <th>
                                #
                            </th>
                            <th>
                                First Name
                            </th>
                            <th>
                               Last Name
                            </th>
                            <th>
                              Email Id
                            </th>
                           
                           
                            <th>
                                Password
                            </th>
                            <th>
                             Action 
                             </th>
                            
                        </tr>
                    </thead>
                    @if(!empty($users))
                    <tbody>
                        <?php $i=0;?>
                        @foreach($users as $user)
                        <tr>
                            <td>
                                {{ ++$i }}
                            </td>
                            <td>
                                {{$user->name}}
                            </td>
                            <td>
                                {{$user->surename}}
                            </td>
                            <td>
                                {{$user->email}}
                            </td>
                              <td>
                                {{$user->password}}
                            </td>
                            
                             <td>
                             <div class="table-data-feature"> 
                             
                                 <button class="item" data-toggle="modal" data-target="#mediumModal{{$user->id}}" data-placement="top" title="" data-original-title="Edit">
                                    <i class="zmdi zmdi-edit"></i>
                                 </button>
                            
                                                        
                             <a class="action_an" href="{{url('common_delete')}}/{{$user->id}}/nannies" > 
                                 <button class="item" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete">
                                    <i class="zmdi zmdi-delete"></i>
                                 </button>
                             </a>
                            </div>
                             </td>
                            
                        </tr>
                        
                        @endforeach
                    </tbody>
                    @endif
                </table>
        </div>
            </div>
        </div>
       
        <!-- END BORDERED TABLE -->
    </div>
</div>

<?php

if(!empty($users)){

    foreach ($users as $user) {
        
    

?>

<div class="modal fade" id="mediumModal{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel{{$user->id}}" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="mediumModalLabel{{$user->id}}">Add Nanny</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="card">
                                   <div id="resultpass{{$user->id}}"></div> 
                                    <div class="card-body card-block">
                                       <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="text-input" class=" form-control-label">Nanny First Name</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <input type="text" id="name{{$user->id}}" name="text-input" placeholder="Nanny First Name" class="form-control" value="{{$user->name}}">
                                                    
                                                </div>
                                            </div>
                                            
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="text-input" class=" form-control-label">Nanny Last Name</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <input type="text" id="surname{{$user->id}}" name="text-input" placeholder="Nanny Last Name" class="form-control" value="{{$user->surename}}">
                                                    
                                                </div>
                                            </div>

                                             <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="text-input" class=" form-control-label">Nanny Email Id</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <input type="email" id="username{{$user->id}}" name="text-input" placeholder="Nanny username" class="form-control" value="{{$user->email}}">
                                                    
                                                </div>
                                            </div>

                                             <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="text-input" class=" form-control-label">Nanny Password</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <input type="text" id="password{{$user->id}}" name="text-input" placeholder="Nanny Password" class="form-control" value="{{$user->password}}">
                                                    
                                                </div>
                                            </div>


                                             <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="text-input" class=" form-control-label">Classroom Name</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                   <select name="classroom_id" id="classroom_idn{{$user->id}}" class="form-control">
                <option value="0" >Please Select a Classroom</option>
                   <?php

                        if(!empty($users1)){

                            foreach ($users1 as $users12) {
                                

                   ?>
                    <option value="{{$users12['id']}}">{{$users12['name']}}  {{$users12['number']}}</option>
                  <?php

                 }

              }

              ?>
        </select>
      
                                                    
                                                </div>
                                            </div>

                                            
                                            
                                             <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                                          
                                      

                                            
                                            
                                    </div>
                                    <div class="card-footer">
                                        <button id="{{$user->id}}" type="button" class="btn btn-primary btn-sm gettingp">
                                            <i class="fa fa-dot-circle-o"></i> Submit
                                        </button>
                                        <button type="reset" class="btn btn-danger btn-sm">
                                            <i class="fa fa-ban"></i> Reset
                                        </button>
                                    </div>
                                      </form>
                                </div>
                        </div>
                        
                    </div>
                </div>
            </div>

<?php

}

}
?>
<div class="modal fade" id="mediumModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="mediumModalLabel">Add Nanny</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="card">
                                   <div id="resultpass"></div>
                                    <div class="card-body card-block">
                                       <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="text-input" class=" form-control-label">Nanny First Name</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <input type="text" id="name" name="text-input" placeholder="Nanny First Name" class="form-control">
                                                    
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="text-input" class=" form-control-label">Nanny Last Name</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <input type="text" id="surname" name="text-input" placeholder="Nanny Last Name" class="form-control">
                                                    
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="text-input" class=" form-control-label">Nanny Email Id</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <input type="email" id="username" name="text-input" placeholder="Nanny Email Id" class="form-control">
                                                    
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="text-input" class=" form-control-label">Nanny Password</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <input type="text" id="password" name="text-input" placeholder="Nanny Password" class="form-control">
                                                    
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="text-input" class=" form-control-label">Classroom Name</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                   <select name="classroom_id" id="classroom_idn" class="form-control">
                <option value="0" >Please Select a Classroom</option>
                   <?php

                        if(!empty($users1)){

                            foreach ($users1 as $users12) {
                                

                   ?>
                    <option value="{{$users12['id']}}">{{$users12['name']}}  {{$users12['number']}}</option>
                  <?php

                 }

              }

              ?>
        </select>
      
                                                    
                                                </div>
                                            </div>
                                            
                                            
                                           
                                             <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                                                     
                                    </div>
                                    <div class="card-footer">
                                        <button id="getpasschange" type="button" class="btn btn-primary btn-sm">
                                            <i class="fa fa-dot-circle-o"></i> Submit
                                        </button>
                                        <button type="reset" class="btn btn-danger btn-sm">
                                            <i class="fa fa-ban"></i> Reset
                                        </button>
                                    </div>
                                      </form>
                                </div>
                        </div>
                        
                    </div>
                </div>
            </div>
 <div class="deletesucchealthapp">
  <div class="modal fade" id="myModal4" style="display: none;">
        <div class="modal-dialog">
          <div class="modal-content">
          <div class="modal-body">
          <div class="succhealth">
           <i class="fa fa-check"></i>
           </div>
          
         
         
          <div class="setbtnoks">
           <h1>Success!</h1>
          <p> Nanny created successfully!</p>
           <button class="close allokbtns datadis12" >Ok</button>
          </div>
          </div>

          </div>
          </div>
          </div>
   </div>

   <div class="deletesucchealthapp">
  <div class="modal fade" id="myModal6" style="display: none;">
        <div class="modal-dialog">
          <div class="modal-content">
          <div class="modal-body">
          <div class="succhealth">
           <i class="fa fa-check"></i>
           </div>
          
         
         
          <div class="setbtnoks">
           <h1>Success!</h1>
          <p> Nanny deleted successfully!</p>
           <button class="close allokbtns datadis12" >Ok</button>
          </div>
          </div>

          </div>
          </div>
          </div>
   </div>

   <div class="deletesucchealthapp">
  <div class="modal fade" id="myModal5" style="display: none;">
        <div class="modal-dialog">
          <div class="modal-content">
          <div class="modal-body">
          <div class="succhealth">
           <i class="fa fa-check"></i>
           </div>
          
         
         
          <div class="setbtnoks">
           <h1>Success!</h1>
          <p> Nanny updated successfully!</p>
           <button class="close allokbtns datadis12" >Ok</button>
          </div>
          </div>

          </div>
          </div>
          </div>
   </div>
            <script type="text/javascript">

   $("#getpasschange").click(function(){
        var name = $("#name").val();

    var surename = $("#surname").val();

     var username = $("#username").val();

    var password = $("#password").val();

    var classroom_id = $("#classroom_idn").val();

    var _token = $("#csrf-token").val();

    $.post("{{url('/')}}/addnannies",{classroom_id:classroom_id,name:name,surename:surename,email:username,password:password,_token:_token},function(result){
            var data = JSON.parse(result);

            if(data[0] == 1){
                    $('#mediumModal').modal('hide'); 
                    $('#myModal4').modal('show'); 
            } else {
                $("#resultpass").html('<div class="alert alert-danger">'+data[1]+'</div>');
            }
            //alert(data);
    });
   });
    
     $(".datadis12").click(function(){
        location.reload();
    });


      $(".gettingp").click(function(){
        var name = $("#name"+$(this).attr('id')).val();

    var surname = $("#surname"+$(this).attr('id')).val();

     var username = $("#username"+$(this).attr('id')).val();

    var password = $("#password"+$(this).attr('id')).val();

      var classroom_id = $("#classroom_idn"+$(this).attr('id')).val();

    var _token = $("#csrf-token").val();

    $.post("{{url('/')}}/editnannies",{classroom_id:classroom_id,name:name,surename:surname,email:username,password:password,_token:_token,id:$(this).attr('id')},function(result){
            var data = JSON.parse(result);

            if(data[0] == 1){
                    $('#mediumModal'+data[2]).modal('hide'); 
                    $('#myModal5').modal('show'); 
            } else {
                $("#resultpass"+data[2]).html('<div class="alert alert-danger">'+data[1]+'</div>');
            }
            //alert(data);
    });
   });

    

</script>
@endsection