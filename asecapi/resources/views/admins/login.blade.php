<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Monami Login</title>

    <!-- Fontfaces CSS-->
    <link href="{{ asset('/public/css1/font-face.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('/public/vendor1/font-awesome-4.7/css1/font-awesome.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('/public/vendor1/font-awesome-5/css1/fontawesome-all.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('/public/vendor1/mdi-font/css1/material-design-iconic-font.min.css') }}" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="{{ asset('/public/vendor1/bootstrap-4.1/bootstrap.min.css') }}" rel="stylesheet" media="all">

    <!-- vendor1 CSS-->
    <link href="{{ asset('/public/vendor1/animsition/animsition.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('/public/vendor1/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('/public/vendor1/wow/animate.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('/public/vendor1/css-hamburgers/hamburgers.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('/public/vendor1/slick/slick.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('/public/vendor1/select2/select2.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('/public/vendor1/perfect-scrollbar/perfect-scrollbar.css') }}" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="{{ asset('/public/css1/theme.css') }}" rel="stylesheet" media="all">

</head>

<body class="animsition">
    <div class="page-wrapper">
        <div class="page-content--bge5">
            <div class="container">
                <div class="login-wrap">
                    <div class="login-content">
                        <div class="login-logo">
                            <a href="#">
                                <img src="{{ asset('/public/img/logo-admin.png') }}" alt="CoolAdmin">
                            </a>
                        </div>
                        <div class="flash-message">
                                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                                    @if(Session::has('alert-' . $msg))
                                        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} 
                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        </p>
                                    @endif
                                @endforeach
                            </div> <!-- end .flash-message -->
                        <div class="login-form">
                            <form action="{{url('/')}}/signin" method="POST">
                                <div class="form-group">
                                    <label>Email Address</label>
                                    <input class="au-input au-input--full" type="email" name="email" placeholder="Email">
                                     @if ($errors->has('email'))
                                        <span class="help-block alert-danger">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input class="au-input au-input--full" type="password" name="password" placeholder="Password">
                                    @if ($errors->has('password'))
                                        <span class="help-block alert-danger">
                                         <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                </div>
                                <div class="login-checkbox">
                                    <label>
                                        <input type="checkbox" name="remember">Remember Me
                                    </label>
                                    <label>
                                        <a href="<?= url('forgot_password') ?>">Forgot Password?</a>
                                    </label>
                                </div>
                                <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit">sign in</button>
                               <!--  <div class="social-login-content">
                                    <div class="social-button">
                                        <button class="au-btn au-btn--block au-btn--blue m-b-20">sign in with facebook</button>
                                        <button class="au-btn au-btn--block au-btn--blue2">sign in with twitter</button>
                                    </div>
                                </div> -->
                            </form>
                            <div class="register-link">
                                <p>
                                    Don't you have account?
                                    <a href="{{url('/')}}/signup">Sign Up</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- Jquery JS-->
    <script src="{{ asset('/public/vendor1/jquery-3.2.1.min.js') }}"></script>
    <!-- Bootstrap JS-->
    <script src="{{ asset('/public/vendor1/bootstrap-4.1/popper.min.js') }}"></script>
    <script src="{{ asset('/public/vendor1/bootstrap-4.1/bootstrap.min.js') }}"></script>
    <!-- vendor1 JS       -->
    <script src="{{ asset('/public/vendor1/slick/slick.min.js') }}">
    </script>
    <script src="{{ asset('/public/vendor1/wow/wow.min.js') }}"></script>
    <script src="{{ asset('/public/vendor1/animsition/animsition.min.js') }}"></script>
    <script src="{{ asset('/public/vendor1/bootstrap-progressbar/bootstrap-progressbar.min.js') }}">
    </script>
    <script src="{{ asset('/public/vendor1/counter-up/jquery.waypoints.min.js') }}"></script>
    <script src="{{ asset('/public/vendor1/counter-up/jquery.counterup.min.js') }}">
    </script>
    <script src="{{ asset('/public/vendor1/circle-progress/circle-progress.min.js') }}"></script>
    <script src="{{ asset('/public/vendor1/perfect-scrollbar/perfect-scrollbar.js') }}"></script>
    <script src="{{ asset('/public/vendor1/chartjs/Chart.bundle.min.js') }}"></script>
    <script src="{{ asset('/public/vendor1/select2/select2.min.js') }}">
    </script>

    <!-- Main JS-->
    <script src="{{ asset('/public/js1/main.js') }}"></script>

</body>

</html>
<!-- end document-->