@extends('layouts.admins') 
@section('title', 'Menu List')
@section('content')
<style type="text/css">
    .formerror{
        color: red;
    }
</style>

<div id="foodmenu" class="tabcontent" >
			<div class="main-content">
						<div class="section__content section__content--p30">
							<div class="container-fluid">
							  @if(session()->has('message'))
		    <div class="alert alert-success">
		        {{ session()->get('message') }}
		    </div>
		@endif
							<div class="overview-wrap">
										<h2 class="title-1">Define Menu</h2>		
										<button class="au-btn au-btn-icon au-btn--blue" data-toggle="modal" data-target="#mediumModal4">
											<i class="zmdi zmdi-plus"></i>Add Meal</button>
										
							</div>
							
							<div class="itemperpageretails">
											<span><strong>Item As per Days:- </strong> </span>
											<select>
												  <option value="volvo">Monday</option>
												  <option value="saab">Tuesday</option>
												  <option value="mercedes">Wednusday</option>
												  <option value="audi">Thursday</option>
												  <option value="audi">Friday</option>
												  <option value="audi">Saturday</option>
												  
											</select>
										</div>	
											
							
							<div class="col-md-12 tabdatabga">
								<div class="row">
                                <div class="col-md-4">
                                <div class="card">
                                    <div class="card-header">
                                        <strong class="card-title mb-3">Breakfast</strong>
                                    </div>
                                    <div class="card-body">
                                     
                                      	<?php

                                      		if(!empty($Breakfast)){ ?>
                                      	
                                      		<?php

                                      			foreach ($Breakfast as $value) { 
                                                $allbreackfastlist = explode(",", $value['item_lists1']);

                                                if(!empty($value['item_lists1'])){
                                              ?>
                                              <p>{{ucfirst($value['day_of_week'])}}</p>
                                                <ol>
                                              <?php
                                                foreach ($allbreackfastlist as $value_new) {
                                                  
                                              ?>

                                              <li>{{$value_new}}</li>

                                              <?php } ?>
                                              </ol>
                                      			<?php

                                            }
                                      				
                                      			} ?>
                                      			
                                      			<?php
                                      		} else{
                                      			echo "<p>Not any item in Breakfast</p>";
                                      		}
                                      	?>
                                      
                                    </div>
                                </div>
                            </div>
							
							
							 <div class="col-md-4">
                                <div class="card">
                                    <div class="card-header">
                                        <strong class="card-title mb-3">Lunch</strong>
                                    </div>
                                    <div class="card-body">
                                    
                                      	<?php

                                      		if(!empty($Lunch)){ ?>
                                      	
                                      		<?php
                                      			foreach ($Lunch as $value) {     
                                            $allbreackfastlist = explode(",", $value['item_lists2']);

                                                if(!empty($value['item_lists2'])){
                                              ?>
                                              <p>{{ucfirst($value['day_of_week'])}}</p>
                                                <ol>
                                              <?php
                                                foreach ($allbreackfastlist as $value_new) {
                                                  
                                              ?>

                                              <li>{{$value_new}}</li>

                                              <?php } ?>
                                              </ol>
                                                <?php

                                            }
                                      				
                                      			} ?>
                                      			
                                      			<?php
                                      		} else{
                                      			echo "<p>Not any item in Lunch</p>";
                                      		}
                                      	?>
                                     
                                    </div>
                                </div>
                            </div>
							
							
							 <div class="col-md-4">
                                <div class="card">
                                    <div class="card-header">
                                        <strong class="card-title mb-3">Snack</strong>
                                    </div>
                                    <div class="card-body">
                                      <ol>
                                      	<?php

                                      		if(!empty($Snack)){ ?>
                                      		
                                      		<?php
                                      			foreach ($Snack as $value) {     
                                                    $allbreackfastlist = explode(",", $value['item_lists3']);

                                                if(!empty($value['item_lists3'])){
                                              ?>
                                              <p>{{ucfirst($value['day_of_week'])}}</p>
                                                <ol>
                                              <?php
                                                foreach ($allbreackfastlist as $value_new) {
                                                  
                                              ?>

                                              <li>{{$value_new}}</li>

                                              <?php } ?>
                                              </ol>
                                                <?php

                                            }
                                      				
                                      			} ?>
                                      			
                                      			<?php
                                      		} else{ 
                                      			echo "<p>Not any item in Snack</p>";
                                      		}
                                      	?>
                                      
                                    </div>
                                </div>
                            </div>
							
							
							
								</div>
							  
							</div>
			
							</div>
						</div>
			</div>
			</div>
			
			<style>
			.itemperpageretails {
    float: right;
    margin: 20px 0;
}
			</style>

@endsection
