@extends('layouts.admins') 
@section('title', 'Chat Messages')
@section('content')


<div class="mychatbars">
<div class="col-md-12">
<div class="panel">
	<div class="panel-heading">
            <h3 class="panel-title">
                Chat Messages
            </h3>             
    </div>
	
	<div class="panel-body">
	
	<div class="chat_list-aside">
                        <div class="chatsection-page">
                            <div class="chat_useru_profile">
                               <span class="chat-headers-imges">
                                    <img src="/monami/images/icon/avatar-01.jpg" class="img-responsive">
                                    <span class="chat_useru_profile-name">Michelle Moreno</span>
                                    <span class="chat_useru_profile-timesago">3 min ago</span>
                               </span>
                            </div>
                            <div class="chat-section-chat">
                                <ul>
                                    <li class="chat-left-sections-type"> 
                                        <div class="chattypesec chattypesec_left">
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>
											<p class="timemgn">28 Nov 2018 at 2:00PM</p>
                                        </div> 
                                    </li>
                                    <li class="chat-right-sections-type">
                                        <div class="chattypesec chattypesec_right">
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>
											<p class="timemgn">today at 2:00PM</p>
                                        </div>
                                    </li>

                                    <li class="chat-left-sections-type">
                                        <div class="chattypesec chattypesec_left">
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>
											<p class="timemgn">today at 2:06PM</p>
                                        </div>
                                    </li>
                                    <li class="chat-right-sections-type">
                                        <div class="chattypesec chattypesec_right">
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>
											<p class="timemgn">today at 2:56PM</p>
                                        </div>
                                    </li>
                                    <li class="chat-right-sections-type">
                                        <div class="chattypesec chattypesec_right">
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since</p>
											<p class="timemgn">today at 3:00PM</p>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="type-message">
                                <div class="typemsagesection">
                                    <div class="meesgaetypefeeilds">
                                        <textarea class="textarechats" placeholder="Write your message..."></textarea>
                                    </div>
                                    <div class="meesgaetypefeeildsicon">
                                        <button type="submit" value="Send" class="sen_btn_chat_mssg"><img src="/monami/images/chat_sendn.png" class="img-responsive"></button>
                                    </div>
                                </div>
                            </div>
                        </div>
	</div>
				
	
	 
</div> 
</div>
</div> 
</div>

@endsection