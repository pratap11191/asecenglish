@extends('layouts.admins') 
@section('title', 'Parents List')
@section('content')

<div class="row">
</div>
    <div class="col-md-12">
        <!-- BORDERED TABLE -->
        <div class="panel">
            <div class="panel-heading">
                <!--<h3 class="panel-title">
                    Parents Management List
                </h3>-->


<p>Parents Management List</p>        
                  <a style="float:right;margin-bottom:10px;" class="au-btn au-btn-icon au-btn--blue" href="{{url('addparents')}}" >
                                 Create New                                         
                                </a>
            </div>
            <div class="panel-body">
			<div class="table-responsive m-b-40">
                <table class="table table-borderless table-data3" id="data">
                    <thead>
                        <tr>
                            <th>
                                #
                            </th>
                            <th>
                                Email
                            </th>
                            <th>
                                Fullname
                            </th>
                            <th>
                                Phone No. 
                            </th>
                           <th>
                                Nursury Name
                            </th>
                           
                            <th>
                                Profile Image
                            </th>
                            <th>
                             Action 
                             </th>
                            <th>
                                Status
                            </th>
                        </tr>
                    </thead>
                    @if(!empty($users))
                    <tbody>
                        <?php $i=0;?>
                        @foreach($users as $user)
                        <tr>
                            <td>
                                {{ ++$i }}
                            </td>
                            <td>
                                {{$user->email}}
                            </td>
                            <td>
                                {{$user->fullname}}
                            </td>
                            <td>
                                {{$user->phone}}
                            </td>
                             <td>
                                {{$user->nfullname}}
                            </td>
                            
                            
                            <td>
                                <img alt="Avatar" class="img-circle" src="{{url('/')}}/public/{{$user->image}}" style="height:50px">
                                </img>
                            </td>
                             <td>
							 <div class="table-data-feature">
                             <a  href="{{url('editparents')}}/{{$user->id}}">
							  <button class="item" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit">
									<i class="zmdi zmdi-edit"></i>
								 </button>
                               </a>
                                
							 <a class="action_an" href="{{url('common_delete')}}/{{$user->id}}/members" >
                                    <button class="item" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete">
									<i class="zmdi zmdi-delete"></i>
								 </button>
                                </a>
								
							</div>
                             </td>
                            <td>
                               
                               
                                        @if($user->admin_status == 0)
                                         <a class="action_an btn btn-danger" href="{{url('change_status1p')}}/{{$user->id}}" >
                                    <span class="dlt_icon">
                                        <?php echo "Inactive"; ?>
                                         </span>
                                </a>
                                        @else
                                         <a class="action_an btn btn-success" href="{{url('change_status1p')}}/{{$user->id}}" >
                                    <span class="dlt_icon">
                                        <?php echo "Active" ?>
                                         </span>
                                </a>
                                        @endif
                                   
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    @endif
                </table>
				</div>
            </div>
        </div>
       
        <!-- END BORDERED TABLE -->
    </div>
</div>

@endsection