@extends('layouts.admins') 
@section('title', 'Report List')
@section('content')


<div class="col-md-12">
        <!-- BORDERED TABLE -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Report Management List
                </h3>
               
            </div>
            <div class="panel-body">						
				<div class="table-responsive m-b-40">
					  <table class="table table-borderless table-data3 dataTable no-footer" id="data" role="grid" aria-describedby="data_info">
						  <thead>
								<tr role="row">
									<th class="sorting_asc" tabindex="0" aria-controls="data" rowspan="1" colspan="1" aria-sort="ascending" aria-label="#: activate to sort column descending" style="width: 11px;">
										#
									</th>
									<th class="sorting" tabindex="0" aria-controls="data" rowspan="1" colspan="1" aria-label="CUID: activate to sort column ascending">
										Nannies Username
									</th>
									<th class="sorting" tabindex="0" aria-controls="data" rowspan="1" colspan="1" aria-label="Name: activate to sort column ascending">
										Childrens
									</th>
									<th class="sorting" tabindex="0" aria-controls="data" rowspan="1" colspan="1" aria-label="
										Surname: activate to sort column ascending" style="width: 60px;">
										Grade
									</th>
									<th class="sorting" tabindex="0" aria-controls="data" rowspan="1" colspan="1" aria-label="
										Surname: activate to sort column ascending" style="width: 60px;">
										Classroom
									</th>
									<th class="sorting" tabindex="0" aria-controls="data" rowspan="1" colspan="1" aria-label="Action : activate to sort column ascending">
										Action 
									 </th>
									
								</tr>
							</thead>
							<tbody>
							<?php $i = 0; ?>
							@foreach ($items as $item) 
							  <?php $i++; ?>
								<tr role="row" class="odd">
									<td class="sorting_1">
										{{$i}}
									</td>
									<td>
										{{ucfirst($item->username)}}
									</td>
									<td>
										{{sizeof(explode(",", $item->children_list))}}
									</td>
									 <td>
										{{ucfirst($item->grade_name)}}
									</td>
									<td>{{ucfirst($item->classroom_name)}}</td>
									<td>
										 <div class="table-data-feature"> 
										 
										 <button class="item" data-toggle="tooltip" data-placement="top" title="" data-original-title="View PDF">
										 <i class="fas fa-file-pdf"></i>
                                         </button>
										 
										   <button class="item" title="" data-toggle="modal" data-target="#mediumModal5{{$item->reports_id}}" data-placement="top" title="" data-original-title="View">
												<i class="fas fa-eye"></i>
											 </button>
											 
											 <!-- <button class="item" data-toggle="modal" data-target="#mediumModal6" data-placement="top" title="" data-original-title="Edit">
												<i class="zmdi zmdi-edit"></i>
											 </button> -->
										
																	
										  <a class="action_an" href="{{url('common_delete')}}/{{$item->reports_id}}/reports" > 
                                 <button class="item" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete">
                                    <i class="zmdi zmdi-delete"></i>
                                 </button>
                             </a>
										</div>
									</td>
									
									
								</tr>
								@endforeach
								
							</tbody>
											
							</table>
							
				</div>
				{{ $items->links() }}
		</div>
            </div>
        </div>
       
        <!-- END BORDERED TABLE -->
    </div>

    <?php

    	foreach ($items as $item){

    ?>

 <div class="modal fade" id="mediumModal5{{$item->reports_id}}" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">				
			   <div class="modal-dialog modal-lg" role="document">					
					<div class="modal-content">	
						<div class="modal-header">	
						<h5 class="modal-title" id="mediumModalLabel">View Reports</h5>							
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
						</button>						
						</div>
						<div class="modal-body">
						<div class="card"> 
						<div class="card-body card-block">
						<div class="row myreportsui">
							<?php
									if(!empty($item->meal_list_array)){	
																	
							?>
					<div class="col-md-4">
						<div class="card">
							<div class="card-header">
								<strong class="card-title mb-3">Meals</strong>
							</div>
							<div class="card-body">
						
								<ol>

									<li><strong>Breakfast Meal:-</strong>
									 <?php
									 
									 if(!empty(explode(",", $item->meal_list_array->item_lists1))){ ?>	
									<ol>
									   <?php  
									   foreach (explode(",", $item->meal_list_array->item_lists1) as  $value11) { ?>
									   	
										<li>{{$value11}}</li>

										<?php } ?>
									</ol>
									<?php }else { ?>
										<p>Not Any Breakfast Meal</p>
									<?php } ?> 
									</li>

									<li><strong>Lunch Meal:- </strong><?php
									 
									 if(!empty(explode(",", $item->meal_list_array->item_lists2))){ ?>	
									<ol>
									   <?php  
									   foreach (explode(",", $item->meal_list_array->item_lists2) as  $value12) { ?>
									   	
										<li>{{$value12}}</li>

										<?php } ?>
									</ol>
									<?php }else { ?>
										<p>Not Any Lunch Meal</p>
									<?php } ?> </li>
									<li><strong>Snack Meal:- </strong><?php
									 
									 if(!empty(explode(",", $item->meal_list_array->item_lists3))){ ?>	
									<ol>
									   <?php  
									   foreach (explode(",", $item->meal_list_array->item_lists3) as  $value13) { ?>
									   	
										<li>{{$value13}}</li>

										<?php } ?>
									</ol>
									<?php }else { ?>
										<p>Not Any Snack Meal</p>
									<?php } ?> </li>
								</ol>
							
							</div>
						</div>
					</div>
					<?php } ?>
					<?php
									if(!empty($item->nap_list_array)){	
																	
							?>
					<div class="col-md-4">
						<div class="card">
							<div class="card-header">
								<strong class="card-title mb-3">Nap</strong>
							</div>
							<div class="card-body">
							
								<ol>
								<?php 
									foreach (explode(",",$item->nap_list_array->time_list) as $valuenap) {
									
							   ?>

									<li><strong>Time:-</strong> {{$valuenap}}</li>

									<?php
									}
								?>
								</ol>

							</div>
						</div>
					</div>
					<?php } ?>
					<?php
									if(!empty($item->toilet_list_array)){	
																	
							?>
					<div class="col-md-4">
						<div class="card">
							<div class="card-header">
								<strong class="card-title mb-3">Toilets</strong>
							</div>
							<div class="card-body">
								<ol>
									<?php $toiletarray = explode(",",$item->toilet_list_array->list_of_toilet);


									for ($i=0; $i < sizeof($toiletarray); $i = $i + 2) { 
										
									 ?>

										<li><strong>Time:-</strong> {{$toiletarray[$i]}} ({{$toiletarray[$i+1]}})</li>
									
										<?php } ?>
								</ol>
							</div>
						</div>
					</div>
					<?php } 

					?>
					
					<div class="col-md-4">
						<div class="card">
							<div class="card-header">
								<strong class="card-title mb-3">Activites</strong>
							</div>
							<div class="card-body">
								<ol>
									<ol>

										<li><strong>Indoor:-</strong> Spell B</li>
										<li><strong>Outdoor:-</strong> Football</li>
									</ol>

								</ol>
							</div>
						</div>
					</div>

					</div>
					</div>
					</div>
					</div>				
					</div>			
			   </div>
			   </div>

    <?php

		}
    ?>

@endsection