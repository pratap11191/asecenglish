@extends('layouts.admins') 
@section('title', 'Nursery List')
@section('content')

<div class="row">
</div>
    <div class="col-md-12">
        <!-- BORDERED TABLE -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Nursery Management List
                </h3>
                  <a style="float:right;margin-bottom:10px;" class="action_an btn btn-primary" href="{{url('admin/addnursury')}}" >
                                 Create New                                         
                                </a>
            </div>
            <div class="panel-body">
                <table class="table table-bordered" id="data">
                    <thead>
                        <tr>
                            <th>
                                #
                            </th>
                            <th>
                                Email
                            </th>
                            <th>
                                Name
                            </th>
                            <th>
                               Phone No. 
                            </th>
                           
                           
                            <th>
                                Profile Image
                            </th>
                            <th>
                             Action 
                             </th>
                            <th>
                                Status
                            </th>
                        </tr>
                    </thead>
                    @if(!empty($users))
                    <tbody>
                        <?php $i=0;?>
                        @foreach($users as $user)
                        <tr>
                            <td>
                                {{ ++$i }}
                            </td>
                            <td>
                                {{$user->username}}
                            </td>
                            <td>
                                {{$user->fullname}}
                            </td>
                            <td>
                                {{$user->phone}}
                            </td>
                            
                            
                            
                            <td>
                                <img alt="Avatar" class="img-circle" src="{{url('/')}}/public/{{$user->image}}" style="height:50px">
                                </img>
                            </td>
                             <td>
                             <!-- <a  href="{{url('admin/edit_user_details')}}/{{$user->id}}"><i class="fa fa-edit" style="font-size:24px;color:black;"></i>
                               </a> -->
                                <a class="action_an" href="{{url('admin/common_delete')}}/{{$user->id}}/users" >
                                    <span class="dlt_icon">
                                        <img class="img-responsive" src="{{url('/public')}}/img/delete-button.png"/>
										<i class="fa fa-edit" style="font-size: 19px; color: #41b314;"></i>
                                    </span>
                                </a>
                             </td>
                            <td>
                               
                               
                                        @if($user->admin_status == 0)
                                         <a class="action_an btn btn-danger" href="{{url('admin/change_status')}}/{{$user->id}}" >
                                    <span class="dlt_icon">
                                        <?php echo "Inactive"; ?>
                                         </span>
                                </a>
                                        @else
                                         <a class="action_an btn btn-success" href="{{url('admin/change_status')}}/{{$user->id}}" >
                                    <span class="dlt_icon">
                                        <?php echo "Active" ?>
                                         </span>
                                </a>
                                        @endif
                                   
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    @endif
                </table>
            </div>
        </div>
       
        <!-- END BORDERED TABLE -->
    </div>
</div>

@endsection