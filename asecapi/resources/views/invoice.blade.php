<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Invoice</title>
   <style>
       .clearfix:after {
  content: "";
  display: table;
  clear: both;
}

a {
  color: #5D6975;
  text-decoration: underline;
}

body {
  position: relative;
  width: 18cm;
  height: 29.7cm;
  margin: 0 auto;
  color: #001028;
  background: #FFFFFF;
  font-family: Arial, sans-serif;
  font-size: 12px;
  font-family: Arial;
  border: 1px solid #ddd;
  padding: 10px;
}

header {
  padding: 10px 0;
  margin-bottom: 30px;
}

#logo {
  text-align: center;
  margin-bottom: 10px;
}

#logo img {
  width: 151px;
}

h1 {
  border-top: 1px solid  #5D6975;
  border-bottom: 1px solid  #5D6975;
  color: #5D6975;
  font-size: 2.4em;
  line-height: 1.4em;
  font-weight: normal;
  text-align: center;
  margin: 0 0 20px 0;
  background: url(../dimension.png);
}

#project {
  float: right;
}

#project span {
  color: #000000;
  text-align: left;
  width: 52px;
  margin-right: 10px;
  display: inline-block;
  font-size: 11px;
  font-weight: 700;
  text-transform: uppercase;
}

#company {
  float: left;
  text-align: left;
}

#project div,
#company div {
  white-space: nowrap;        
}

table {
  width: 100%;
  border-collapse: collapse;
  border-spacing: 0;
  margin-bottom: 20px;
}

table tr:nth-child(2n-1) td {
  background: #F5F5F5;
}

table th,
table td {
  text-align: left;
}

table th {
  padding: 5px 20px;
  color: #5D6975;
  border-bottom: 1px solid #C1CED9;
  white-space: nowrap;        
  font-weight: normal;
}

table .service,
table .desc {
  text-align: left;
}

table td {
  padding: 20px;
  text-align: left;
}

table td.service,
table td.desc {
  vertical-align: top;
}

table td.unit,
table td.qty,
table td.total {
  font-size: 1.2em;
}

table td.grand {
  border-top: 1px solid #5D6975;;
}

#notices .notice {
  color: #5D6975;
  font-size: 1.2em;
}

footer {
  color: #5D6975;
  width: 100%;
  height: 30px;
  position: absolute;
  bottom: 0;
  border-top: 1px solid #C1CED9;
  padding: 8px 0;
  text-align: center;
}

#company h2 {
    margin: 0;
}

div#basicdtls {
    margin: 20px 0 0;
}

span {
    color: #000000;
    text-align: left;
    width: 52px;
    margin-right: 10px;
    display: inline-block;
    font-size: 11px;
    font-weight: 700;
    text-transform: uppercase;
}

#project span, #project p{
	display:inline-block;
	padding:0;
	margin:0;
}

#project span{
	width:70px;
}
   </style>
  </head>
  <body>
    <header class="clearfix">
      <div id="logo">
        <img src="logo.png">
      </div>
      <h1>INVOICE 3-2-1</h1>
      <div id="company" class="clearfix">
	  <h2>ASEC</h2>
        <div>CLASSIC CLASSES</div>
        <div>M-1, A-40/41 Ansal Building,</div>
        <div>Commerical Complex,</div>
        <div> Mukherjee Nagar, New Delhi, Delhi 110009</div>
         <div>GSTN - 07BSVPS9249B1Z1</div>
        
		
		<div id="basicdtls">
		<span>BIL To</span>
        <div>Name : {{$userdata->fullname}}</div>
        <div>Email : {{$userdata->email}}</div>
        <div>Street Address :  {{$userdata->address}}</div>
        <div>Phone :  {{$userdata->phone}}</div>
		
      </div>
	  
      </div>
      <?php 

          if(!empty($flag)){
      ?>
      <div id="project">

        <div><span>Date</span> <p>{{$billdata['paiddate']}}</p></div>
        <div><span>Invoice #</span><p> ASECORDER{{$billdata['id']}}</p></div>
      <div><span>For</span><p> Purchasing {{ucfirst($billdata['type'])}}</p></div>
      </div>
      <?php } else { ?>
      <div id="project">

        <div><span>Date</span> <p>{{$billdata->paiddate}}</p></div>
        <div><span>Invoice #</span><p> ASECORDER{{$billdata->id}}</p></div>
        <div><span>Txn No #</span> <p>{{$billdata->trnx_id}}</p></div>
      <div><span>For</span><p> Purchasing {{ucfirst($billdata->type)}}</p></div>
      </div>
      <?php } ?>
	  
	  
	  
    </header>
    <main>
      <table>
        <thead>
          <tr>
            <th class="service">SNO.</th>
            <th class="desc">DESCRIPTION</th>
            <th>AMOUNT</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="service">1</td>
            <td class="desc">{{$coursedata->title}}</td>
          
            <td class="total">{{$coursedata->price}}</td>
          </tr>
          
          <tr>
            <td class="service">Total (*The payment includes GST)</td>
            <td class="desc"></td>
          
            <td class="total"><?php echo $coursedata->price; ?></td>
          </tr>
        
         
        </tbody>
      </table>
      <div id="notices">
        <div>NOTICE:</div>
        <div class="notice">A finance charge of 1.5% will be made on unpaid balances after 30 days.</div>
      </div>
    </main>
    <footer>
      Invoice was created on a computer and is valid without the signature and seal.
    </footer>
  
</body>
</html>