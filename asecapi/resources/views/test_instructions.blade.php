
<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Theme Made By www.w3schools.com -->
  <title>Bootstrap Theme Company Page</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <style>
  body {
    font: 400 15px Lato, sans-serif;
    line-height: 1.8;
    color: #000;
  }
 h1 {
    background: #ddd;
    padding: 11px;
    text-align: center;
    border-radius: 10px;
}
  .container-fluid {
    padding: 60px 50px;
  }
 .box {
    float: left;
    width: 100%;
}
.content {
    float: left;
    width: 100%;
	
}
.remember-label label {
    margin-left: 10px;
}
.footer {
    float: left;
    width: 100%;
    text-align: center;
    font-weight: 600;
    color: #000;
    font-size: 20px;
}
.content span {
    color: red;
    background: pink;
    padding: 4px;
    border-radius: 4px;
    margin: 0px;
}
.content p {
    margin-bottom: 0px;
}
.text,.text1,.text2,.text3 {
    float: left;
    width: 100%;
    display: -webkit-box;
}
.text button,.text1 button,.text2 button,.text3 button {
    background: black;
    padding: 10px;
    margin: 7px;
}
.cont span {
    background: pink;
    padding: 7px;
    margin: 1px;
    border-radius: 5px;
}
.cont {
    float: left;
    width: 100%;
}
.section {
    float: left;
    width: 100%;
	font-weight: 600;
}
strong {
    margin: 10px;
    padding: 0px;
}




  @keyframes slide {
    0% {
      opacity: 0;
      transform: translateY(70%);
    } 
    100% {
      opacity: 1;
      transform: translateY(0%);
    }
  }
  @-webkit-keyframes slide {
    0% {
      opacity: 0;
      -webkit-transform: translateY(70%);
    } 
    100% {
      opacity: 1;
      -webkit-transform: translateY(0%);
    }
  }
  @media screen and (max-width: 768px) {
    .col-sm-4 {
      text-align: center;
      margin: 25px 0;
    }
    .btn-lg {
      width: 100%;
      margin-bottom: 35px;
    }
  }
  @media screen and (max-width: 480px) {
    .logo {
      font-size: 150px;
    }
  }
  </style>
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">



<div class="col-md-12">
  <h1>Candidate</h1> 
  <div class="box">
    <div class="content">
	<p><strong>1</strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
	sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
	<span>Ut </span>enim ad minim veniam, 
	quis nostrud exercitation ullamco laboris nisi ut <span>aliquip</span> ex ea commodo consequat.</p>
	<p><strong>2</strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
	sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
	</div>
  <div class="text">
  <button></button>
  <p>Lorem ipsum dolor sit amet</p>
  </div>
    <div class="text3">
  <button></button>
  <p>Lorem ipsum dolor sit amet</p>
  </div>
    <div class="text1">
  <button></button>
  <p>Lorem ipsum dolor sit amet</p>
  </div>
    <div class="text2">
  <button></button>
  <p>Lorem ipsum dolor sit amet</p>
  </div>
  <div class="cont">
  <p><strong>3</strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
	sed do eiusmod tempor incididunt ut labore et dolore<span> magna</span> aliqua.</p>
  </div>
  <div class="section">
  <p><strong>4</strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
	sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
	Ut enim ad minim veniam, 
	quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
	sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
	Ut enim ad minim veniam, 
	quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
  </div>
  <div class="footer">
  <p>*All the best</p>
  </div>
  <div class="remember-label">
  <p>
<input type="checkbox" name="cb" id="cb1"><label for="cb1">I have read the instruction</label>

									</p>
  
  
  </div>
 
</div>


<script>
$(document).ready(function(){
  // Add smooth scrolling to all links in navbar + footer link
  $(".navbar a, footer a[href='#myPage']").on('click', function(event) {
    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 900, function(){
   
        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
  
  $(window).scroll(function() {
    $(".slideanim").each(function(){
      var pos = $(this).offset().top;

      var winTop = $(window).scrollTop();
        if (pos < winTop + 600) {
          $(this).addClass("slide");
        }
    });
  });
})
</script>

</body>
</html>
