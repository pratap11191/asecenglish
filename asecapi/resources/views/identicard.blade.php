<!DOCTYPE html>
<html lang="en" >

<head>
  <meta charset="UTF-8">
  <title>Make ID Card Using HTML & CSS</title>
 <meta name="viewport" content="width=device-width, initial-scale=1"> 
  
  
      <link rel="stylesheet" href="css/style.css">

  <style>
   body {
			background-color: #d7d6d3;
			font-family:'verdana';
		}
		.id-card-holder {
			width: 431px;
			padding: 4px;
			margin: 0 auto;
			background-color: #1f1f1f;
			border-radius: 5px;
			position: relative;
		}
		.id-card-holder:after {
		    content: '';
		    width: 7px;
		    display: block;
		    background-color: #0a0a0a;
		    height: 100px;
		    position: absolute;
		    top: 59px;
		    border-radius: 0 5px 5px 0;
		}
		.id-card-holder:before {
		    content: '';
		    width: 7px;
		    display: block;
		    background-color: #0a0a0a;
		    height: 100px;
		    position: absolute;
		    top: 59px;
		    right: 4px;
		    border-radius: 5px 0 0 5px;
		}
		.id-card {
			background-color: #fff;
			padding: 10px;
			border-radius: 10px;
			text-align: center;
			box-shadow: 0 0 1.5px 0px #b9b9b9;
			height: 202px;
		}
		.id-card img {
			margin: 0 auto;
		}
		.maincomlogo img {
			width: 100px;
    		margin-top: 15px;
		}
		.photo img {
			width: 80px;
			margin-top: 0;
		}
		h2 {
			font-size: 15px;
		
		}
		h3 {
			font-size: 12px;
		
			font-weight: 300;
		}
		.qr-code img {
			width: 50px;
		}
		p {
			font-size: 10px;
			margin: 0px;
		}
		.id-card-hook {
			background-color: #000;
		    width: 70px;
		    margin: 0 auto;
		    height: 15px;
		    border-radius: 5px 5px 0 0;
		}
		.id-card-hook:after {
			content: '';
		    background-color: #d7d6d3;
		    width: 47px;
		    height: 6px;
		    display: block;
		    margin: 0px auto;
		    position: relative;
		    top: 6px;
		    border-radius: 4px;
		}
		.id-card-tag-strip {
			width: 45px;
		    height: 40px;
		    background-color: #0950ef;
		    margin: 0 auto;
		    border-radius: 5px;
		    position: relative;
		    top: 9px;
		    z-index: 1;
		    border: 1px solid #0041ad;
		}
		.id-card-tag-strip:after {
			content: '';
		    display: block;
		    width: 100%;
		    height: 1px;
		    background-color: #c1c1c1;
		    position: relative;
		    top: 10px;
		}
		.id-card-tag {
			width: 0;
			height: 0;
			border-left: 100px solid transparent;
			border-right: 100px solid transparent;
			border-top: 100px solid #0958db;
			margin: -10px auto -30px auto;
		}
		.id-card-tag:after {
			content: '';
		    display: block;
		    width: 0;
		    height: 0;
		    border-left: 50px solid transparent;
		    border-right: 50px solid transparent;
		    border-top: 100px solid #d7d6d3;
		    margin: -10px auto -30px auto;
		    position: relative;
		    top: -130px;
		    left: -50px;
		}
		
		.photo {
    float: left;
    width: 80px;
}

.photo img {
    width: 100%;
}

.detailedinf {
    float: left;
    text-align: left;
    padding: 0 10px;
    width: 100%;
    margin: 10px 0;
}

.detailedinf h2 {
    font-size: 11px;
}

hr {
    float: left;
    width: 100%;
	    margin: 0;
}

.orgdtls {
    float: left;
    width: 100%;
}

.detailedinf label {
    width: 100px;
    float: left;
    color: #505256;
}

span.username {
    font-size: 11px;
}

.detailedinf table {
    float: left;
    width: 100%;
	margin-left:60px;
	margin-bottom:30px;
}

table {
	width;100%;
	margin-bottom:30px;
}

table td img{
	    width: 60px;

}
label, h2, span{margin:0; padding:0;}
.photo img{width:60px;}
.maincomlogo{width:100%; float:left; text-align:center; margin-bottom:20px;}
.maincomlogo img{width:60px;}
  </style>
</head>

<body>

 
	
	<div class="id-card-holder">
		<div class="id-card">
			
			<div class="header" style="height:40px; text-align:center;">
				<img src="https://asecenglish.com/asecapi/public/logo-admin.png" width="80">
			</div>
			
			<div class="detailedinf">
			
			<div class="photo">
				<img src="{{$datanameimage}}">
			</div>
			
			<table>
			<tbody>
			
			<tr>
				<th><h2><label>Name:</label></th>
				<td><span class="username">{{$data->fullname}}</span></h2></td>
			</tr>
			<tr>
				<th><h2><label>Contact No. :</label></th>
				<td><span class="username">{{$data->phone}}</span></h2></td>
			</tr>
			
			<tr>
				<th><h2><label>Address: </label></th>
				<td><span class="username">{{$data->address}}</span></h2></td>
			</tr>
			
		
			</tbody>
			</table>	
			</div>
			
			<table style="margin:20px 0 0 0;">
			
			
			<tbody>
			
			<tr><td><p><strong>"ASEC"</strong>Institue,4th Floor, Noida, Division Office Road </p></td></tr>
			<tr><td><p>Near Metro, Noida (NCR), India <strong>201301</strong></p></td></tr>
			<tr><td><p>Ph: 9446062493 | E-ail: asec@info.com</p></td></tr>
			
			</tbody>
			</table>
			
		
			
			</div> 

		</div>
	</div>
  
  

</body>

</html>
