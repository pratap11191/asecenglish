<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <meta name="description" content="">
        <meta name="author" content="">
        <link href="{{url('/')}}/public/img/logo-admin.png" rel="shortcut icon" type="image/x-icon"></link>
        <title>@yield('title') | Monami admin</title>
         @yield('styles')
         @include('includes.admins.head')
    </head>
    <body>
	<style>
	.myreportsui .card {
    border-radius: 12px;
    box-shadow: #d4def9 -1px 1px 20px 0px;
    border: 1px solid #ab9fa1;
}

.myreportsui .card-header {
    background: transparent;
    padding: 7px 9px;
    text-align: center;
    color: #dc87c3;
}

.myreportsui ol {
    padding: 0 25px;
}

	</style>
        <div id="wrapper">
            @include('includes.admins.sidebar')
            <div class="main" id="page-wrapper">
                @include('includes.admins.header')
                 <div class="flash-message">
                    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                        @if(Session::has('alert-' . $msg)) <?php //$msg = 'success';?>
                            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} 
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            </p>
                        @endif
                    @endforeach
                </div>
               
                <div class="wrapper wrapper-content animated fadeInRight">
                    {{ csrf_field() }}
                    @yield('content')
                </div>
            </div>
        </div>
        <div class="modal fade" id="mediumModal9" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">				<div class="modal-dialog modal-lg" role="document">					
					<div class="modal-content">	
						<div class="modal-header">	
						<h5 class="modal-title" id="mediumModalLabel">Add Grade</h5>							
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
						</button>						
						</div>
						<div class="modal-body">
						<div class="card">
						<div class="card-body card-block">
						<form action="" method="post" enctype="multipart/form-data" class="form-horizontal"> 
						<div class="row form-group">
						<div class="col col-md-3">
						<label for="text-input" class=" form-control-label">Grade Name </label> 
						</div>
						<div class="col-12 col-md-9">
						<input type="text" id="gradename" name="name" placeholder="Grade Name" class="form-control">
						<p id="gradenameerror" style="color:red;"></p>
						</div> 
						</div>
						<div class="row form-group">
						<div class="col col-md-3">
						<label for="text-input" class=" form-control-label">Grade Icon</label>
						</div>
						<div class="col-12 col-md-9">
						<input type="file" id="image" name="image" class="form-control-file">
						</div>                                            
						</div>											                                        
						<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
						</div>
						<div class="card-footer">
						<button id="graddadding" type="button" class="btn btn-primary btn-sm">
						<i class="fa fa-dot-circle-o"></i> Submit
						</button>                                        
						<button type="reset" class="btn btn-danger btn-sm">
						<i class="fa fa-ban"></i> Reset</button>
						</div>
						</form>
					</div>
					</div>
					</div>				
					</div>			
			   </div>
			   <div class="modal fade" id="mediumModal4" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">				<div class="modal-dialog modal-lg" role="document">					
					<div class="modal-content">	
						<div class="modal-header">	
						<h5 class="modal-title" id="mediumModalLabel">Add Menu Item</h5>							
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
						</button>						
						</div>
						<div class="modal-body">
						<div class="card">
						<div class="card-body card-block">
						<form action="" method="post" enctype="multipart/form-data" class="form-horizontal"> 
						<div class="row form-group">
						<div class="col col-md-3">
						<label for="text-input" class=" form-control-label">Name </label> 
						</div>
						<div class="col-12 col-md-9">
						<input type="text" id="namemenuitem" name="item_msg" placeholder="Text" class="form-control">
						<p id="namemenuitemerror" style="color: red;"></p>
						</div> 
						</div>
						<div class="row form-group">
						<div class="col col-md-3">
						<label for="text-input" class=" form-control-label">Select Meal</label>
						</div>
						<div class="col-12 col-md-9">
						<select  id="type" name="type" class="form-control">
                                                        <option value="Breakfast">Breakfast</option>
                                                        <option value="Lunch">Lunch</option>
                                                        <option value="Snack">Snack</option>
                                                    </select>
						</div>                                            
						</div>
						<div class="row form-group">
						<div class="col col-md-3">
						<label for="text-input" class=" form-control-label">Select Day</label>
						</div>
						<div class="col-12 col-md-9">
						<select  id="day_of_week" name="day_of_week" class="form-control">
                                                        <option value="Monday">Monday</option>
                                                        <option value="Tuesday">Tuesday</option>
                                                        <option value="Wednesday">Wednesday</option>
                                                          <option value="Thursday">Thursday</option>
                                                        <option value="Friday">Friday</option>
                                                    </select>
						</div>                                            
						</div>											                                        
					    <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
						</div>
						<div class="card-footer">
						<button id="graddaddingmenu" type="button" class="btn btn-primary btn-sm">
						<i class="fa fa-dot-circle-o"></i> Submit
						</button>                                                                         
						<button type="reset" class="btn btn-danger btn-sm">
						<i class="fa fa-ban"></i> Reset</button>
						</div>
						</form>
					</div>
					</div>
					</div>				
					</div>			
			   </div>
			   
			   
			  
			   
			   
			   
			    <div class="modal fade" id="mediumModal6" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg" role="document">					
					<div class="modal-content">	
						<div class="modal-header">	
						<h5 class="modal-title" id="mediumModalLabel">Edit Reports</h5>							
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
						</button>						
						</div>
						<div class="modal-body">
							<div class="card">
								<div id="resultpass"></div>
								<div class="card-body card-block">
									<form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
										<div class="row form-group">
											<div class="col col-md-3">
												<label for="text-input" class=" form-control-label">Nannies Username</label>
											</div>
											<div class="col-12 col-md-9">
												<input type="text" id="name" name="text-input" placeholder="Nannies Name" class="form-control">
											</div>
										</div>

										<div class="row form-group">
											<div class="col col-md-3">
												<label for="select" class=" form-control-label">Children Name</label>
											</div>
											<div class="col-12 col-md-9">
												<input type="text" id="name" name="text-input" placeholder="Classroom Name" class="form-control">
											</div>
										</div>
										
										<div class="row form-group">
											<div class="col col-md-3">
												<label for="select" class=" form-control-label">Grades Info</label>
											</div>
											<div class="col-12 col-md-9">
												<input type="text" id="name" name="text-input" placeholder="Grade Name" class="form-control">
											</div>
										</div>
									</form>
								</div>
								<div class="card-footer">
									<button id="getpasschange" type="button" class="btn btn-primary btn-sm">
										<i class="fa fa-dot-circle-o"></i> Submit
									</button>
									<button type="reset" class="btn btn-danger btn-sm">
										<i class="fa fa-ban"></i> Reset
									</button>
								</div>

							</div>
						</div>
					</div>				
					</div>			
			   </div>
			   
			    <div class="modal fade" id="mediumModal7" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg" role="document">					
					<div class="modal-content">	
						<div class="modal-header">	
						<h5 class="modal-title" id="mediumModalLabel">Add Reports</h5>							
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
						</button>						
						</div>
						<div class="modal-body">
						<div class="card">
								<div id="resultpass"></div>
								<div class="card-body card-block">
									<form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
										<div class="row form-group">
											<div class="col col-md-3">
												<label for="text-input" class=" form-control-label">Nannies Username</label>
											</div>
											<div class="col-12 col-md-9">
												<input type="text" id="name" name="text-input" placeholder="Nannies Name" class="form-control">
											</div>
										</div>

										<div class="row form-group">
											<div class="col col-md-3">
												<label for="select" class=" form-control-label">Children Name</label>
											</div>
											<div class="col-12 col-md-9">
												<input type="text" id="name" name="text-input" placeholder="Classroom Name" class="form-control">
											</div>
										</div>
										
										<div class="row form-group">
											<div class="col col-md-3">
												<label for="select" class=" form-control-label">Grades Info</label>
											</div>
											<div class="col-12 col-md-9">
												<input type="text" id="name" name="text-input" placeholder="Grade Name" class="form-control">
											</div>
										</div>
									</form>
								</div>
								<div class="card-footer">
									<button id="getpasschange" type="button" class="btn btn-primary btn-sm">
										<i class="fa fa-dot-circle-o"></i> Submit
									</button>
									<button type="reset" class="btn btn-danger btn-sm">
										<i class="fa fa-ban"></i> Reset
									</button>
								</div>

							</div>
						</div>
					</div>				
					</div>			
			   </div>
			   
			   
			   
			    
			    <div class="modal fade" id="mediumModalgedetails" tabindex="-1" role="dialog" aria-labelledby="mediumModalgedetailsLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div id="resultpass"></div>
            <div class="modal-header">
                <h5 class="modal-title" id="mediumModalgedetailsLabel">Add Children</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="card-body card-block">
                        <form  action="{{url('/')}}/addchildes1" method="post" enctype="multipart/form-data" class="form-horizontal">
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="text-input" class=" form-control-label">First Name </label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="text" id="name1" name="name" placeholder="Name" class="form-control">
                                    <span class="formerror ername"></span>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="text-input" class=" form-control-label">Last Name</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="text" id="surename" name="surname" placeholder="Last Name" class="form-control">
                                    <span class="formerror ersurename"></span>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="text-input" class=" form-control-label">Age (birth date)</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="text" id="age" name="age" placeholder="Age" class="form-control">
                                    <span class="formerror erage"></span>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="text-input" class=" form-control-label">Profile photo (optional)</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="file" id="image" name="image" class="form-control-file">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="text-input" class=" form-control-label">Father Name</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="text" id="father" name="father" placeholder="Father" class="form-control">
                                    <span class="formerror erfather"></span>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="text-input" class=" form-control-label">Mother Name </label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="text" id="mother" name="mother" placeholder="Mother" class="form-control">
                                    <span class="formerror ermother"></span>
                                </div>
                            </div>
                            <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="text-input" class=" form-control-label">Email</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="email" id="email" name="email" placeholder="Email" class="form-control">
                                    <span class="formerror eremail"></span>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="text-input" class=" form-control-label">Phone</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="text" id="phone" name="phone" placeholder="Phone" class="form-control">
                                    <span class="formerror erphone"></span>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="select" class=" form-control-label">Classroom</label>
                                </div>
                                <div class="col-12 col-md-9">
                                   <input type="text" name="modaluukkshowvalue" value="<?php if(!empty($classroomsdetails_name)){ echo $classroomsdetails_name;} ?>" readonly>
                                   <input type="hidden" id="classroom_id" name="classroom_id" value="<?php if(!empty($classroomsdetails_id)){ echo $classroomsdetails_id;} ?>">
                                    
                                    <span class="formerror erclassroom_id"></span>
                                </div>
                            </div>
                    </div>
                    <div class="card-footer">
                    <button id="getpasschangegradedetails" type="button" class="btn btn-primary btn-sm">
                    <i class="fa fa-dot-circle-o"></i> Submit
                    </button>
                    <button type="reset" class="btn btn-danger btn-sm">
                    <i class="fa fa-ban"></i> Reset
                    </button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
    if(!empty($usersgradedetails)){
    
        foreach ($usersgradedetails as $user) {
            
        
    
    ?>
<div class="modal fade" id="mediumModalgradedetails{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="mediumModalgradedetailsLabel{{$user->id}}" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div id="resultpass{{$user->id}}"></div>
            <div class="modal-header">
                <h5 class="modal-title" id="mediumModalgradedetailsLabel{{$user->id}}">Add Children</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="card-body card-block">
                        <form  action="{{url('/')}}/editchildes1" method="post" enctype="multipart/form-data" class="form-horizontal">
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="text-input" class=" form-control-label">First Name </label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="text" id="name{{$user->id}}" name="name" placeholder="Name" class="form-control" value="{{$user->name}}">
                                    <span class="formerror ername{{$user->id}}"></span>
                                </div>
                            </div>
                            <input type="hidden" name="id" value="{{$user->id}}">
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="text-input" class=" form-control-label">Last Name</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="text" id="surename{{$user->id}}" name="surname" placeholder="Last Name" class="form-control" value="{{$user->surname}}">
                                    <span class="formerror ersurename{{$user->id}}"></span>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="text-input" class=" form-control-label">Age (birth date)</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="text" id="age{{$user->id}}" name="age" placeholder="Age" class="form-control" value="{{$user->age}}">
                                    <span class="formerror erage{{$user->id}}"></span>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="text-input" class=" form-control-label">Profile photo (optional)</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="file" id="image{{$user->id}}" name="image" class="form-control-file">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="text-input" class=" form-control-label">Father Name</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="text" id="father{{$user->id}}" name="father" placeholder="Father" class="form-control" value="{{$user->father}}">
                                    <span class="formerror erfather{{$user->id}}"></span>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="text-input" class=" form-control-label">Mother Name </label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="text" id="mother{{$user->id}}" name="mother" placeholder="Mother" class="form-control" value="{{$user->mother}}">
                                    <span class="formerror ermother{{$user->id}}"></span>
                                </div>
                            </div>
                            <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="text-input" class=" form-control-label">Email</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="email" id="email{{$user->id}}" name="email" placeholder="Email" class="form-control" value="{{$user->email}}">
                                    <span class="formerror eremail{{$user->id}}"></span>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="text-input" class=" form-control-label">Phone</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="text" id="phone{{$user->id}}" name="phone" placeholder="Phone" class="form-control" value="{{$user->phone}}">
                                    <span class="formerror erphone{{$user->id}}"></span>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="select" class=" form-control-label">Classroom</label>
                                </div>
                                 <div class="col-12 col-md-9">
                                   <input type="text" name="modaluukkshowvalue" value="<?php if(!empty($classroomsdetails_name)){ echo $classroomsdetails_name;} ?>" readonly>
                                   <input type="hidden" id="classroom_id{{$user->id}}" name="classroom_id" value="<?php if(!empty($classroomsdetails_id)){ echo $classroomsdetails_id;} ?>">
                                    
                                    <span class="formerror erclassroom_id"></span>
                                </div>
                                
                            </div>
                    </div>
                    <div class="card-footer">
                    <button id="getpasschangegradedetails{{$user->id}}" data="{{$user->id}}" type="button" class="btn btn-primary btn-sm commonchildedit">
                    <i class="fa fa-dot-circle-o"></i> Submit
                    </button>
                    <button type="reset" class="btn btn-danger btn-sm">
                    <i class="fa fa-ban"></i> Reset
                    </button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
    }
    
    }
    ?>

<div class="modal fade" id="mediumModalgradedetailsnanny" tabindex="-1" role="dialog" aria-labelledby="mediumModalgradedetailsnannyLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="mediumModalgradedetailsnannyLabel">Add Nanny</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="card">
                                   <div id="resultpassnanny"></div>
                                    <div class="card-body card-block">
                                       <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="text-input" class=" form-control-label">Nanny First Name</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <input type="text" id="namenanny" name="text-input" placeholder="Nanny First Name" class="form-control">
                                                    
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="text-input" class=" form-control-label">Nanny Last Name</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <input type="text" id="surnamenanny" name="text-input" placeholder="Nanny Last Name" class="form-control">
                                                    
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="text-input" class=" form-control-label">Nanny Email Id</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <input type="email" id="usernamenanny" name="text-input" placeholder="Nanny Email Id" class="form-control">
                                                    
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="text-input" class=" form-control-label">Nanny Password</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <input type="text" id="passwordnanny" name="text-input" placeholder="Nanny Password" class="form-control">
                                                    
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="text-input" class=" form-control-label">Classroom Name</label>
                                                </div>
                                                  <div class="col-12 col-md-9">
                                   <input type="text" name="modaluukkshowvalue" value="<?php if(!empty($classroomsdetails_name)){ echo $classroomsdetails_name;} ?>" readonly>
                                   <input type="hidden" id="classroom_idn" name="classroom_id" value="<?php if(!empty($classroomsdetails_id)){ echo $classroomsdetails_id;} ?>">
                                    
                                    <span class="formerror erclassroom_id"></span>
                                </div>
                                            </div>
                                            
                                            
                                           
                                             <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                                                     
                                    </div>
                                    <div class="card-footer">
                                        <button id="getpasschangegradedetailsnanny" type="button" class="btn btn-primary btn-sm">
                                            <i class="fa fa-dot-circle-o"></i> Submit
                                        </button>
                                        <button type="reset" class="btn btn-danger btn-sm">
                                            <i class="fa fa-ban"></i> Reset
                                        </button>
                                    </div>
                                      </form>
                                </div>
                        </div>
                        
                    </div>
                </div>
            </div> 

            <?php

if(!empty($users1gradedetails)){

    foreach ($users1gradedetails as $user) {
        
    

?>

<div class="modal fade" id="mediumModalgradedetailsnanny{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="mediumModalgradedetailsnannyLabel{{$user->id}}" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="mediumModalgradedetailsnannyLabel{{$user->id}}">Edit Nanny</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="card">
                                   <div id="resultpassnanny{{$user->id}}"></div> 
                                    <div class="card-body card-block">
                                       <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="text-input" class=" form-control-label">Nanny First Name</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <input type="text" id="namenanny{{$user->id}}" name="text-input" placeholder="Nanny First Name" class="form-control" value="{{$user->name}}">
                                                    
                                                </div>
                                            </div>
                                            
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="text-input" class=" form-control-label">Nanny Last Name</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <input type="text" id="surnamenanny{{$user->id}}" name="text-input" placeholder="Nanny Last Name" class="form-control" value="{{$user->surename}}">
                                                    
                                                </div>
                                            </div>

                                             <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="text-input" class=" form-control-label">Nanny Email Id</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <input type="email" id="usernamenanny{{$user->id}}" name="text-input" placeholder="Nanny username" class="form-control" value="{{$user->email}}">
                                                    
                                                </div>
                                            </div>

                                             <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="text-input" class=" form-control-label">Nanny Password</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <input type="text" id="passwordnanny{{$user->id}}" name="text-input" placeholder="Nanny Password" class="form-control" value="{{$user->password}}">
                                                    
                                                </div>
                                            </div>


                                             <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="text-input" class=" form-control-label">Classroom Name</label>
                                                </div>
                                               
                                                <div class="col-12 col-md-9">
                                   <input type="text" name="modaluukkshowvalue" value="<?php if(!empty($classroomsdetails_name)){ echo $classroomsdetails_name;} ?>" readonly>
                                   <input type="hidden" id="classroom_idn{{$user->id}}" name="classroom_id" value="<?php if(!empty($classroomsdetails_id)){ echo $classroomsdetails_id;} ?>">
                                    
                                    <span class="formerror erclassroom_id"></span>
                                </div>
                                            </div>

                                            
                                            
                                             <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                                          
                                      

                                            
                                            
                                    </div>
                                    <div class="card-footer">
                                        <button data="{{$user->id}}" type="button" class="btn btn-primary btn-sm gettingpnanny">
                                            <i class="fa fa-dot-circle-o"></i> Submit
                                        </button>
                                        <button type="reset" class="btn btn-danger btn-sm">
                                            <i class="fa fa-ban"></i> Reset
                                        </button>
                                    </div>
                                      </form>
                                </div>
                        </div>
                        
                    </div>
                </div>
            </div>

<?php

}

}
?>   

             <script type="text/javascript">

   $("#getpasschangegradedetailsnanny").click(function(){
        var name = $("#namenanny").val();

    var surename = $("#surnamenanny").val();

     var username = $("#usernamenanny").val();

    var password = $("#passwordnanny").val();

    var classroom_id = $("#classroom_idn").val();

    var _token = $("#csrf-token").val();

    $.post("{{url('/')}}/addnannies",{classroom_id:classroom_id,name:name,surename:surename,email:username,password:password,_token:_token},function(result){
            var data = JSON.parse(result);

            if(data[0] == 1){
                    $('#mediumModalgradedetailsnanny').modal('hide'); 
                    location.reload();
                    
            } else {
                $("#resultpassnanny").html('<div class="alert alert-danger">'+data[1]+'</div>');
            }
            //alert(data);
    });
   });
    
   

      $(".gettingpnanny").click(function(){
        var name = $("#namenanny"+$(this).attr('data')).val();

    var surname = $("#surnamenanny"+$(this).attr('data')).val();

     var username = $("#usernamenanny"+$(this).attr('data')).val();

    var password = $("#passwordnanny"+$(this).attr('data')).val();

      var classroom_id = $("#classroom_idn"+$(this).attr('data')).val();

    var _token = $("#csrf-token").val();

    $.post("{{url('/')}}/editnannies",{classroom_id:classroom_id,name:name,surename:surname,email:username,password:password,_token:_token,id:$(this).attr('data')},function(result){
            var data = JSON.parse(result);

            if(data[0] == 1){
                    $('#mediumModal'+data[2]).modal('hide'); 
                     location.reload();
            } else {
                $("#resultpassnanny"+data[2]).html('<div class="alert alert-danger">'+data[1]+'</div>');
            }
            //alert(data);
    });
   });

    

</script>

<script type="text/javascript">
    $(".commonchildedit").click(function(){
    
        var nodechildflag = $(this).attr("data");
    
    if($('#name'+nodechildflag).val() == ''){
                $('.ername'+nodechildflag).text('Please Enter Child Name');
            }
            
    if($('#surename'+nodechildflag).val() == ''){
                $('.ersurename'+nodechildflag).text('Please Enter Child Surname');
            } 
    
    if($('#father'+nodechildflag).val() == ''){
                $('.erfather'+nodechildflag).text('Please Enter Father Name');
            }
    if($('#mother'+nodechildflag).val() == ''){
                $('.ermother'+nodechildflag).text('Please Enter Mother Name');
            }
            
    if($('#email'+nodechildflag).val() == ''){
                $('.eremail'+nodechildflag).text('Please Enter Email');
            } 
            
    if($('#phone'+nodechildflag).val() == ''){
                $('.erphone'+nodechildflag).text('Please Enter Phone Number');
            }
    if($('#age'+nodechildflag).val() == ''){
                $('.erage'+nodechildflag).text('Please enter Child Age');
            }
    if($('#classroom_id'+nodechildflag).val() == '0'){
                $('.erclassroom_id'+nodechildflag).text('Please Select a Classroom');
    }                
                                                 
           
    
            if($('#name'+nodechildflag).val() != '' && $('#email'+nodechildflag).val() != '' && $('#father'+nodechildflag).val() != '' && $('#mother'+nodechildflag).val() != '' && $('#email'+nodechildflag).val() != '' && $('#phone'+nodechildflag).val() != ''  && $('#age'+nodechildflag).val() != '' && $('#surename'+nodechildflag).val() != '' && $('#classroom_id'+nodechildflag).val() != '0'){
                    //
                    //alert("bdsfdsfgjsdgfjsdgfjgdsvjkfvdsjgfvgsjdfg");
                    $('#getpasschangegradedetails'+nodechildflag).attr('type','submit');
                    $('#getpasschangegradedetails'+nodechildflag).click();
            }
    
    });
</script>    

<script type="text/javascript">
    $("#getpasschangegradedetails").click(function(){
    
         if($('#name1').val() == ''){
                             $('.ername').text('Please Enter Child Name');
                         }
                         
         if($('#surename').val() == ''){
                             $('.ersurename').text('Please Enter Child Surname');
                         } 
    
          if($('#father').val() == ''){
                             $('.erfather').text('Please Enter Father Name');
                         }
         if($('#mother').val() == ''){
                             $('.ermother').text('Please Enter Mother Name');
                         }
                         
         if($('#email').val() == ''){
                             $('.eremail').text('Please Enter Email');
                         } 
                         
          if($('#phone').val() == ''){
                             $('.erphone').text('Please Enter Phone Number');
                         }
         if($('#age').val() == ''){
                             $('.erage').text('Please enter Child Age');
                         }
         if($('#classroom_id').val() == '0'){
                             $('.erclassroom_id').text('Please Select a Classroom');
         }                
                                                              
                        
    
                         if($('#name1').val() != '' && $('#email').val() != '' && $('#father').val() != '' && $('#mother').val() != '' && $('#email').val() != '' && $('#phone').val() != ''  && $('#age').val() != '' && $('#surename').val() != '' && $('#classroom_id').val() != '0'){
                                 //
                                 //alert("bdsfdsfgjsdgfjsdgfjgdsvjkfvdsjgfvgsjdfg");
                                 $('#getpasschangegradedetails').attr('type','submit');
                                 $('#getpasschangegradedetails').click();
                         }
    
    });
</script>			   
			   
			   
			  
			   
			   
			   
			   <script type="text/javascript">
                    $("#graddaddingmenu").click(function(){
                            if($("#namemenuitem").val() == ''){
                                    $("#namemenuitemerror").text("Please Enter Name");
                            } else {
                                $("#graddaddingmenu").attr("type","submit");
                                $("#graddaddingmenu").click();
                            }
                    });
                </script>
			    <script type="text/javascript">
                    $("#graddadding").click(function(){
                            if($("#gradename").val() == ''){
                                    $("#gradenameerror").text("Please Enter Grade Name");
                            } else {
                                $("#graddadding").attr("type","submit");
                                $("#graddadding").click();
                            }
                    });
                </script>
            <script type="text/javascript">
                $(document).ready(function() {
                    $('#data').DataTable();
                } );
            </script>
			
			  <script src="{{ asset('/public/vendor/monarss/jquery-3.2.1.min.js') }}"></script>

    <script src="{{ asset('/public/vendor/monarss/bootstrap-4.1/popper.min.js') }}"></script>
	
	<script src="{{ asset('/public/vendor/monarss/bootstrap-4.1/bootstrap.min.js') }}"></script>
	
	<script src="{{ asset('/public/vendor/monarss/slick/slick.min.js') }}"></script>
	
	
  
        
	<script src="{{ asset('/public/vendor/monarss/wow/wow.min.js') }}"></script>
	 <script src="{{ asset('/public/vendor/monarss/animsition/animsition.min.js') }}"></script> 
    
    <script src="{{ asset('/public/vendor/monarss/bootstrap-progressbar/bootstrap-progressbar.min.js') }}"></script>
	<script src="{{ asset('/public/vendor/monarss/counter-up/jquery.waypoints.min.js') }}"></script>
	<script src="{{ asset('/public/vendor/monarss/circle-progress/circle-progress.min.js') }}"></script>
	
	<script src="{{ asset('/public/vendor/monarss/counter-up/jquery.counterup.min.js') }}"></script>
	<script src="{{ asset('/public/vendor/monarss/perfect-scrollbar/perfect-scrollbar.js') }}"></script>
	<script src="{{ asset('/public/vendor/monarss/chartjs/Chart.bundle.min.js') }}"></script>
	<script src="{{ asset('/public/vendor/monarss/select2/select2.min.js') }}"></script>
    
	
<script type="text/javascript" src="{{ asset('/public/vendor/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/public/scripts/klorofil-common.js') }}"></script>
<script type="text/javascript" src="{{ asset('/public/DataTables/datatables.min.js') }}"></script>

    
	
	   <script src="{{ asset('/public/vendor/monarss/js/main.js') }}"></script>
	<script src="{{ asset('/public/vendor/monarss/js/monami.js') }}"></script>
	
            @section('scripts')
			
			



    </body>
</html>