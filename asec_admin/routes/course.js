const fs = require('fs');

module.exports = {
    getcourselist: (req, res) => {
        if(req.session.email){
        let query = "SELECT `id`,`title`,`description`,`intro_video` as video,`thumbnail`,'paid' as type,`created_at` FROM `paidcourses`"; // query database to get all the players
       
        // execute query
        db.query(query, (err, result) => {
            if (err) {
                res.redirect('/');
            }
            res.render('course.ejs', {
                title: "ASEC | COURSE MANAGEMENT",
                players: result
            });
        });
        } else {
          res.redirect('/login'); 
        }
    },
};