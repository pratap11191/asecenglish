function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}


// Add Grades activity //

function openDash(evt, gradesName) {
    var i, tabdashcontent, tabdashlinks;
    tabdashcontent = document.getElementsByClassName("tabdashcontent");
    for (i = 0; i < tabdashcontent.length; i++) {
        tabdashcontent[i].style.display = "none";
    }
    tabdashlinks = document.getElementsByClassName("tabdashlinks");
    for (i = 0; i < tabdashlinks.length; i++) {
        tabdashlinks[i].className = tabdashlinks[i].className.replace(" active", "");
    }
    document.getElementById(gradesName).style.display = "block";
    evt.currentTarget.className += " active";
}


// Add Grades activity //

function openActivity(evt, activityName) {
    var i, tabactscontent, tabactslinks;
    tabactscontent = document.getElementsByClassName("tabactscontent");
    for (i = 0; i < tabactscontent.length; i++) {
        tabactscontent[i].style.display = "none";
    }
    tabactslinks = document.getElementsByClassName("tabactslinks");
    for (i = 0; i < tabactslinks.length; i++) {
        tabactslinks[i].className = tabactslinks[i].className.replace(" active", "");
    }
    document.getElementById(activityName).style.display = "block";
    evt.currentTarget.className += " active";
}


// Add Grades activity //

function openCourse(evt, activityName) {
    var i, coursecontent, courselinks;
    coursecontent = document.getElementsByClassName("coursecontent");
    for (i = 0; i < coursecontent.length; i++) {
        coursecontent[i].style.display = "none";
    }
    courselinks = document.getElementsByClassName("courselinks");
    for (i = 0; i < courselinks.length; i++) {
        courselinks[i].className = courselinks[i].className.replace(" active", "");
    }
    document.getElementById(activityName).style.display = "block";
    evt.currentTarget.className += " active";
}


function openCourseadd(evt, activityName) {
    var i, coursetypecontent, coursenewlink;
    coursetypecontent = document.getElementsByClassName("coursetypecontent");
    for (i = 0; i < coursetypecontent.length; i++) {
        coursetypecontent[i].style.display = "none";
    }
    coursenewlink = document.getElementsByClassName("coursenewlink");
    for (i = 0; i < coursenewlink.length; i++) {
        coursenewlink[i].className = coursenewlink[i].className.replace(" active", "");
    }
    document.getElementById(activityName).style.display = "block";
    evt.currentTarget.className += " active";
}


// image upload to show
$(document).ready(function(){
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#profile-img-tag').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#profile-img").change(function(){
        readURL(this);
    });
}); 


// image upload to show
$(document).ready(function(){
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#contact-img-tag').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#contact-img").change(function(){
        readURL(this);
    });
}); 



// js For Video Thumbnails //

$(document).on("change", ".file_multi_video", function(evt) {
  var $source = $('#video_here');
  $source[0].src = URL.createObjectURL(this.files[0]);
  $source.parent()[0].load();
});

$(document).on("change", ".file_multi_video1", function(evt) {
  var $source = $('#video_here1');
  $source[0].src = URL.createObjectURL(this.files[0]);
  $source.parent()[0].load();
});
