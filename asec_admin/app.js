const express = require('express');
const fileUpload = require('express-fileupload');
const bodyParser = require('body-parser');
const mysql = require('mysql');
const path = require('path');
var expressValidator = require('express-validator');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var nodemailer = require('nodemailer');
var morgan = require('morgan');
var moment = require('moment');
var AWS = require('aws-sdk');

var ffmpeg = require('ffmpeg');

var transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'dfafdsf@gmail.com',
    pass: '1120@dsad454ram'
  }
});
const app = express();

 const {detailcourse,livechannelpage1,deletecupon1,tandc1,tandc,viewcomment,deleteassign,addassignment,addassignmentsubmit,assgnlist,paymentlist,sendnotify,submitsendnotify,getthumbnailbyurl,search,sendsms,deleteulpv,ulpvpagesubmit,ulpvpage,modifychannel,editchannelpage,servicerun,livechannelpage,submitfaq,getfaqlist,addfaq,delfaqlist,deleteorder,orderstatuschange,orderpage,deletecupon,cuponpagesubmit,cuponpage,getHomePage,login,loginsubmit,logout,deleteuser,changeuserstatus,forgotpassword,getcourselist,addcourse,addfreecoursesubmit,deletefreecourse,getbannerlist,addbannersubmit,deletebanner,addpaidcourse,deletepaidcourse,createthumbnail,getcontent,addaboutus,addcontactus,addingforumpage,forum,addforumtopic,addforumvideos,deletetopicforum,changetopicforumstatus,deletevideoforum,changevideoforumstatus,getsubscription,deletesub,addsub,addsubsubmit,changepassword,changepasswordsubmit,addbook,addbooksubmit,booklist,deletebook,gettestlist,deltestlist,changeteststatus,addtest,submittest,getsessionlist,addsession,submitsession,delsessionlist,changesessionstatus} = require('./routes/index');
//  const {addPlayerPage, addPlayer, deletePlayer, editPlayer, editPlayerPage,login,loginsubmit} = require('./routes/player');
const port = 5800;

// create connection to database
// the mysql.createConnection function takes in a configuration object which contains host, user, password and the database name.
const db = mysql.createConnection ({
    host: 'localhost',
    user: 'root',
    password: 'ramby2019',
    database: 'mobulous_asec'
});

// connect to database
db.connect((err) => {
    if (err) {
        throw err;
    }
    console.log('Connected to database');
});
global.db = db;

// configure middleware
app.set('port', process.env.port || port); // set express to use this port
app.set('views', __dirname + '/views'); // set express to look in this folder to render our view
app.set('view engine', 'ejs'); // configure template engine
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json()); // parse form data client
app.use(express.static(path.join(__dirname, 'public'))); // configure express to use public folder
app.use(fileUpload()); // configure fileupload
app.use(expressValidator());
app.use(cookieParser());
app.use(session({
    key: 'user_sid',
    secret: 'somerandonstuffs',
    resave: true,
    saveUninitialized: true,
    cookie: {
        expires: 60000000
    }
}));


// routes for the app
app.get('/previous_live',livechannelpage1)
app.get('/detailcourse/:id1/:id2',detailcourse)
app.get('/viewcomment/:id/:id2',viewcomment)
app.get('/deleteassign/:id',deleteassign);
app.post('/addassignment',addassignmentsubmit)
app.get('/addassignment',addassignment)
app.get('/assignmentlist',assgnlist)
app.get('/paymentlist',paymentlist)
app.get('/sendnotify',sendnotify)
app.post('/sendnotify',submitsendnotify)
app.get('/getthumbnailbyurl/:id',getthumbnailbyurl);
app.post('/search',search)
app.get('/sendsms/:id/:id2',sendsms);
app.post('/modifychannel/:id',modifychannel);
app.get('/editstream/:id',editchannelpage);
app.get('/servicerun/:id/:id2/:id3/:id4',servicerun);
app.get('/livestream',livechannelpage);
app.get('/faqlist',getfaqlist);
app.get('/addfaq',addfaq);
app.post('/submitfaq',submitfaq);
app.get('/delfaqlist/:id',delfaqlist);
app.get('/sessionlist',getsessionlist);
app.post('/submittest',submittest);
app.post('/submitsession',submitsession)
app.get('/addtest',addtest)
app.get('/addsession',addsession)
app.get('/testlist',gettestlist)
app.post('/orderstatuschange',orderstatuschange)
app.get('/order',orderpage)
app.post('/cupon',cuponpagesubmit)
app.get('/cupon',cuponpage);
app.post('/ulpv',ulpvpagesubmit)
app.get('/ulpv',ulpvpage);
app.get('/books',booklist);
app.get('/addbook',addbook);
app.post('/addbook',addbooksubmit);
app.post('/changepassword',changepasswordsubmit);
app.get('/changepassword',changepassword)
app.post('/addsubsubmit',addsubsubmit);
app.get('/addsubscription',addsub);
app.get('/subscriptions',getsubscription);
app.get('/forum',forum);
app.post('/addforumvideo',addforumvideos);
app.post('/addforumtopic',addforumtopic);
app.get('/addforum',addingforumpage);
app.post('/contactus',addcontactus);
app.post('/aboutus',addaboutus)
app.post('/tandc',tandc)
app.post('/tandc1',tandc1)
app.get('/content',getcontent);

app.get('/createthumbnail',createthumbnail);

app.get('/banner',getbannerlist);

app.post('/banner',addbannersubmit);

app.post('/addfreecourse',addfreecoursesubmit);

app.post('/addpaidcourse',addpaidcourse);

app.get('/addcourse',addcourse);

app.get('/course',getcourselist);

app.get('/',login);

app.post('/login',loginsubmit);

app.get('/login',login);

app.post('/',loginsubmit);

app.get('/users',getHomePage);

app.get('/logout',logout);

app.get('/deltestlist/:id',deltestlist);

app.get('/delsessionlist/:id',delsessionlist);

app.get('/changesessionstatus/:id',changesessionstatus)

app.get('/changeteststatus/:id',changeteststatus);

app.get('/deleteorder/:id',deleteorder);

app.get('/deletebook/:id',deletebook);

app.get('/deletesub/:id',deletesub);

app.get('/deletetopicforum/:id',deletetopicforum);

app.get('/changetopicforumstatus/:id',changetopicforumstatus);

app.get('/deletevideoforum/:id',deletevideoforum);

app.get('/changevideoforumstatus/:id',changevideoforumstatus);

app.get('/deletebanner/:id',deletebanner);
app.get('/deletecupon1/:id',deletecupon1);
app.get('/deletecupon/:id',deletecupon);
app.get('/deleteulpv/:id',deleteulpv);

app.get('/deleteuser/:id',deleteuser);

app.get('/deletepaidcourse/:id',deletepaidcourse);

app.get('/deletefreecourse/:id',deletefreecourse);

app.get('/changeuserstatus/:id',changeuserstatus);

app.get('/forgotpassword',forgotpassword);

app.post('/forgotpassword',function(req, res) {
       //  req.checkBody(‘email’, ‘Email Id required’).notEmpty();
       //  req.checkBody(‘password’, ‘Password is required’).notEmpty();

       //  var errors = req.validationErrors();

       // if(errors){
       //      req.session.errors = errors;
       // } else {
           let message = '';
        let email = req.body.email;
       
        
        
        
        let usernameQuery1 = "SELECT * FROM `admin` WHERE `email`='"+email+"'";

        db.query(usernameQuery1, (err, result) => {
            if (err) {
                req.session.msgf = err;
                res.redirect('/login'); 
            }
            if (result.length > 0) {
               	var mailOptions = {
			  from: 'vuewavideo@gmail.com',
			  to: email,
			  subject: "Forgot password by ASEC Admin Panel",
			  text: "Your password is : "+result[0].password
			};
			transporter.sendMail(mailOptions, function(error, info){
			  if (error) {
			  	
			    console.log(error);
			  } else {
			  	
			    console.log('Email sent: ' + info.response);
			  }
			});  
			   req.session.msgf = 'Password sent to your email id';
               res.redirect('/forgotpassword');
            } else {
                
                req.session.msgf = 'Your email id does not exists';
                res.redirect('/forgotpassword'); 
            }
        });
       
    });

// app.get('/', login);
// app.get('/home', getHomePage);
// app.get('/add', addPlayerPage);
// app.get('/edit/:id', editPlayerPage);
// app.get('/delete/:id', deletePlayer);
// app.post('/add', addPlayer);
// app.post('/edit/:id', editPlayer);

// app.post('/login',loginsubmit)


// set the app to listen on the port
app.listen(port, () => {
    console.log(`Server running on port: ${port}`);
});